﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void OneSignalSDK.OneSignalBehaviour::Start()
extern void OneSignalBehaviour_Start_mD43BDABE484924BE18E2533D414CF14BE822D668 (void);
// 0x00000002 System.Void OneSignalSDK.OneSignalBehaviour::.ctor()
extern void OneSignalBehaviour__ctor_mC493B1BE2CD99B4C7CDD5CEDCB93E3D0C484040F (void);
// 0x00000003 OneSignalSDK.OneSignal OneSignalSDK.OneSignal::get_Default()
extern void OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4 (void);
// 0x00000004 System.Void OneSignalSDK.OneSignal::set_Default(OneSignalSDK.OneSignal)
extern void OneSignal_set_Default_m26C62F9861FF4EAAAE90D75E36E8F5DC0BABD138 (void);
// 0x00000005 OneSignalSDK.User.IUserManager OneSignalSDK.OneSignal::get_User()
// 0x00000006 OneSignalSDK.Session.ISessionManager OneSignalSDK.OneSignal::get_Session()
// 0x00000007 OneSignalSDK.Notifications.INotificationsManager OneSignalSDK.OneSignal::get_Notifications()
// 0x00000008 OneSignalSDK.Location.ILocationManager OneSignalSDK.OneSignal::get_Location()
// 0x00000009 OneSignalSDK.InAppMessages.IInAppMessagesManager OneSignalSDK.OneSignal::get_InAppMessages()
// 0x0000000A OneSignalSDK.Debug.IDebugManager OneSignalSDK.OneSignal::get_Debug()
// 0x0000000B System.Boolean OneSignalSDK.OneSignal::get_PrivacyConsent()
// 0x0000000C System.Void OneSignalSDK.OneSignal::set_PrivacyConsent(System.Boolean)
// 0x0000000D System.Boolean OneSignalSDK.OneSignal::get_RequiresPrivacyConsent()
// 0x0000000E System.Void OneSignalSDK.OneSignal::set_RequiresPrivacyConsent(System.Boolean)
// 0x0000000F System.Void OneSignalSDK.OneSignal::SetLaunchURLsInApp(System.Boolean)
// 0x00000010 System.Void OneSignalSDK.OneSignal::Initialize(System.String)
// 0x00000011 System.Void OneSignalSDK.OneSignal::Login(System.String,System.String)
// 0x00000012 System.Void OneSignalSDK.OneSignal::Logout()
// 0x00000013 System.Threading.Tasks.Task`1<System.Boolean> OneSignalSDK.OneSignal::EnterLiveActivity(System.String,System.String)
// 0x00000014 System.Threading.Tasks.Task`1<System.Boolean> OneSignalSDK.OneSignal::ExitLiveActivity(System.String)
// 0x00000015 System.Void OneSignalSDK.OneSignal::add_OnInitialize(System.Action`1<System.String>)
extern void OneSignal_add_OnInitialize_m3335E7F05B20B0DF9A5B06E0CBD22A9BCF6CB9EC (void);
// 0x00000016 System.Void OneSignalSDK.OneSignal::remove_OnInitialize(System.Action`1<System.String>)
extern void OneSignal_remove_OnInitialize_m3B078C7A154DE3958C4050C6AF9AF21043AC951E (void);
// 0x00000017 System.String OneSignalSDK.OneSignal::get_AppId()
extern void OneSignal_get_AppId_mE3F51B18F55331DD5DAA72C6652E0E1EB99A12A3 (void);
// 0x00000018 System.Void OneSignalSDK.OneSignal::set_AppId(System.String)
extern void OneSignal_set_AppId_mA7AFE519FA396A621BAED79D13C63854ADE77F7F (void);
// 0x00000019 System.Boolean OneSignalSDK.OneSignal::get_DidInitialize()
extern void OneSignal_get_DidInitialize_mBD975801E862BD290960CE766AC76A19F3E2225E (void);
// 0x0000001A System.Void OneSignalSDK.OneSignal::set_DidInitialize(System.Boolean)
extern void OneSignal_set_DidInitialize_m8EFFB9C9D7E997672E5EA5309160463F7961C634 (void);
// 0x0000001B System.Void OneSignalSDK.OneSignal::_completedInit(System.String)
extern void OneSignal__completedInit_m891E343A785F50C38EAAB885C56487FC5B4E0F9A (void);
// 0x0000001C OneSignalSDK.OneSignal OneSignalSDK.OneSignal::_getDefaultInstance()
extern void OneSignal__getDefaultInstance_m849C6811959C6FDA6953AD987EA80B119CC09584 (void);
// 0x0000001D System.Void OneSignalSDK.OneSignal::.ctor()
extern void OneSignal__ctor_m182500A027110E66DD2443D6E8A22C96AA03F9DB (void);
// 0x0000001E System.Object OneSignalSDK.Json::Deserialize(System.String)
extern void Json_Deserialize_mF4067D3E8C01190CACA3166B8D9EF7E9A109259A (void);
// 0x0000001F System.String OneSignalSDK.Json::Serialize(System.Object,System.Boolean,System.Int32)
extern void Json_Serialize_m35E0717344D0105ACDDFDCE3BDB49792D100EB81 (void);
// 0x00000020 System.Boolean OneSignalSDK.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_m9D8592F6FB2502FC2B225C42DB7AC1160D8F9BE3 (void);
// 0x00000021 System.Void OneSignalSDK.Json/Parser::.ctor(System.String)
extern void Parser__ctor_m9BE59139F16DFAC491D3A03DCB162B051070807C (void);
// 0x00000022 System.Object OneSignalSDK.Json/Parser::Parse(System.String)
extern void Parser_Parse_m49285B0BD066EBA61990D9DBAA900AD380E05E15 (void);
// 0x00000023 System.Void OneSignalSDK.Json/Parser::Dispose()
extern void Parser_Dispose_mC393F1F7B2BE6DAD7B54A9B07175D06683004616 (void);
// 0x00000024 System.Collections.Generic.Dictionary`2<System.String,System.Object> OneSignalSDK.Json/Parser::ParseObject()
extern void Parser_ParseObject_mC129607C90BA628D70512884CD7410E92EC7493D (void);
// 0x00000025 System.Collections.Generic.List`1<System.Object> OneSignalSDK.Json/Parser::ParseArray()
extern void Parser_ParseArray_m91F6F3E6E84BA32CF62544967F412C86A6C940F1 (void);
// 0x00000026 System.Object OneSignalSDK.Json/Parser::ParseValue()
extern void Parser_ParseValue_m1B7E05DCE0B6435C7679A4878A0371B990764B41 (void);
// 0x00000027 System.Object OneSignalSDK.Json/Parser::ParseByToken(OneSignalSDK.Json/Parser/TOKEN)
extern void Parser_ParseByToken_mC26F86B5A9BB6DC7A8F419619B74B628F3C9F738 (void);
// 0x00000028 System.String OneSignalSDK.Json/Parser::ParseString()
extern void Parser_ParseString_mB3FF8B4C52AF5BC6279CE7DD5FD058B874E0078B (void);
// 0x00000029 System.Object OneSignalSDK.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_m2C0B8EAD7D8ED45A37E1FB2ACD5E7A8212A2C714 (void);
// 0x0000002A System.Void OneSignalSDK.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_mD5146E17EF15AFE2DF0C64426301D1EE6513C0A3 (void);
// 0x0000002B System.Char OneSignalSDK.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_mA1D0A3B252CDD78BEA07DB4E6F898C66F71A7571 (void);
// 0x0000002C System.Char OneSignalSDK.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_mB5FC1831EF72916E114ECA94FBCB3D8CA1056ACF (void);
// 0x0000002D System.String OneSignalSDK.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_m523714E46B1A48A04432D11FF5FC181B9CEE638C (void);
// 0x0000002E OneSignalSDK.Json/Parser/TOKEN OneSignalSDK.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_mE11FFDBF97BAC9AD22EE729FD0D55364388325D6 (void);
// 0x0000002F System.Void OneSignalSDK.Json/Serializer::.ctor(System.Boolean,System.Int32)
extern void Serializer__ctor_m99E1E12C48B39230E7488D02026BF2572D0792DF (void);
// 0x00000030 System.String OneSignalSDK.Json/Serializer::MakeSerialization(System.Object,System.Boolean,System.Int32)
extern void Serializer_MakeSerialization_m58B5B1531741F5A9143E9B0B70807331BEBA61CA (void);
// 0x00000031 System.Void OneSignalSDK.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m05F48CBD03A0580F94083F0EC60ACC24025F9847 (void);
// 0x00000032 System.Void OneSignalSDK.Json/Serializer::AppendNewLineFunc()
extern void Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26 (void);
// 0x00000033 System.Void OneSignalSDK.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m752864D4085775CF21F8A9866C857DCC72614491 (void);
// 0x00000034 System.Void OneSignalSDK.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m9D612F968FC32957BBCEA48C7554FB8B73CE6021 (void);
// 0x00000035 System.Void OneSignalSDK.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_mDC5E9C2B126BF45E499655700E82D80929FA3511 (void);
// 0x00000036 System.Void OneSignalSDK.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_mFFB73E1A04876CD6A2625EDD360AAC8B30DF155A (void);
// 0x00000037 System.Collections.Generic.IEnumerable`1<System.Type> OneSignalSDK.ReflectionHelpers::FindAllAssignableTypes(System.String)
// 0x00000038 System.Void OneSignalSDK.ReflectionHelpers/<>c__DisplayClass0_0`1::.ctor()
// 0x00000039 System.Boolean OneSignalSDK.ReflectionHelpers/<>c__DisplayClass0_0`1::<FindAllAssignableTypes>b__0(System.Reflection.Assembly)
// 0x0000003A System.Boolean OneSignalSDK.ReflectionHelpers/<>c__DisplayClass0_0`1::<FindAllAssignableTypes>b__2(System.Type)
// 0x0000003B System.Void OneSignalSDK.ReflectionHelpers/<>c__0`1::.cctor()
// 0x0000003C System.Void OneSignalSDK.ReflectionHelpers/<>c__0`1::.ctor()
// 0x0000003D System.Collections.Generic.IEnumerable`1<System.Type> OneSignalSDK.ReflectionHelpers/<>c__0`1::<FindAllAssignableTypes>b__0_1(System.Reflection.Assembly)
// 0x0000003E System.Void OneSignalSDK.UnityMainThreadDispatch::Send(System.Threading.SendOrPostCallback)
extern void UnityMainThreadDispatch_Send_mD724C3333BC0BF58B2A8256E466D8F32231CF76F (void);
// 0x0000003F System.Void OneSignalSDK.UnityMainThreadDispatch::Post(System.Threading.SendOrPostCallback)
extern void UnityMainThreadDispatch_Post_m142E52EBFF48DC316B6D09D0485FB1C432214C42 (void);
// 0x00000040 System.Void OneSignalSDK.UnityMainThreadDispatch::_initialize()
extern void UnityMainThreadDispatch__initialize_m4E19ED351179F0D3BA1096740320718AC7481243 (void);
// 0x00000041 System.Void OneSignalSDK.User.IUserManager::set_Language(System.String)
// 0x00000042 OneSignalSDK.User.Models.IPushSubscription OneSignalSDK.User.IUserManager::get_PushSubscription()
// 0x00000043 System.Void OneSignalSDK.User.IUserManager::AddTag(System.String,System.String)
// 0x00000044 System.Void OneSignalSDK.User.IUserManager::AddTags(System.Collections.Generic.Dictionary`2<System.String,System.String>)
// 0x00000045 System.Void OneSignalSDK.User.IUserManager::RemoveTag(System.String)
// 0x00000046 System.Void OneSignalSDK.User.IUserManager::RemoveTags(System.String[])
// 0x00000047 System.Void OneSignalSDK.User.IUserManager::AddAlias(System.String,System.String)
// 0x00000048 System.Void OneSignalSDK.User.IUserManager::AddAliases(System.Collections.Generic.Dictionary`2<System.String,System.String>)
// 0x00000049 System.Void OneSignalSDK.User.IUserManager::RemoveAlias(System.String)
// 0x0000004A System.Void OneSignalSDK.User.IUserManager::RemoveAliases(System.String[])
// 0x0000004B System.Void OneSignalSDK.User.IUserManager::AddEmail(System.String)
// 0x0000004C System.Void OneSignalSDK.User.IUserManager::RemoveEmail(System.String)
// 0x0000004D System.Void OneSignalSDK.User.IUserManager::AddSms(System.String)
// 0x0000004E System.Void OneSignalSDK.User.IUserManager::RemoveSms(System.String)
// 0x0000004F System.Void OneSignalSDK.User.Models.SubscriptionChangedDelegate::.ctor(System.Object,System.IntPtr)
extern void SubscriptionChangedDelegate__ctor_mEF3302A0E1CC3E8D4902CB9EBCAD601E65694FF0 (void);
// 0x00000050 System.Void OneSignalSDK.User.Models.SubscriptionChangedDelegate::Invoke(OneSignalSDK.User.Models.PushSubscriptionState)
extern void SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5 (void);
// 0x00000051 System.IAsyncResult OneSignalSDK.User.Models.SubscriptionChangedDelegate::BeginInvoke(OneSignalSDK.User.Models.PushSubscriptionState,System.AsyncCallback,System.Object)
extern void SubscriptionChangedDelegate_BeginInvoke_m625675C1BF9FCB5A65244CB3DF2198C97AAB440B (void);
// 0x00000052 System.Void OneSignalSDK.User.Models.SubscriptionChangedDelegate::EndInvoke(System.IAsyncResult)
extern void SubscriptionChangedDelegate_EndInvoke_mC381A634181E2C955457387F00174AA40403E455 (void);
// 0x00000053 System.Void OneSignalSDK.User.Models.IPushSubscription::add_Changed(OneSignalSDK.User.Models.SubscriptionChangedDelegate)
// 0x00000054 System.Void OneSignalSDK.User.Models.IPushSubscription::remove_Changed(OneSignalSDK.User.Models.SubscriptionChangedDelegate)
// 0x00000055 System.String OneSignalSDK.User.Models.IPushSubscription::get_Id()
// 0x00000056 System.String OneSignalSDK.User.Models.IPushSubscription::get_Token()
// 0x00000057 System.Boolean OneSignalSDK.User.Models.IPushSubscription::get_OptedIn()
// 0x00000058 System.Void OneSignalSDK.User.Models.IPushSubscription::OptIn()
// 0x00000059 System.Void OneSignalSDK.User.Models.IPushSubscription::OptOut()
// 0x0000005A System.Void OneSignalSDK.User.Models.PushSubscriptionState::.ctor()
extern void PushSubscriptionState__ctor_m017AD4E1C48437A42A4BCEB842F01DD9F73BCD01 (void);
// 0x0000005B System.Void OneSignalSDK.Session.ISessionManager::AddOutcome(System.String)
// 0x0000005C System.Void OneSignalSDK.Session.ISessionManager::AddUniqueOutcome(System.String)
// 0x0000005D System.Void OneSignalSDK.Session.ISessionManager::AddOutcomeWithValue(System.String,System.Single)
// 0x0000005E System.Void OneSignalSDK.Notifications.NotificationWillShowDelegate::.ctor(System.Object,System.IntPtr)
extern void NotificationWillShowDelegate__ctor_m729B11DF786451F7F6EE1C5AEF4E9AA75007485F (void);
// 0x0000005F OneSignalSDK.Notifications.Models.Notification OneSignalSDK.Notifications.NotificationWillShowDelegate::Invoke(OneSignalSDK.Notifications.Models.Notification)
extern void NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E (void);
// 0x00000060 System.IAsyncResult OneSignalSDK.Notifications.NotificationWillShowDelegate::BeginInvoke(OneSignalSDK.Notifications.Models.Notification,System.AsyncCallback,System.Object)
extern void NotificationWillShowDelegate_BeginInvoke_m34ADC0F539131C74580E9392FE460CE25B408EBE (void);
// 0x00000061 OneSignalSDK.Notifications.Models.Notification OneSignalSDK.Notifications.NotificationWillShowDelegate::EndInvoke(System.IAsyncResult)
extern void NotificationWillShowDelegate_EndInvoke_m904FA2FC04EB22BE5C47AE62F6F103C695A7DFF6 (void);
// 0x00000062 System.Void OneSignalSDK.Notifications.NotificationClickedDelegate::.ctor(System.Object,System.IntPtr)
extern void NotificationClickedDelegate__ctor_m1705C28B9F07F8C103F7C31D9383DCEF44416A72 (void);
// 0x00000063 System.Void OneSignalSDK.Notifications.NotificationClickedDelegate::Invoke(OneSignalSDK.Notifications.Models.NotificationClickedResult)
extern void NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074 (void);
// 0x00000064 System.IAsyncResult OneSignalSDK.Notifications.NotificationClickedDelegate::BeginInvoke(OneSignalSDK.Notifications.Models.NotificationClickedResult,System.AsyncCallback,System.Object)
extern void NotificationClickedDelegate_BeginInvoke_m92ABCADC3ED1F9B7A0E424EBE77B3B1A1E829210 (void);
// 0x00000065 System.Void OneSignalSDK.Notifications.NotificationClickedDelegate::EndInvoke(System.IAsyncResult)
extern void NotificationClickedDelegate_EndInvoke_m31C9D6D51AF94B3092B3043EFCE2048B42C804C7 (void);
// 0x00000066 System.Void OneSignalSDK.Notifications.PermissionChangedDelegate::.ctor(System.Object,System.IntPtr)
extern void PermissionChangedDelegate__ctor_mFD33D3C6CDFFF844A92B3BDAFCAD2C98C218E750 (void);
// 0x00000067 System.Void OneSignalSDK.Notifications.PermissionChangedDelegate::Invoke(System.Boolean)
extern void PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29 (void);
// 0x00000068 System.IAsyncResult OneSignalSDK.Notifications.PermissionChangedDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void PermissionChangedDelegate_BeginInvoke_m9FDECF5A75F7B7C8EB620DC8D22203CC48DC16B0 (void);
// 0x00000069 System.Void OneSignalSDK.Notifications.PermissionChangedDelegate::EndInvoke(System.IAsyncResult)
extern void PermissionChangedDelegate_EndInvoke_mB103E215707B23A791ED2A9483F85FBC5E582585 (void);
// 0x0000006A System.Void OneSignalSDK.Notifications.INotificationsManager::add_WillShow(OneSignalSDK.Notifications.NotificationWillShowDelegate)
// 0x0000006B System.Void OneSignalSDK.Notifications.INotificationsManager::remove_WillShow(OneSignalSDK.Notifications.NotificationWillShowDelegate)
// 0x0000006C System.Void OneSignalSDK.Notifications.INotificationsManager::add_Clicked(OneSignalSDK.Notifications.NotificationClickedDelegate)
// 0x0000006D System.Void OneSignalSDK.Notifications.INotificationsManager::remove_Clicked(OneSignalSDK.Notifications.NotificationClickedDelegate)
// 0x0000006E System.Void OneSignalSDK.Notifications.INotificationsManager::add_PermissionChanged(OneSignalSDK.Notifications.PermissionChangedDelegate)
// 0x0000006F System.Void OneSignalSDK.Notifications.INotificationsManager::remove_PermissionChanged(OneSignalSDK.Notifications.PermissionChangedDelegate)
// 0x00000070 System.Boolean OneSignalSDK.Notifications.INotificationsManager::get_Permission()
// 0x00000071 System.Threading.Tasks.Task`1<System.Boolean> OneSignalSDK.Notifications.INotificationsManager::RequestPermissionAsync(System.Boolean)
// 0x00000072 System.Void OneSignalSDK.Notifications.INotificationsManager::ClearAllNotifications()
// 0x00000073 System.Void OneSignalSDK.Notifications.Models.ActionButton::.ctor()
extern void ActionButton__ctor_m3E105EF7912352576B788EB4E5B0C10500519DAF (void);
// 0x00000074 System.Void OneSignalSDK.Notifications.Models.BackgroundImageLayout::.ctor()
extern void BackgroundImageLayout__ctor_m9298306566C0AFB9DFC04176B91F344AC9A454A7 (void);
// 0x00000075 System.Void OneSignalSDK.Notifications.Models.NotificationBase::.ctor()
extern void NotificationBase__ctor_m2860AACFC6907FC61442BB28CFE72D45A3573264 (void);
// 0x00000076 System.Void OneSignalSDK.Notifications.Models.Notification::.ctor()
extern void Notification__ctor_mB78313D7086E32F041B34F83D010051A4A534138 (void);
// 0x00000077 System.Void OneSignalSDK.Notifications.Models.NotificationAction::.ctor()
extern void NotificationAction__ctor_m11F6F18F901A8E07A34012423CE90131FD313B1F (void);
// 0x00000078 System.Void OneSignalSDK.Notifications.Models.NotificationClickedResult::.ctor()
extern void NotificationClickedResult__ctor_m589F1242108E92D3530D0CC876B62D62E20AEFD9 (void);
// 0x00000079 System.Boolean OneSignalSDK.Location.ILocationManager::get_IsShared()
// 0x0000007A System.Void OneSignalSDK.Location.ILocationManager::set_IsShared(System.Boolean)
// 0x0000007B System.Void OneSignalSDK.Location.ILocationManager::RequestPermission()
// 0x0000007C System.Void OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate::.ctor(System.Object,System.IntPtr)
extern void InAppMessageLifecycleDelegate__ctor_m1A037771A206CB303800408EC1B531ECE7ACF9E7 (void);
// 0x0000007D System.Void OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate::Invoke(OneSignalSDK.InAppMessages.Models.InAppMessage)
extern void InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725 (void);
// 0x0000007E System.IAsyncResult OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate::BeginInvoke(OneSignalSDK.InAppMessages.Models.InAppMessage,System.AsyncCallback,System.Object)
extern void InAppMessageLifecycleDelegate_BeginInvoke_mCFEFF7F2C7D80597D2E6FBC4666C2B63052D1865 (void);
// 0x0000007F System.Void OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate::EndInvoke(System.IAsyncResult)
extern void InAppMessageLifecycleDelegate_EndInvoke_m7F124CBDA1A79920308DF953A37E26E50C8F0A70 (void);
// 0x00000080 System.Void OneSignalSDK.InAppMessages.InAppMessageClickedDelegate::.ctor(System.Object,System.IntPtr)
extern void InAppMessageClickedDelegate__ctor_m0CFC49C490314B140CAE01763D18EB405636A4CA (void);
// 0x00000081 System.Void OneSignalSDK.InAppMessages.InAppMessageClickedDelegate::Invoke(OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult)
extern void InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F (void);
// 0x00000082 System.IAsyncResult OneSignalSDK.InAppMessages.InAppMessageClickedDelegate::BeginInvoke(OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult,System.AsyncCallback,System.Object)
extern void InAppMessageClickedDelegate_BeginInvoke_mC33D35346B49B2BF3146E790BA37EEC1F1D8D56B (void);
// 0x00000083 System.Void OneSignalSDK.InAppMessages.InAppMessageClickedDelegate::EndInvoke(System.IAsyncResult)
extern void InAppMessageClickedDelegate_EndInvoke_m239D80FFA6E671BC6148EE69B7D18D852966E819 (void);
// 0x00000084 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::add_WillDisplay(OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate)
// 0x00000085 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::remove_WillDisplay(OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate)
// 0x00000086 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::add_DidDisplay(OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate)
// 0x00000087 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::remove_DidDisplay(OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate)
// 0x00000088 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::add_WillDismiss(OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate)
// 0x00000089 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::remove_WillDismiss(OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate)
// 0x0000008A System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::add_DidDismiss(OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate)
// 0x0000008B System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::remove_DidDismiss(OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate)
// 0x0000008C System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::add_Clicked(OneSignalSDK.InAppMessages.InAppMessageClickedDelegate)
// 0x0000008D System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::remove_Clicked(OneSignalSDK.InAppMessages.InAppMessageClickedDelegate)
// 0x0000008E System.Boolean OneSignalSDK.InAppMessages.IInAppMessagesManager::get_Paused()
// 0x0000008F System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::set_Paused(System.Boolean)
// 0x00000090 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::AddTrigger(System.String,System.Object)
// 0x00000091 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::AddTriggers(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x00000092 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::RemoveTrigger(System.String)
// 0x00000093 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::RemoveTriggers(System.String[])
// 0x00000094 System.Void OneSignalSDK.InAppMessages.IInAppMessagesManager::ClearTriggers()
// 0x00000095 System.String OneSignalSDK.InAppMessages.Models.InAppMessageAction::get_ClickName()
extern void InAppMessageAction_get_ClickName_mB9BA05E950A6A1FA8B070BAD85A66E7FD2066AC7 (void);
// 0x00000096 System.String OneSignalSDK.InAppMessages.Models.InAppMessageAction::get_ClickUrl()
extern void InAppMessageAction_get_ClickUrl_m14B0126EC220A546F11CD023090BF05172C1B71E (void);
// 0x00000097 System.Boolean OneSignalSDK.InAppMessages.Models.InAppMessageAction::get_IsFirstClick()
extern void InAppMessageAction_get_IsFirstClick_mBD101B7A7AD940494AD1540CF5205C880CC29381 (void);
// 0x00000098 System.Boolean OneSignalSDK.InAppMessages.Models.InAppMessageAction::get_ClosesMessage()
extern void InAppMessageAction_get_ClosesMessage_mB987735CD5F9329A7629804D6C8D71CDAE4410A2 (void);
// 0x00000099 System.Void OneSignalSDK.InAppMessages.Models.InAppMessageAction::.ctor()
extern void InAppMessageAction__ctor_mFC28FFD67728ECE79F12C7F1EC741F36DF719600 (void);
// 0x0000009A System.Void OneSignalSDK.InAppMessages.Models.InAppMessage::.ctor()
extern void InAppMessage__ctor_m79C91EC28E66B99708C36336A24CAB4947379F9C (void);
// 0x0000009B System.String OneSignalSDK.InAppMessages.Models.IInAppMessageAction::get_ClickName()
// 0x0000009C System.String OneSignalSDK.InAppMessages.Models.IInAppMessageAction::get_ClickUrl()
// 0x0000009D System.Boolean OneSignalSDK.InAppMessages.Models.IInAppMessageAction::get_IsFirstClick()
// 0x0000009E System.Boolean OneSignalSDK.InAppMessages.Models.IInAppMessageAction::get_ClosesMessage()
// 0x0000009F System.Void OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult::.ctor()
extern void InAppMessageClickedResult__ctor_mACC7F9B3D42E61648D396832042E78411F97EF0A (void);
// 0x000000A0 OneSignalSDK.Debug.Models.LogLevel OneSignalSDK.Debug.IDebugManager::get_LogLevel()
// 0x000000A1 System.Void OneSignalSDK.Debug.IDebugManager::set_LogLevel(OneSignalSDK.Debug.Models.LogLevel)
// 0x000000A2 OneSignalSDK.Debug.Models.LogLevel OneSignalSDK.Debug.IDebugManager::get_AlertLevel()
// 0x000000A3 System.Void OneSignalSDK.Debug.IDebugManager::set_AlertLevel(OneSignalSDK.Debug.Models.LogLevel)
// 0x000000A4 System.Void OneSignalSDK.Debug.Utilities.SDKDebug::add_LogIntercept(System.Action`1<System.Object>)
extern void SDKDebug_add_LogIntercept_m61BE636BC5C5E5829B311939EB1EE0C3E3517003 (void);
// 0x000000A5 System.Void OneSignalSDK.Debug.Utilities.SDKDebug::remove_LogIntercept(System.Action`1<System.Object>)
extern void SDKDebug_remove_LogIntercept_mC9FBC2FA351BAFE0190493C8E0C963C43C75027C (void);
// 0x000000A6 System.Void OneSignalSDK.Debug.Utilities.SDKDebug::add_WarnIntercept(System.Action`1<System.Object>)
extern void SDKDebug_add_WarnIntercept_m0C178174F2632C5CB898FA381C3A8E8F721CE393 (void);
// 0x000000A7 System.Void OneSignalSDK.Debug.Utilities.SDKDebug::remove_WarnIntercept(System.Action`1<System.Object>)
extern void SDKDebug_remove_WarnIntercept_m7669E8015F8C1DCFD990ACB466656FA4D5FA0100 (void);
// 0x000000A8 System.Void OneSignalSDK.Debug.Utilities.SDKDebug::add_ErrorIntercept(System.Action`1<System.Object>)
extern void SDKDebug_add_ErrorIntercept_m9D17C17A2FD46E3316179B2565D56BEA75608342 (void);
// 0x000000A9 System.Void OneSignalSDK.Debug.Utilities.SDKDebug::remove_ErrorIntercept(System.Action`1<System.Object>)
extern void SDKDebug_remove_ErrorIntercept_m436DDAFBD3CB3820D20B069022F07D09383BDF7D (void);
// 0x000000AA System.Void OneSignalSDK.Debug.Utilities.SDKDebug::Info(System.String)
extern void SDKDebug_Info_m531A4499060F346EAB9413566E49445F27E3D6CB (void);
// 0x000000AB System.Void OneSignalSDK.Debug.Utilities.SDKDebug::Warn(System.String)
extern void SDKDebug_Warn_m1580DF9F5739B31951AEEB5F2457F7C76331FC40 (void);
// 0x000000AC System.Void OneSignalSDK.Debug.Utilities.SDKDebug::Error(System.String)
extern void SDKDebug_Error_m47AE8403BECFDE427D13E676064C7AD491769786 (void);
// 0x000000AD System.String OneSignalSDK.Debug.Utilities.SDKDebug::_formatMessage(System.String)
extern void SDKDebug__formatMessage_m0CCD6BF7BE05E151DAAE8ADD438321725DB6818F (void);
static Il2CppMethodPointer s_methodPointers[173] = 
{
	OneSignalBehaviour_Start_mD43BDABE484924BE18E2533D414CF14BE822D668,
	OneSignalBehaviour__ctor_mC493B1BE2CD99B4C7CDD5CEDCB93E3D0C484040F,
	OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4,
	OneSignal_set_Default_m26C62F9861FF4EAAAE90D75E36E8F5DC0BABD138,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OneSignal_add_OnInitialize_m3335E7F05B20B0DF9A5B06E0CBD22A9BCF6CB9EC,
	OneSignal_remove_OnInitialize_m3B078C7A154DE3958C4050C6AF9AF21043AC951E,
	OneSignal_get_AppId_mE3F51B18F55331DD5DAA72C6652E0E1EB99A12A3,
	OneSignal_set_AppId_mA7AFE519FA396A621BAED79D13C63854ADE77F7F,
	OneSignal_get_DidInitialize_mBD975801E862BD290960CE766AC76A19F3E2225E,
	OneSignal_set_DidInitialize_m8EFFB9C9D7E997672E5EA5309160463F7961C634,
	OneSignal__completedInit_m891E343A785F50C38EAAB885C56487FC5B4E0F9A,
	OneSignal__getDefaultInstance_m849C6811959C6FDA6953AD987EA80B119CC09584,
	OneSignal__ctor_m182500A027110E66DD2443D6E8A22C96AA03F9DB,
	Json_Deserialize_mF4067D3E8C01190CACA3166B8D9EF7E9A109259A,
	Json_Serialize_m35E0717344D0105ACDDFDCE3BDB49792D100EB81,
	Parser_IsWordBreak_m9D8592F6FB2502FC2B225C42DB7AC1160D8F9BE3,
	Parser__ctor_m9BE59139F16DFAC491D3A03DCB162B051070807C,
	Parser_Parse_m49285B0BD066EBA61990D9DBAA900AD380E05E15,
	Parser_Dispose_mC393F1F7B2BE6DAD7B54A9B07175D06683004616,
	Parser_ParseObject_mC129607C90BA628D70512884CD7410E92EC7493D,
	Parser_ParseArray_m91F6F3E6E84BA32CF62544967F412C86A6C940F1,
	Parser_ParseValue_m1B7E05DCE0B6435C7679A4878A0371B990764B41,
	Parser_ParseByToken_mC26F86B5A9BB6DC7A8F419619B74B628F3C9F738,
	Parser_ParseString_mB3FF8B4C52AF5BC6279CE7DD5FD058B874E0078B,
	Parser_ParseNumber_m2C0B8EAD7D8ED45A37E1FB2ACD5E7A8212A2C714,
	Parser_EatWhitespace_mD5146E17EF15AFE2DF0C64426301D1EE6513C0A3,
	Parser_get_PeekChar_mA1D0A3B252CDD78BEA07DB4E6F898C66F71A7571,
	Parser_get_NextChar_mB5FC1831EF72916E114ECA94FBCB3D8CA1056ACF,
	Parser_get_NextWord_m523714E46B1A48A04432D11FF5FC181B9CEE638C,
	Parser_get_NextToken_mE11FFDBF97BAC9AD22EE729FD0D55364388325D6,
	Serializer__ctor_m99E1E12C48B39230E7488D02026BF2572D0792DF,
	Serializer_MakeSerialization_m58B5B1531741F5A9143E9B0B70807331BEBA61CA,
	Serializer_SerializeValue_m05F48CBD03A0580F94083F0EC60ACC24025F9847,
	Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26,
	Serializer_SerializeObject_m752864D4085775CF21F8A9866C857DCC72614491,
	Serializer_SerializeArray_m9D612F968FC32957BBCEA48C7554FB8B73CE6021,
	Serializer_SerializeString_mDC5E9C2B126BF45E499655700E82D80929FA3511,
	Serializer_SerializeOther_mFFB73E1A04876CD6A2625EDD360AAC8B30DF155A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityMainThreadDispatch_Send_mD724C3333BC0BF58B2A8256E466D8F32231CF76F,
	UnityMainThreadDispatch_Post_m142E52EBFF48DC316B6D09D0485FB1C432214C42,
	UnityMainThreadDispatch__initialize_m4E19ED351179F0D3BA1096740320718AC7481243,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SubscriptionChangedDelegate__ctor_mEF3302A0E1CC3E8D4902CB9EBCAD601E65694FF0,
	SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5,
	SubscriptionChangedDelegate_BeginInvoke_m625675C1BF9FCB5A65244CB3DF2198C97AAB440B,
	SubscriptionChangedDelegate_EndInvoke_mC381A634181E2C955457387F00174AA40403E455,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PushSubscriptionState__ctor_m017AD4E1C48437A42A4BCEB842F01DD9F73BCD01,
	NULL,
	NULL,
	NULL,
	NotificationWillShowDelegate__ctor_m729B11DF786451F7F6EE1C5AEF4E9AA75007485F,
	NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E,
	NotificationWillShowDelegate_BeginInvoke_m34ADC0F539131C74580E9392FE460CE25B408EBE,
	NotificationWillShowDelegate_EndInvoke_m904FA2FC04EB22BE5C47AE62F6F103C695A7DFF6,
	NotificationClickedDelegate__ctor_m1705C28B9F07F8C103F7C31D9383DCEF44416A72,
	NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074,
	NotificationClickedDelegate_BeginInvoke_m92ABCADC3ED1F9B7A0E424EBE77B3B1A1E829210,
	NotificationClickedDelegate_EndInvoke_m31C9D6D51AF94B3092B3043EFCE2048B42C804C7,
	PermissionChangedDelegate__ctor_mFD33D3C6CDFFF844A92B3BDAFCAD2C98C218E750,
	PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29,
	PermissionChangedDelegate_BeginInvoke_m9FDECF5A75F7B7C8EB620DC8D22203CC48DC16B0,
	PermissionChangedDelegate_EndInvoke_mB103E215707B23A791ED2A9483F85FBC5E582585,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ActionButton__ctor_m3E105EF7912352576B788EB4E5B0C10500519DAF,
	BackgroundImageLayout__ctor_m9298306566C0AFB9DFC04176B91F344AC9A454A7,
	NotificationBase__ctor_m2860AACFC6907FC61442BB28CFE72D45A3573264,
	Notification__ctor_mB78313D7086E32F041B34F83D010051A4A534138,
	NotificationAction__ctor_m11F6F18F901A8E07A34012423CE90131FD313B1F,
	NotificationClickedResult__ctor_m589F1242108E92D3530D0CC876B62D62E20AEFD9,
	NULL,
	NULL,
	NULL,
	InAppMessageLifecycleDelegate__ctor_m1A037771A206CB303800408EC1B531ECE7ACF9E7,
	InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725,
	InAppMessageLifecycleDelegate_BeginInvoke_mCFEFF7F2C7D80597D2E6FBC4666C2B63052D1865,
	InAppMessageLifecycleDelegate_EndInvoke_m7F124CBDA1A79920308DF953A37E26E50C8F0A70,
	InAppMessageClickedDelegate__ctor_m0CFC49C490314B140CAE01763D18EB405636A4CA,
	InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F,
	InAppMessageClickedDelegate_BeginInvoke_mC33D35346B49B2BF3146E790BA37EEC1F1D8D56B,
	InAppMessageClickedDelegate_EndInvoke_m239D80FFA6E671BC6148EE69B7D18D852966E819,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InAppMessageAction_get_ClickName_mB9BA05E950A6A1FA8B070BAD85A66E7FD2066AC7,
	InAppMessageAction_get_ClickUrl_m14B0126EC220A546F11CD023090BF05172C1B71E,
	InAppMessageAction_get_IsFirstClick_mBD101B7A7AD940494AD1540CF5205C880CC29381,
	InAppMessageAction_get_ClosesMessage_mB987735CD5F9329A7629804D6C8D71CDAE4410A2,
	InAppMessageAction__ctor_mFC28FFD67728ECE79F12C7F1EC741F36DF719600,
	InAppMessage__ctor_m79C91EC28E66B99708C36336A24CAB4947379F9C,
	NULL,
	NULL,
	NULL,
	NULL,
	InAppMessageClickedResult__ctor_mACC7F9B3D42E61648D396832042E78411F97EF0A,
	NULL,
	NULL,
	NULL,
	NULL,
	SDKDebug_add_LogIntercept_m61BE636BC5C5E5829B311939EB1EE0C3E3517003,
	SDKDebug_remove_LogIntercept_mC9FBC2FA351BAFE0190493C8E0C963C43C75027C,
	SDKDebug_add_WarnIntercept_m0C178174F2632C5CB898FA381C3A8E8F721CE393,
	SDKDebug_remove_WarnIntercept_m7669E8015F8C1DCFD990ACB466656FA4D5FA0100,
	SDKDebug_add_ErrorIntercept_m9D17C17A2FD46E3316179B2565D56BEA75608342,
	SDKDebug_remove_ErrorIntercept_m436DDAFBD3CB3820D20B069022F07D09383BDF7D,
	SDKDebug_Info_m531A4499060F346EAB9413566E49445F27E3D6CB,
	SDKDebug_Warn_m1580DF9F5739B31951AEEB5F2457F7C76331FC40,
	SDKDebug_Error_m47AE8403BECFDE427D13E676064C7AD491769786,
	SDKDebug__formatMessage_m0CCD6BF7BE05E151DAAE8ADD438321725DB6818F,
};
static const int32_t s_InvokerIndices[173] = 
{
	6023,
	6023,
	11305,
	10506,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	10506,
	10506,
	11305,
	10506,
	11284,
	10498,
	10506,
	11305,
	6023,
	10167,
	7808,
	9924,
	4854,
	10167,
	6023,
	5891,
	5891,
	5891,
	4335,
	5891,
	5891,
	6023,
	6006,
	6006,
	5891,
	5866,
	2323,
	7808,
	4854,
	6023,
	4854,
	4854,
	4854,
	4854,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	10506,
	10506,
	11334,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	2760,
	4854,
	1319,
	4854,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6023,
	0,
	0,
	0,
	2760,
	4339,
	1319,
	4339,
	2760,
	4854,
	1319,
	4854,
	2760,
	4772,
	1266,
	4854,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6023,
	6023,
	6023,
	6023,
	6023,
	6023,
	0,
	0,
	0,
	2760,
	4854,
	1319,
	4854,
	2760,
	4854,
	1319,
	4854,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	5891,
	5891,
	5809,
	5809,
	6023,
	6023,
	0,
	0,
	0,
	0,
	6023,
	0,
	0,
	0,
	0,
	10506,
	10506,
	10506,
	10506,
	10506,
	10506,
	10506,
	10506,
	10506,
	10167,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x0200000A, { 7, 3 } },
	{ 0x06000037, { 0, 7 } },
};
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_1_t2F239327A08008F2BB015FBA4FBBA79AB6D8D95D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_1__ctor_m751CFF3A171D13005A57192EFE8CE0B47F55DEEA;
extern const uint32_t g_rgctx_T_t0F2E9AF70DA9E8F34C43FB6C82DC270777C02D66;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_1_U3CFindAllAssignableTypesU3Eb__0_m6E81704F6AFCEE553523ABFFCB2CBE6D29942A20;
extern const uint32_t g_rgctx_U3CU3Ec__0_1_tA4B50308657B23F0B3FA83D98089D72A4F75B432;
extern const uint32_t g_rgctx_U3CU3Ec__0_1_U3CFindAllAssignableTypesU3Eb__0_1_m6B78479B31689395BE7E8E72958E12A57A4A0383;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_1_U3CFindAllAssignableTypesU3Eb__2_m073041367EB296A679B82EF928DD990D8382849C;
extern const uint32_t g_rgctx_U3CU3Ec__0_1_t16DAF27A44F4F36B896BFD09E2669E07C326A69C;
extern const uint32_t g_rgctx_U3CU3Ec__0_1__ctor_mBA513EFF180676D3B4A432FDD109121C16D710FE;
extern const uint32_t g_rgctx_U3CU3Ec__0_1_t16DAF27A44F4F36B896BFD09E2669E07C326A69C;
static const Il2CppRGCTXDefinition s_rgctxValues[10] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_1_t2F239327A08008F2BB015FBA4FBBA79AB6D8D95D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_1__ctor_m751CFF3A171D13005A57192EFE8CE0B47F55DEEA },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t0F2E9AF70DA9E8F34C43FB6C82DC270777C02D66 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_1_U3CFindAllAssignableTypesU3Eb__0_m6E81704F6AFCEE553523ABFFCB2CBE6D29942A20 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__0_1_tA4B50308657B23F0B3FA83D98089D72A4F75B432 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__0_1_U3CFindAllAssignableTypesU3Eb__0_1_m6B78479B31689395BE7E8E72958E12A57A4A0383 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_1_U3CFindAllAssignableTypesU3Eb__2_m073041367EB296A679B82EF928DD990D8382849C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__0_1_t16DAF27A44F4F36B896BFD09E2669E07C326A69C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__0_1__ctor_mBA513EFF180676D3B4A432FDD109121C16D710FE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__0_1_t16DAF27A44F4F36B896BFD09E2669E07C326A69C },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_OneSignal_Core_CodeGenModule;
const Il2CppCodeGenModule g_OneSignal_Core_CodeGenModule = 
{
	"OneSignal.Core.dll",
	173,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	10,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
