﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtualActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InvokerActionInvoker1
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1 p1)
	{
		void* params[1] = { &p1 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename T1>
struct InvokerActionInvoker1<T1*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1)
	{
		void* params[1] = { p1 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename T1, typename T2>
struct InvokerActionInvoker2;
template <typename T1, typename T2>
struct InvokerActionInvoker2<T1*, T2>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1, T2 p2)
	{
		void* params[2] = { p1, &p2 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename T1, typename T2>
struct InvokerActionInvoker2<T1*, T2*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1, T2* p2)
	{
		void* params[2] = { p1, p2 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename R, typename T1>
struct InvokerFuncInvoker1;
template <typename R, typename T1>
struct InvokerFuncInvoker1<R, T1*>
{
	static inline R Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1)
	{
		R ret;
		void* params[1] = { p1 };
		method->invoker_method(methodPtr, method, obj, params, &ret);
		return ret;
	}
};
template <typename R, typename T1, typename T2>
struct InvokerFuncInvoker2;
template <typename R, typename T1, typename T2>
struct InvokerFuncInvoker2<R, T1*, T2*>
{
	static inline R Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1, T2* p2)
	{
		R ret;
		void* params[2] = { p1, p2 };
		method->invoker_method(methodPtr, method, obj, params, &ret);
		return ret;
	}
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t9FA6D82CAFC18769F7515BB51D1C56DAE09381C3;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_tE1603CE612C16451D1E56FF4D4859D4FE4087C28;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t6686595E4CB7AC210F0EF075F7B1DD4A21D3902B;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36;
// System.Collections.Generic.List`1<OneSignalSDK.Notifications.Models.ActionButton>
struct List_1_t3173AEAC2CD678AC1E8802FE100BFCBB6C096FE3;
// System.Collections.Generic.List`1<OneSignalSDK.Notifications.Models.NotificationBase>
struct List_1_tB2A9DC3BB80251C6410CDE58170B9B566FF563DA;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// OneSignalSDK.Notifications.Models.ActionButton
struct ActionButton_tD0F4E6CE1C60DE2866BA6361EB6DA58461452F04;
// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C;
// OneSignalSDK.Notifications.Models.BackgroundImageLayout
struct BackgroundImageLayout_tD6FC701C0DDF4BD63EDB891962009F9E14E4E2F2;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// System.Globalization.Calendar
struct Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B;
// System.Globalization.CompareInfo
struct CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57;
// System.Globalization.CultureData
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D;
// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.IAsyncResult
struct IAsyncResult_t7B9B5A0ECB35DCEC31B8A8122C37D687369253B5;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.IFormatProvider
struct IFormatProvider_tC202922D43BFF3525109ABF3FB79625F5646AB52;
// System.Collections.IList
struct IList_t1C522956D79B7DC92B5B01053DF1AC058C8B598D;
// OneSignalSDK.InAppMessages.Models.InAppMessage
struct InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449;
// OneSignalSDK.InAppMessages.Models.InAppMessageAction
struct InAppMessageAction_tCE4215F332A511CE8DA5B85CC5A15D22EFB1D430;
// OneSignalSDK.InAppMessages.InAppMessageClickedDelegate
struct InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84;
// OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult
struct InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5;
// OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate
struct InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// OneSignalSDK.Notifications.Models.Notification
struct Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B;
// OneSignalSDK.Notifications.Models.NotificationAction
struct NotificationAction_tCFE5BF25A39173707A04C574311BAA53CC8C1591;
// OneSignalSDK.Notifications.Models.NotificationBase
struct NotificationBase_tEAE8E5ACEC71A0A416C4AAB702F39C734AAF3686;
// OneSignalSDK.Notifications.NotificationClickedDelegate
struct NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332;
// OneSignalSDK.Notifications.Models.NotificationClickedResult
struct NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C;
// OneSignalSDK.Notifications.NotificationWillShowDelegate
struct NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472;
// OneSignalSDK.OneSignal
struct OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85;
// OneSignalSDK.OneSignalBehaviour
struct OneSignalBehaviour_t984420F12C8E067001F23C636606C6B3A395AFD7;
// OneSignalSDK.Notifications.PermissionChangedDelegate
struct PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED;
// OneSignalSDK.User.Models.PushSubscriptionState
struct PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.IO.StringReader
struct StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8;
// OneSignalSDK.User.Models.SubscriptionChangedDelegate
struct SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693;
// System.Threading.SynchronizationContext
struct SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0;
// System.Globalization.TextInfo
struct TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4;
// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7;
// System.Type
struct Type_t;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// OneSignalSDK.Json/Parser
struct Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C;
// OneSignalSDK.Json/Serializer
struct Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_t37E7B9DC5B4EF41D190D607F92835BF1171C0E8E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_t6331596D5DD37C462B1B8D49CF6B319B00AB7131_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IList_t1C522956D79B7DC92B5B01053DF1AC058C8B598D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ILocationManager_t606C7B541F9B38F180FF09F582F1302B5FB080F9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int16_tB8EF286A9C33492FA6E6D6E67320BE93E794A175_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SByte_tFEFFEF5D2FEBF5207950AE6FAC150FC53B668DB5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral2C3D4826D5236B3C9A914C5CE2E3D8CEA48AC7CE;
IL2CPP_EXTERN_C String_t* _stringLiteral526C999F139627F5A148FB7032812E83F8AEFC82;
IL2CPP_EXTERN_C String_t* _stringLiteral5962E944D7340CE47999BF097B4AFD70C1501FB9;
IL2CPP_EXTERN_C String_t* _stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174;
IL2CPP_EXTERN_C String_t* _stringLiteral77D38C0623F92B292B925F6E72CF5CF99A20D4EB;
IL2CPP_EXTERN_C String_t* _stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51;
IL2CPP_EXTERN_C String_t* _stringLiteral7F3238CD8C342B06FB9AB185C610175C84625462;
IL2CPP_EXTERN_C String_t* _stringLiteral848E5ED630B3142F565DD995C6E8D30187ED33CD;
IL2CPP_EXTERN_C String_t* _stringLiteral84C1E07F84B6E7BDCC02A904AFEC3BBD2CAE6EAA;
IL2CPP_EXTERN_C String_t* _stringLiteral8DC762363E16C7B48A45F7AD0379382854A5AAC7;
IL2CPP_EXTERN_C String_t* _stringLiteral91F7E6F20B6646D90CD184646336FEFB5B3FCC2F;
IL2CPP_EXTERN_C String_t* _stringLiteral94672909F51EA6867CBD4D94D2F92A0929C2009E;
IL2CPP_EXTERN_C String_t* _stringLiteralA7C3FCA8C63E127B542B38A5CA5E3FEEDDD1B122;
IL2CPP_EXTERN_C String_t* _stringLiteralB78F235D4291950A7D101307609C259F3E1F033F;
IL2CPP_EXTERN_C String_t* _stringLiteralB7C45DD316C68ABF3429C20058C2981C652192F2;
IL2CPP_EXTERN_C String_t* _stringLiteralBFB5DAA5265160CE753212EA77C3196536EF9342;
IL2CPP_EXTERN_C String_t* _stringLiteralCBCEF9519EFE4C7006CFC5D13511B4CA84EDFAA1;
IL2CPP_EXTERN_C String_t* _stringLiteralDA666908BB15F4E1D2649752EC5DCBD0D5C64699;
IL2CPP_EXTERN_C String_t* _stringLiteralE727BF366E3CC855B808D806440542BF7152AF19;
IL2CPP_EXTERN_C String_t* _stringLiteralF18840F490E42D3CE48CDCBF47229C1C240F8ABE;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_First_TisType_t_m22B99E5DC993C761AE0CB9632BA5749F27E1E074_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReflectionHelpers_FindAllAssignableTypes_TisOneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_m81EA6181DA97D554BEE7ADD4C1CCD66FB43849AF_RuntimeMethod_var;
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_com;
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_pinvoke;
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com;
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t68C32C04644151B6D2857B11559C9F0880906353 
{
};

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// OneSignalSDK.Notifications.Models.ActionButton
struct ActionButton_tD0F4E6CE1C60DE2866BA6361EB6DA58461452F04  : public RuntimeObject
{
	// System.String OneSignalSDK.Notifications.Models.ActionButton::id
	String_t* ___id_0;
	// System.String OneSignalSDK.Notifications.Models.ActionButton::text
	String_t* ___text_1;
	// System.String OneSignalSDK.Notifications.Models.ActionButton::icon
	String_t* ___icon_2;
};

// OneSignalSDK.Notifications.Models.BackgroundImageLayout
struct BackgroundImageLayout_tD6FC701C0DDF4BD63EDB891962009F9E14E4E2F2  : public RuntimeObject
{
	// System.String OneSignalSDK.Notifications.Models.BackgroundImageLayout::image
	String_t* ___image_0;
	// System.String OneSignalSDK.Notifications.Models.BackgroundImageLayout::titleTextColor
	String_t* ___titleTextColor_1;
	// System.String OneSignalSDK.Notifications.Models.BackgroundImageLayout::bodyTextColor
	String_t* ___bodyTextColor_2;
};

// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0  : public RuntimeObject
{
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D* ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;
};
// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// OneSignalSDK.InAppMessages.Models.InAppMessage
struct InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449  : public RuntimeObject
{
	// System.String OneSignalSDK.InAppMessages.Models.InAppMessage::messageId
	String_t* ___messageId_0;
};

// OneSignalSDK.InAppMessages.Models.InAppMessageAction
struct InAppMessageAction_tCE4215F332A511CE8DA5B85CC5A15D22EFB1D430  : public RuntimeObject
{
	// System.String OneSignalSDK.InAppMessages.Models.InAppMessageAction::click_name
	String_t* ___click_name_0;
	// System.String OneSignalSDK.InAppMessages.Models.InAppMessageAction::click_url
	String_t* ___click_url_1;
	// System.Boolean OneSignalSDK.InAppMessages.Models.InAppMessageAction::first_click
	bool ___first_click_2;
	// System.Boolean OneSignalSDK.InAppMessages.Models.InAppMessageAction::closes_message
	bool ___closes_message_3;
};

// OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult
struct InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5  : public RuntimeObject
{
	// OneSignalSDK.InAppMessages.Models.InAppMessageAction OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult::action
	InAppMessageAction_tCE4215F332A511CE8DA5B85CC5A15D22EFB1D430* ___action_0;
};

// OneSignalSDK.Json
struct Json_tCC41D098E71C9750C8C409790108D1635F2D3BFC  : public RuntimeObject
{
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// OneSignalSDK.Notifications.Models.NotificationAction
struct NotificationAction_tCFE5BF25A39173707A04C574311BAA53CC8C1591  : public RuntimeObject
{
	// System.String OneSignalSDK.Notifications.Models.NotificationAction::actionId
	String_t* ___actionId_0;
	// OneSignalSDK.Notifications.Models.ActionType OneSignalSDK.Notifications.Models.NotificationAction::type
	int32_t ___type_1;
};

// OneSignalSDK.Notifications.Models.NotificationBase
struct NotificationBase_tEAE8E5ACEC71A0A416C4AAB702F39C734AAF3686  : public RuntimeObject
{
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::notificationId
	String_t* ___notificationId_0;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::templateName
	String_t* ___templateName_1;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::templateId
	String_t* ___templateId_2;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::title
	String_t* ___title_3;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::body
	String_t* ___body_4;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::launchURL
	String_t* ___launchURL_5;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::sound
	String_t* ___sound_6;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::collapseId
	String_t* ___collapseId_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> OneSignalSDK.Notifications.Models.NotificationBase::additionalData
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___additionalData_8;
	// System.Collections.Generic.List`1<OneSignalSDK.Notifications.Models.ActionButton> OneSignalSDK.Notifications.Models.NotificationBase::actionButtons
	List_1_t3173AEAC2CD678AC1E8802FE100BFCBB6C096FE3* ___actionButtons_9;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::rawPayload
	String_t* ___rawPayload_10;
	// System.Int32 OneSignalSDK.Notifications.Models.NotificationBase::androidNotificationId
	int32_t ___androidNotificationId_11;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::smallIcon
	String_t* ___smallIcon_12;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::largeIcon
	String_t* ___largeIcon_13;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::bigPicture
	String_t* ___bigPicture_14;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::smallIconAccentColor
	String_t* ___smallIconAccentColor_15;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::ledColor
	String_t* ___ledColor_16;
	// OneSignalSDK.Notifications.Models.LockScreenVisibility OneSignalSDK.Notifications.Models.NotificationBase::lockScreenVisibility
	int32_t ___lockScreenVisibility_17;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::groupKey
	String_t* ___groupKey_18;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::groupMessage
	String_t* ___groupMessage_19;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::fromProjectNumber
	String_t* ___fromProjectNumber_20;
	// System.Int32 OneSignalSDK.Notifications.Models.NotificationBase::priority
	int32_t ___priority_21;
	// OneSignalSDK.Notifications.Models.BackgroundImageLayout OneSignalSDK.Notifications.Models.NotificationBase::backgroundImageLayout
	BackgroundImageLayout_tD6FC701C0DDF4BD63EDB891962009F9E14E4E2F2* ___backgroundImageLayout_22;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::subtitle
	String_t* ___subtitle_23;
	// System.Boolean OneSignalSDK.Notifications.Models.NotificationBase::contentAvailable
	bool ___contentAvailable_24;
	// System.Boolean OneSignalSDK.Notifications.Models.NotificationBase::mutableContent
	bool ___mutableContent_25;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::category
	String_t* ___category_26;
	// System.Int32 OneSignalSDK.Notifications.Models.NotificationBase::badge
	int32_t ___badge_27;
	// System.Int32 OneSignalSDK.Notifications.Models.NotificationBase::badgeIncrement
	int32_t ___badgeIncrement_28;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::threadId
	String_t* ___threadId_29;
	// System.Double OneSignalSDK.Notifications.Models.NotificationBase::relevanceScore
	double ___relevanceScore_30;
	// System.String OneSignalSDK.Notifications.Models.NotificationBase::interruptionLevel
	String_t* ___interruptionLevel_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> OneSignalSDK.Notifications.Models.NotificationBase::attachments
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___attachments_32;
};

// OneSignalSDK.Notifications.Models.NotificationClickedResult
struct NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C  : public RuntimeObject
{
	// OneSignalSDK.Notifications.Models.NotificationAction OneSignalSDK.Notifications.Models.NotificationClickedResult::action
	NotificationAction_tCFE5BF25A39173707A04C574311BAA53CC8C1591* ___action_0;
	// OneSignalSDK.Notifications.Models.Notification OneSignalSDK.Notifications.Models.NotificationClickedResult::notification
	Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* ___notification_1;
};

// OneSignalSDK.OneSignal
struct OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85  : public RuntimeObject
{
};

// OneSignalSDK.User.Models.PushSubscriptionState
struct PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA  : public RuntimeObject
{
	// System.String OneSignalSDK.User.Models.PushSubscriptionState::id
	String_t* ___id_0;
	// System.Boolean OneSignalSDK.User.Models.PushSubscriptionState::optedIn
	bool ___optedIn_1;
	// System.String OneSignalSDK.User.Models.PushSubscriptionState::token
	String_t* ___token_2;
};

// OneSignalSDK.ReflectionHelpers
struct ReflectionHelpers_t8B5D5000145D2CF5596CF272724D4A7A324AE0F0  : public RuntimeObject
{
};

// OneSignalSDK.Debug.Utilities.SDKDebug
struct SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.Text.StringBuilder
struct StringBuilder_t  : public RuntimeObject
{
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t* ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;
};

// System.Threading.SynchronizationContext
struct SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0  : public RuntimeObject
{
	// System.Threading.SynchronizationContextProperties System.Threading.SynchronizationContext::_props
	int32_t ____props_0;
};

// OneSignalSDK.UnityMainThreadDispatch
struct UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12  : public RuntimeObject
{
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// OneSignalSDK.Json/Parser
struct Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C  : public RuntimeObject
{
	// System.IO.StringReader OneSignalSDK.Json/Parser::json
	StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* ___json_1;
};

// OneSignalSDK.Json/Serializer
struct Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5  : public RuntimeObject
{
	// System.Text.StringBuilder OneSignalSDK.Json/Serializer::builder
	StringBuilder_t* ___builder_0;
	// System.Boolean OneSignalSDK.Json/Serializer::humanReadable
	bool ___humanReadable_1;
	// System.Int32 OneSignalSDK.Json/Serializer::indentSpaces
	int32_t ___indentSpaces_2;
	// System.Int32 OneSignalSDK.Json/Serializer::indentLevel
	int32_t ___indentLevel_3;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// System.Decimal
struct Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Decimal::flags
			int32_t ___flags_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___flags_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___hi_9_OffsetPadding[4];
			// System.Int32 System.Decimal::hi
			int32_t ___hi_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___hi_9_OffsetPadding_forAlignmentOnly[4];
			int32_t ___hi_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___lo_10_OffsetPadding[8];
			// System.Int32 System.Decimal::lo
			int32_t ___lo_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___lo_10_OffsetPadding_forAlignmentOnly[8];
			int32_t ___lo_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___mid_11_OffsetPadding[12];
			// System.Int32 System.Decimal::mid
			int32_t ___mid_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___mid_11_OffsetPadding_forAlignmentOnly[12];
			int32_t ___mid_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___ulomidLE_12_OffsetPadding[8];
			// System.UInt64 System.Decimal::ulomidLE
			uint64_t ___ulomidLE_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___ulomidLE_12_OffsetPadding_forAlignmentOnly[8];
			uint64_t ___ulomidLE_12_forAlignmentOnly;
		};
	};
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int16
struct Int16_tB8EF286A9C33492FA6E6D6E67320BE93E794A175 
{
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// OneSignalSDK.Notifications.Models.Notification
struct Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B  : public NotificationBase_tEAE8E5ACEC71A0A416C4AAB702F39C734AAF3686
{
	// System.Collections.Generic.List`1<OneSignalSDK.Notifications.Models.NotificationBase> OneSignalSDK.Notifications.Models.Notification::groupedNotifications
	List_1_tB2A9DC3BB80251C6410CDE58170B9B566FF563DA* ___groupedNotifications_33;
};

// System.SByte
struct SByte_tFEFFEF5D2FEBF5207950AE6FAC150FC53B668DB5 
{
	// System.SByte System.SByte::m_value
	int8_t ___m_value_0;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
};

// System.UInt16
struct UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455 
{
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// System.UInt64
struct UInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF 
{
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// System.IO.StringReader
struct StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8  : public TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7
{
	// System.String System.IO.StringReader::_s
	String_t* ____s_2;
	// System.Int32 System.IO.StringReader::_pos
	int32_t ____pos_3;
	// System.Int32 System.IO.StringReader::_length
	int32_t ____length_4;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87  : public MulticastDelegate_t
{
};

// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A  : public MulticastDelegate_t
{
};

// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// OneSignalSDK.InAppMessages.InAppMessageClickedDelegate
struct InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84  : public MulticastDelegate_t
{
};

// OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate
struct InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD  : public MulticastDelegate_t
{
};

// OneSignalSDK.Notifications.NotificationClickedDelegate
struct NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332  : public MulticastDelegate_t
{
};

// OneSignalSDK.Notifications.NotificationWillShowDelegate
struct NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13  : public MulticastDelegate_t
{
};

// OneSignalSDK.Notifications.PermissionChangedDelegate
struct PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED  : public MulticastDelegate_t
{
};

// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E  : public MulticastDelegate_t
{
};

// OneSignalSDK.User.Models.SubscriptionChangedDelegate
struct SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693  : public MulticastDelegate_t
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// OneSignalSDK.OneSignalBehaviour
struct OneSignalBehaviour_t984420F12C8E067001F23C636606C6B3A395AFD7  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String OneSignalSDK.OneSignalBehaviour::AppId
	String_t* ___AppId_4;
	// OneSignalSDK.Debug.Models.LogLevel OneSignalSDK.OneSignalBehaviour::LogLevel
	int32_t ___LogLevel_5;
	// OneSignalSDK.Debug.Models.LogLevel OneSignalSDK.OneSignalBehaviour::AlertLevel
	int32_t ___AlertLevel_6;
	// System.Boolean OneSignalSDK.OneSignalBehaviour::RequiresPrivacyConsent
	bool ___RequiresPrivacyConsent_7;
	// System.Boolean OneSignalSDK.OneSignalBehaviour::IsShared
	bool ___IsShared_8;
};

// <Module>

// <Module>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>

// OneSignalSDK.Notifications.Models.ActionButton

// OneSignalSDK.Notifications.Models.ActionButton

// OneSignalSDK.Notifications.Models.BackgroundImageLayout

// OneSignalSDK.Notifications.Models.BackgroundImageLayout

// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_StaticFields
{
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject* ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_DefaultThreadCurrentUICulture_34;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_DefaultThreadCurrentCulture_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t9FA6D82CAFC18769F7515BB51D1C56DAE09381C3* ___shared_by_number_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_tE1603CE612C16451D1E56FF4D4859D4FE4087C28* ___shared_by_name_37;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::s_UserPreferredCultureInfoInAppX
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_UserPreferredCultureInfoInAppX_38;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_39;
};

// System.Globalization.CultureInfo

// OneSignalSDK.InAppMessages.Models.InAppMessage

// OneSignalSDK.InAppMessages.Models.InAppMessage

// OneSignalSDK.InAppMessages.Models.InAppMessageAction

// OneSignalSDK.InAppMessages.Models.InAppMessageAction

// OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult

// OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult

// OneSignalSDK.Json

// OneSignalSDK.Json

// OneSignalSDK.Notifications.Models.NotificationAction

// OneSignalSDK.Notifications.Models.NotificationAction

// OneSignalSDK.Notifications.Models.NotificationBase

// OneSignalSDK.Notifications.Models.NotificationBase

// OneSignalSDK.Notifications.Models.NotificationClickedResult

// OneSignalSDK.Notifications.Models.NotificationClickedResult

// OneSignalSDK.OneSignal
struct OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields
{
	// OneSignalSDK.OneSignal OneSignalSDK.OneSignal::_default
	OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* ____default_2;
	// System.Action`1<System.String> OneSignalSDK.OneSignal::OnInitialize
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___OnInitialize_3;
	// System.String OneSignalSDK.OneSignal::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_4;
	// System.Boolean OneSignalSDK.OneSignal::<DidInitialize>k__BackingField
	bool ___U3CDidInitializeU3Ek__BackingField_5;
};

// OneSignalSDK.OneSignal

// OneSignalSDK.User.Models.PushSubscriptionState

// OneSignalSDK.User.Models.PushSubscriptionState

// OneSignalSDK.ReflectionHelpers

// OneSignalSDK.ReflectionHelpers

// OneSignalSDK.Debug.Utilities.SDKDebug
struct SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields
{
	// System.Action`1<System.Object> OneSignalSDK.Debug.Utilities.SDKDebug::LogIntercept
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___LogIntercept_0;
	// System.Action`1<System.Object> OneSignalSDK.Debug.Utilities.SDKDebug::WarnIntercept
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___WarnIntercept_1;
	// System.Action`1<System.Object> OneSignalSDK.Debug.Utilities.SDKDebug::ErrorIntercept
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___ErrorIntercept_2;
};

// OneSignalSDK.Debug.Utilities.SDKDebug

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.Text.StringBuilder

// System.Text.StringBuilder

// System.Threading.SynchronizationContext

// System.Threading.SynchronizationContext

// OneSignalSDK.UnityMainThreadDispatch
struct UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_StaticFields
{
	// System.Threading.SynchronizationContext OneSignalSDK.UnityMainThreadDispatch::_context
	SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* ____context_0;
};

// OneSignalSDK.UnityMainThreadDispatch

// OneSignalSDK.Json/Parser

// OneSignalSDK.Json/Parser

// OneSignalSDK.Json/Serializer

// OneSignalSDK.Json/Serializer

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// System.Decimal
struct Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F_StaticFields
{
	// System.Decimal System.Decimal::Zero
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___Zero_3;
	// System.Decimal System.Decimal::One
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___One_4;
	// System.Decimal System.Decimal::MinusOne
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MinusOne_5;
	// System.Decimal System.Decimal::MaxValue
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MaxValue_6;
	// System.Decimal System.Decimal::MinValue
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MinValue_7;
};

// System.Decimal

// System.Double

// System.Double

// System.Int16

// System.Int16

// System.Int32

// System.Int32

// System.Int64

// System.Int64

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// OneSignalSDK.Notifications.Models.Notification

// OneSignalSDK.Notifications.Models.Notification

// System.SByte

// System.SByte

// System.Single

// System.Single

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7_StaticFields
{
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7* ___Null_1;
};

// System.IO.TextReader

// System.UInt16

// System.UInt16

// System.UInt32

// System.UInt32

// System.UInt64

// System.UInt64

// System.Void

// System.Void

// System.Delegate

// System.Delegate

// System.IO.StringReader

// System.IO.StringReader

// System.Type
struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Type

// System.Action`1<System.Object>

// System.Action`1<System.Object>

// System.Action`1<System.String>

// System.Action`1<System.String>

// System.AsyncCallback

// System.AsyncCallback

// OneSignalSDK.InAppMessages.InAppMessageClickedDelegate

// OneSignalSDK.InAppMessages.InAppMessageClickedDelegate

// OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate

// OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate

// OneSignalSDK.Notifications.NotificationClickedDelegate

// OneSignalSDK.Notifications.NotificationClickedDelegate

// OneSignalSDK.Notifications.NotificationWillShowDelegate

// OneSignalSDK.Notifications.NotificationWillShowDelegate

// OneSignalSDK.Notifications.PermissionChangedDelegate

// OneSignalSDK.Notifications.PermissionChangedDelegate

// System.Threading.SendOrPostCallback

// System.Threading.SendOrPostCallback

// OneSignalSDK.User.Models.SubscriptionChangedDelegate

// OneSignalSDK.User.Models.SubscriptionChangedDelegate

// UnityEngine.MonoBehaviour

// UnityEngine.MonoBehaviour

// OneSignalSDK.OneSignalBehaviour

// OneSignalSDK.OneSignalBehaviour
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB  : public RuntimeArray
{
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771  : public RuntimeArray
{
	ALIGN_FIELD (8) Delegate_t* m_Items[1];

	inline Delegate_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Action`1<System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<System.Type> OneSignalSDK.ReflectionHelpers::FindAllAssignableTypes<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReflectionHelpers_FindAllAssignableTypes_TisRuntimeObject_m0B0DADD391410251C9D562B58989F6358C9EF093_gshared (String_t* ___0_assemblyFilter, const RuntimeMethod* method) ;
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_First_TisRuntimeObject_mEFECF1B8C3201589C5AF34176DCBF8DD926642D6_gshared (RuntimeObject* ___0_source, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) ;

// OneSignalSDK.OneSignal OneSignalSDK.OneSignal::get_Default()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4 (const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// OneSignalSDK.OneSignal OneSignalSDK.OneSignal::_getDefaultInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* OneSignal__getDefaultInstance_m849C6811959C6FDA6953AD987EA80B119CC09584 (const RuntimeMethod* method) ;
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987 (String_t* ___0_format, RuntimeObject* ___1_arg0, RuntimeObject* ___2_arg1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00 (Delegate_t* ___0_a, Delegate_t* ___1_b, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3 (Delegate_t* ___0_source, Delegate_t* ___1_value, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.OneSignal::set_AppId(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OneSignal_set_AppId_mA7AFE519FA396A621BAED79D13C63854ADE77F7F_inline (String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.OneSignal::set_DidInitialize(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OneSignal_set_DidInitialize_m8EFFB9C9D7E997672E5EA5309160463F7961C634_inline (bool ___0_value, const RuntimeMethod* method) ;
// System.String OneSignalSDK.OneSignal::get_AppId()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OneSignal_get_AppId_mE3F51B18F55331DD5DAA72C6652E0E1EB99A12A3_inline (const RuntimeMethod* method) ;
// System.Void System.Action`1<System.String>::Invoke(T)
inline void Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* __this, String_t* ___0_obj, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*, String_t*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___0_obj, method);
}
// System.Collections.Generic.IEnumerable`1<System.Type> OneSignalSDK.ReflectionHelpers::FindAllAssignableTypes<OneSignalSDK.OneSignal>(System.String)
inline RuntimeObject* ReflectionHelpers_FindAllAssignableTypes_TisOneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_m81EA6181DA97D554BEE7ADD4C1CCD66FB43849AF (String_t* ___0_assemblyFilter, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (String_t*, const RuntimeMethod*))ReflectionHelpers_FindAllAssignableTypes_TisRuntimeObject_m0B0DADD391410251C9D562B58989F6358C9EF093_gshared)(___0_assemblyFilter, method);
}
// TSource System.Linq.Enumerable::First<System.Type>(System.Collections.Generic.IEnumerable`1<TSource>)
inline Type_t* Enumerable_First_TisType_t_m22B99E5DC993C761AE0CB9632BA5749F27E1E074 (RuntimeObject* ___0_source, const RuntimeMethod* method)
{
	return ((  Type_t* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_First_TisRuntimeObject_mEFECF1B8C3201589C5AF34176DCBF8DD926642D6_gshared)(___0_source, method);
}
// System.Object System.Activator::CreateInstance(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Activator_CreateInstance_mFF030428C64FDDFACC74DFAC97388A1C628BFBCF (Type_t* ___0_type, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::Info(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_Info_m531A4499060F346EAB9413566E49445F27E3D6CB (String_t* ___0_message, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2 (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Object OneSignalSDK.Json/Parser::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Parser_Parse_m49285B0BD066EBA61990D9DBAA900AD380E05E15 (String_t* ___0_jsonString, const RuntimeMethod* method) ;
// System.String OneSignalSDK.Json/Serializer::MakeSerialization(System.Object,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Serializer_MakeSerialization_m58B5B1531741F5A9143E9B0B70807331BEBA61CA (RuntimeObject* ___0_obj, bool ___1_humanReadable, int32_t ___2_indentSpaces, const RuntimeMethod* method) ;
// System.Boolean System.Char::IsWhiteSpace(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Char_IsWhiteSpace_m02AEC6EA19513CAFC6882CFCA54C45794D2B5924 (Il2CppChar ___0_c, const RuntimeMethod* method) ;
// System.Int32 System.String::IndexOf(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_IndexOf_mE21E78F35EF4A7768E385A72814C88D22B689966 (String_t* __this, Il2CppChar ___0_value, const RuntimeMethod* method) ;
// System.Void System.IO.StringReader::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringReader__ctor_m72556EC1062F49E05CF41B0825AC7FA2DB2A81C0 (StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* __this, String_t* ___0_s, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Json/Parser::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Parser__ctor_m9BE59139F16DFAC491D3A03DCB162B051070807C (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, String_t* ___0_jsonString, const RuntimeMethod* method) ;
// System.Object OneSignalSDK.Json/Parser::ParseValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Parser_ParseValue_m1B7E05DCE0B6435C7679A4878A0371B990764B41 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.Void System.IO.TextReader::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextReader_Dispose_mDCB332EFA06970A9CC7EC4596FCC5220B9512616 (TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor()
inline void Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9 (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// OneSignalSDK.Json/Parser/TOKEN OneSignalSDK.Json/Parser::get_NextToken()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Parser_get_NextToken_mE11FFDBF97BAC9AD22EE729FD0D55364388325D6 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.String OneSignalSDK.Json/Parser::ParseString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Parser_ParseString_mB3FF8B4C52AF5BC6279CE7DD5FD058B874E0078B (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341 (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* __this, String_t* ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*, String_t*, RuntimeObject*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
inline void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690 (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Object OneSignalSDK.Json/Parser::ParseByToken(OneSignalSDK.Json/Parser/TOKEN)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Parser_ParseByToken_mC26F86B5A9BB6DC7A8F419619B74B628F3C9F738 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, int32_t ___0_token, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
inline void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___0_item, method);
}
// System.Object OneSignalSDK.Json/Parser::ParseNumber()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Parser_ParseNumber_m2C0B8EAD7D8ED45A37E1FB2ACD5E7A8212A2C714 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> OneSignalSDK.Json/Parser::ParseObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* Parser_ParseObject_mC129607C90BA628D70512884CD7410E92EC7493D (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1<System.Object> OneSignalSDK.Json/Parser::ParseArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* Parser_ParseArray_m91F6F3E6E84BA32CF62544967F412C86A6C940F1 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_m1D99713357DE05DAFA296633639DB55F8C30587D (StringBuilder_t* __this, const RuntimeMethod* method) ;
// System.Char OneSignalSDK.Json/Parser::get_NextChar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar Parser_get_NextChar_mB5FC1831EF72916E114ECA94FBCB3D8CA1056ACF (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t* StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1 (StringBuilder_t* __this, Il2CppChar ___0_value, const RuntimeMethod* method) ;
// System.String System.String::CreateString(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mFBC28D2E3EB87D497F7E702E4FFAD65F635E44DF (String_t* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___0_val, const RuntimeMethod* method) ;
// System.Int32 System.Convert::ToInt32(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Convert_ToInt32_mD1B3AFBDA26E52D0382434804364FEF8BA241FB4 (String_t* ___0_value, int32_t ___1_fromBase, const RuntimeMethod* method) ;
// System.String OneSignalSDK.Json/Parser::get_NextWord()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Parser_get_NextWord_m523714E46B1A48A04432D11FF5FC181B9CEE638C (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6 (const RuntimeMethod* method) ;
// System.Boolean System.Int64::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.Int64&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int64_TryParse_m64CEDECE4C3F16B715CA1057801018B2957AE0E3 (String_t* ___0_s, int32_t ___1_style, RuntimeObject* ___2_provider, int64_t* ___3_result, const RuntimeMethod* method) ;
// System.Boolean System.Double::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.Double&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Double_TryParse_m1D39DC22A45BC9A576B9D9130600BFD3CB6DA382 (String_t* ___0_s, int32_t ___1_style, RuntimeObject* ___2_provider, double* ___3_result, const RuntimeMethod* method) ;
// System.Char OneSignalSDK.Json/Parser::get_PeekChar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar Parser_get_PeekChar_mA1D0A3B252CDD78BEA07DB4E6F898C66F71A7571 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.Char System.Convert::ToChar(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar Convert_ToChar_mF1B1B205DDEFDE52251235514E7DAFCAB37D1F24 (int32_t ___0_value, const RuntimeMethod* method) ;
// System.Boolean OneSignalSDK.Json/Parser::IsWordBreak(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Parser_IsWordBreak_m9D8592F6FB2502FC2B225C42DB7AC1160D8F9BE3 (Il2CppChar ___0_c, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Json/Parser::EatWhitespace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Parser_EatWhitespace_mD5146E17EF15AFE2DF0C64426301D1EE6513C0A3 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1 (String_t* ___0_a, String_t* ___1_b, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Json/Serializer::.ctor(System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer__ctor_m99E1E12C48B39230E7488D02026BF2572D0792DF (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, bool ___0_humanReadable, int32_t ___1_indentSpaces, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Json/Serializer::SerializeValue(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeValue_m05F48CBD03A0580F94083F0EC60ACC24025F9847 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t* StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D (StringBuilder_t* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Json/Serializer::SerializeString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeString_mDC5E9C2B126BF45E499655700E82D80929FA3511 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, String_t* ___0_str, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Json/Serializer::SerializeArray(System.Collections.IList)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeArray_m9D612F968FC32957BBCEA48C7554FB8B73CE6021 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, RuntimeObject* ___0_anArray, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Json/Serializer::SerializeObject(System.Collections.IDictionary)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeObject_m752864D4085775CF21F8A9866C857DCC72614491 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) ;
// System.String System.String::CreateString(System.Char,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B (String_t* __this, Il2CppChar ___0_c, int32_t ___1_count, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Json/Serializer::SerializeOther(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeOther_mFFB73E1A04876CD6A2625EDD360AAC8B30DF155A (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Text.StringBuilder System.Text.StringBuilder::AppendLine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t* StringBuilder_AppendLine_m3BC704C4E6A8531027D8C9287D0AB2AA0188AC4E (StringBuilder_t* __this, const RuntimeMethod* method) ;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t* StringBuilder_Append_mE20F6CD28FC8E8C9FD65987DBD32E6087CCE1CF3 (StringBuilder_t* __this, Il2CppChar ___0_value, int32_t ___1_repeatCount, const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Json/Serializer::AppendNewLineFunc()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, const RuntimeMethod* method) ;
// System.Char[] System.String::ToCharArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* String_ToCharArray_m0699A92AA3E744229EF29CB9D943C47DF4FE5B46 (String_t* __this, const RuntimeMethod* method) ;
// System.Int32 System.Convert::ToInt32(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Convert_ToInt32_mDBBE9318A7CCE1560974CE93F5BFED9931CF0052 (Il2CppChar ___0_value, const RuntimeMethod* method) ;
// System.String System.Int32::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m967AECC237535C552A97A80C7875E31B98496CA9 (int32_t* __this, String_t* ___0_format, const RuntimeMethod* method) ;
// System.String System.Single::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_mF468A56B3A746EFD805E0604EE7A2873DA157ADE (float* __this, String_t* ___0_format, RuntimeObject* ___1_provider, const RuntimeMethod* method) ;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t* StringBuilder_Append_m3A7D629DAA5E0E36B8A617A911E34F79AF84AE63 (StringBuilder_t* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Double System.Convert::ToDouble(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double Convert_ToDouble_m86FF4F837721833186E883102C056A35F0860EB0 (RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.String System.Double::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Double_ToString_m7E3930DDFB35B1919FE538A246A59C3FC62AF789 (double* __this, String_t* ___0_format, RuntimeObject* ___1_provider, const RuntimeMethod* method) ;
// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* SynchronizationContext_get_Current_m8DE6D3020745B7955249A2470A23EC0ECBB02A82 (const RuntimeMethod* method) ;
// System.Void OneSignalSDK.Notifications.Models.NotificationBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotificationBase__ctor_m2860AACFC6907FC61442BB28CFE72D45A3573264 (NotificationBase_tEAE8E5ACEC71A0A416C4AAB702F39C734AAF3686* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::Invoke(T)
inline void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*, RuntimeObject*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___0_obj, method);
}
// System.String OneSignalSDK.Debug.Utilities.SDKDebug::_formatMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SDKDebug__formatMessage_m0CCD6BF7BE05E151DAAE8ADD438321725DB6818F (String_t* ___0_message, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m33EF1B897E0C7C6FF538989610BFAFFEF4628CA9 (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9E3155FB84015C823606188F53B47CB44C444991 (String_t* ___0_str0, String_t* ___1_str1, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.OneSignalBehaviour::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneSignalBehaviour_Start_mD43BDABE484924BE18E2533D414CF14BE822D668 (OneSignalBehaviour_t984420F12C8E067001F23C636606C6B3A395AFD7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ILocationManager_t606C7B541F9B38F180FF09F582F1302B5FB080F9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OneSignal.Default.Debug.LogLevel               = LogLevel;
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_0;
		L_0 = OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4(NULL);
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = VirtualFuncInvoker0< RuntimeObject* >::Invoke(9 /* OneSignalSDK.Debug.IDebugManager OneSignalSDK.OneSignal::get_Debug() */, L_0);
		int32_t L_2 = __this->___LogLevel_5;
		NullCheck(L_1);
		InterfaceActionInvoker1< int32_t >::Invoke(1 /* System.Void OneSignalSDK.Debug.IDebugManager::set_LogLevel(OneSignalSDK.Debug.Models.LogLevel) */, IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var, L_1, L_2);
		// OneSignal.Default.Debug.AlertLevel             = AlertLevel;
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_3;
		L_3 = OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4(NULL);
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = VirtualFuncInvoker0< RuntimeObject* >::Invoke(9 /* OneSignalSDK.Debug.IDebugManager OneSignalSDK.OneSignal::get_Debug() */, L_3);
		int32_t L_5 = __this->___AlertLevel_6;
		NullCheck(L_4);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void OneSignalSDK.Debug.IDebugManager::set_AlertLevel(OneSignalSDK.Debug.Models.LogLevel) */, IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var, L_4, L_5);
		// OneSignal.Default.RequiresPrivacyConsent       = RequiresPrivacyConsent;
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_6;
		L_6 = OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4(NULL);
		bool L_7 = __this->___RequiresPrivacyConsent_7;
		NullCheck(L_6);
		VirtualActionInvoker1< bool >::Invoke(13 /* System.Void OneSignalSDK.OneSignal::set_RequiresPrivacyConsent(System.Boolean) */, L_6, L_7);
		// OneSignal.Default.Location.IsShared            = IsShared;
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_8;
		L_8 = OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4(NULL);
		NullCheck(L_8);
		RuntimeObject* L_9;
		L_9 = VirtualFuncInvoker0< RuntimeObject* >::Invoke(7 /* OneSignalSDK.Location.ILocationManager OneSignalSDK.OneSignal::get_Location() */, L_8);
		bool L_10 = __this->___IsShared_8;
		NullCheck(L_9);
		InterfaceActionInvoker1< bool >::Invoke(1 /* System.Void OneSignalSDK.Location.ILocationManager::set_IsShared(System.Boolean) */, ILocationManager_t606C7B541F9B38F180FF09F582F1302B5FB080F9_il2cpp_TypeInfo_var, L_9, L_10);
		// OneSignal.Default.Initialize(AppId);
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_11;
		L_11 = OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4(NULL);
		String_t* L_12 = __this->___AppId_4;
		NullCheck(L_11);
		VirtualActionInvoker1< String_t* >::Invoke(15 /* System.Void OneSignalSDK.OneSignal::Initialize(System.String) */, L_11, L_12);
		// }
		return;
	}
}
// System.Void OneSignalSDK.OneSignalBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneSignalBehaviour__ctor_mC493B1BE2CD99B4C7CDD5CEDCB93E3D0C484040F (OneSignalBehaviour_t984420F12C8E067001F23C636606C6B3A395AFD7* __this, const RuntimeMethod* method) 
{
	{
		// public LogLevel LogLevel = LogLevel.Warn;
		__this->___LogLevel_5 = 3;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// OneSignalSDK.OneSignal OneSignalSDK.OneSignal::get_Default()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4 (const RuntimeMethod* method) 
{
	{
		// get => _getDefaultInstance();
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_0;
		L_0 = OneSignal__getDefaultInstance_m849C6811959C6FDA6953AD987EA80B119CC09584(NULL);
		return L_0;
	}
}
// System.Void OneSignalSDK.OneSignal::set_Default(OneSignalSDK.OneSignal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneSignal_set_Default_m26C62F9861FF4EAAAE90D75E36E8F5DC0BABD138 (OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral94672909F51EA6867CBD4D94D2F92A0929C2009E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCBCEF9519EFE4C7006CFC5D13511B4CA84EDFAA1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UnityEngine.Debug.Log($"[OneSignal] OneSignal.Default set to platform SDK {value.GetType()}. Current version is {Version}");
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_0 = ___0_value;
		NullCheck(L_0);
		Type_t* L_1;
		L_1 = Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3(L_0, NULL);
		String_t* L_2;
		L_2 = String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987(_stringLiteral94672909F51EA6867CBD4D94D2F92A0929C2009E, L_1, _stringLiteralCBCEF9519EFE4C7006CFC5D13511B4CA84EDFAA1, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(L_2, NULL);
		// _default = value;
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_3 = ___0_value;
		((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->____default_2 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->____default_2), (void*)L_3);
		// }
		return;
	}
}
// System.Void OneSignalSDK.OneSignal::add_OnInitialize(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneSignal_add_OnInitialize_m3335E7F05B20B0DF9A5B06E0CBD22A9BCF6CB9EC (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___OnInitialize_3;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_5 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>((&((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___OnInitialize_3), L_5, L_6);
		V_0 = L_7;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_8) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void OneSignalSDK.OneSignal::remove_OnInitialize(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneSignal_remove_OnInitialize_m3B078C7A154DE3958C4050C6AF9AF21043AC951E (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___OnInitialize_3;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_5 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>((&((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___OnInitialize_3), L_5, L_6);
		V_0 = L_7;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_8) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.String OneSignalSDK.OneSignal::get_AppId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* OneSignal_get_AppId_mE3F51B18F55331DD5DAA72C6652E0E1EB99A12A3 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static string AppId { get; private set; }
		String_t* L_0 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___U3CAppIdU3Ek__BackingField_4;
		return L_0;
	}
}
// System.Void OneSignalSDK.OneSignal::set_AppId(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneSignal_set_AppId_mA7AFE519FA396A621BAED79D13C63854ADE77F7F (String_t* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static string AppId { get; private set; }
		String_t* L_0 = ___0_value;
		((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___U3CAppIdU3Ek__BackingField_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___U3CAppIdU3Ek__BackingField_4), (void*)L_0);
		return;
	}
}
// System.Boolean OneSignalSDK.OneSignal::get_DidInitialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OneSignal_get_DidInitialize_mBD975801E862BD290960CE766AC76A19F3E2225E (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static bool DidInitialize { get; private set; }
		bool L_0 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___U3CDidInitializeU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void OneSignalSDK.OneSignal::set_DidInitialize(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneSignal_set_DidInitialize_m8EFFB9C9D7E997672E5EA5309160463F7961C634 (bool ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static bool DidInitialize { get; private set; }
		bool L_0 = ___0_value;
		((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___U3CDidInitializeU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Void OneSignalSDK.OneSignal::_completedInit(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneSignal__completedInit_m891E343A785F50C38EAAB885C56487FC5B4E0F9A (String_t* ___0_appId, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B2_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* G_B1_0 = NULL;
	{
		// AppId         = appId;
		String_t* L_0 = ___0_appId;
		OneSignal_set_AppId_mA7AFE519FA396A621BAED79D13C63854ADE77F7F_inline(L_0, NULL);
		// DidInitialize = true;
		OneSignal_set_DidInitialize_m8EFFB9C9D7E997672E5EA5309160463F7961C634_inline((bool)1, NULL);
		// OnInitialize?.Invoke(AppId);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___OnInitialize_3;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		String_t* L_3;
		L_3 = OneSignal_get_AppId_mE3F51B18F55331DD5DAA72C6652E0E1EB99A12A3_inline(NULL);
		NullCheck(G_B2_0);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(G_B2_0, L_3, NULL);
		// }
		return;
	}
}
// OneSignalSDK.OneSignal OneSignalSDK.OneSignal::_getDefaultInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* OneSignal__getDefaultInstance_m849C6811959C6FDA6953AD987EA80B119CC09584 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_First_TisType_t_m22B99E5DC993C761AE0CB9632BA5749F27E1E074_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReflectionHelpers_FindAllAssignableTypes_TisOneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_m81EA6181DA97D554BEE7ADD4C1CCD66FB43849AF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral526C999F139627F5A148FB7032812E83F8AEFC82);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8DC762363E16C7B48A45F7AD0379382854A5AAC7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral91F7E6F20B6646D90CD184646336FEFB5B3FCC2F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCBCEF9519EFE4C7006CFC5D13511B4CA84EDFAA1);
		s_Il2CppMethodInitialized = true;
	}
	OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* V_0 = NULL;
	{
		// if (_default != null)
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_0 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->____default_2;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		// return _default;
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_1 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->____default_2;
		return L_1;
	}

IL_000d:
	{
		// var availableSDKs = ReflectionHelpers.FindAllAssignableTypes<OneSignal>("OneSignal");
		RuntimeObject* L_2;
		L_2 = ReflectionHelpers_FindAllAssignableTypes_TisOneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_m81EA6181DA97D554BEE7ADD4C1CCD66FB43849AF(_stringLiteral91F7E6F20B6646D90CD184646336FEFB5B3FCC2F, ReflectionHelpers_FindAllAssignableTypes_TisOneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_m81EA6181DA97D554BEE7ADD4C1CCD66FB43849AF_RuntimeMethod_var);
		// if (Activator.CreateInstance(availableSDKs.First()) is OneSignal sdk) {
		Type_t* L_3;
		L_3 = Enumerable_First_TisType_t_m22B99E5DC993C761AE0CB9632BA5749F27E1E074(L_2, Enumerable_First_TisType_t_m22B99E5DC993C761AE0CB9632BA5749F27E1E074_RuntimeMethod_var);
		RuntimeObject* L_4;
		L_4 = Activator_CreateInstance_mFF030428C64FDDFACC74DFAC97388A1C628BFBCF(L_3, NULL);
		V_0 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85*)IsInstClass((RuntimeObject*)L_4, OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var));
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_5 = V_0;
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		// _default = sdk;
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_6 = V_0;
		((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->____default_2 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->____default_2), (void*)L_6);
		// SDKDebug.Info($"OneSignal.Default set to platform SDK {sdk.GetType()}. Current version is {Version}");
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_7 = V_0;
		NullCheck(L_7);
		Type_t* L_8;
		L_8 = Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3(L_7, NULL);
		String_t* L_9;
		L_9 = String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987(_stringLiteral8DC762363E16C7B48A45F7AD0379382854A5AAC7, L_8, _stringLiteralCBCEF9519EFE4C7006CFC5D13511B4CA84EDFAA1, NULL);
		SDKDebug_Info_m531A4499060F346EAB9413566E49445F27E3D6CB(L_9, NULL);
		goto IL_0056;
	}

IL_004c:
	{
		// UnityEngine.Debug.LogError("[OneSignal] Could not find an implementation of OneSignal SDK to use!");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(_stringLiteral526C999F139627F5A148FB7032812E83F8AEFC82, NULL);
	}

IL_0056:
	{
		// return _default;
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_10 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->____default_2;
		return L_10;
	}
}
// System.Void OneSignalSDK.OneSignal::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneSignal__ctor_m182500A027110E66DD2443D6E8A22C96AA03F9DB (OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object OneSignalSDK.Json::Deserialize(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Json_Deserialize_mF4067D3E8C01190CACA3166B8D9EF7E9A109259A (String_t* ___0_json, const RuntimeMethod* method) 
{
	{
		// if (json == null)
		String_t* L_0 = ___0_json;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		// return null;
		return NULL;
	}

IL_0005:
	{
		// return Parser.Parse(json);
		String_t* L_1 = ___0_json;
		RuntimeObject* L_2;
		L_2 = Parser_Parse_m49285B0BD066EBA61990D9DBAA900AD380E05E15(L_1, NULL);
		return L_2;
	}
}
// System.String OneSignalSDK.Json::Serialize(System.Object,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Json_Serialize_m35E0717344D0105ACDDFDCE3BDB49792D100EB81 (RuntimeObject* ___0_obj, bool ___1_humanReadable, int32_t ___2_indentSpaces, const RuntimeMethod* method) 
{
	{
		// return Serializer.MakeSerialization(obj, humanReadable, indentSpaces);
		RuntimeObject* L_0 = ___0_obj;
		bool L_1 = ___1_humanReadable;
		int32_t L_2 = ___2_indentSpaces;
		String_t* L_3;
		L_3 = Serializer_MakeSerialization_m58B5B1531741F5A9143E9B0B70807331BEBA61CA(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean OneSignalSDK.Json/Parser::IsWordBreak(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Parser_IsWordBreak_m9D8592F6FB2502FC2B225C42DB7AC1160D8F9BE3 (Il2CppChar ___0_c, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral84C1E07F84B6E7BDCC02A904AFEC3BBD2CAE6EAA);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Char.IsWhiteSpace(c) || WORD_BREAK.IndexOf(c) != -1;
		Il2CppChar L_0 = ___0_c;
		il2cpp_codegen_runtime_class_init_inline(Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Char_IsWhiteSpace_m02AEC6EA19513CAFC6882CFCA54C45794D2B5924(L_0, NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		Il2CppChar L_2 = ___0_c;
		NullCheck(_stringLiteral84C1E07F84B6E7BDCC02A904AFEC3BBD2CAE6EAA);
		int32_t L_3;
		L_3 = String_IndexOf_mE21E78F35EF4A7768E385A72814C88D22B689966(_stringLiteral84C1E07F84B6E7BDCC02A904AFEC3BBD2CAE6EAA, L_2, NULL);
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_001a:
	{
		return (bool)1;
	}
}
// System.Void OneSignalSDK.Json/Parser::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Parser__ctor_m9BE59139F16DFAC491D3A03DCB162B051070807C (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, String_t* ___0_jsonString, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Parser(string jsonString)
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// json = new StringReader(jsonString);
		String_t* L_0 = ___0_jsonString;
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_1 = (StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8*)il2cpp_codegen_object_new(StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		StringReader__ctor_m72556EC1062F49E05CF41B0825AC7FA2DB2A81C0(L_1, L_0, NULL);
		__this->___json_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___json_1), (void*)L_1);
		// }
		return;
	}
}
// System.Object OneSignalSDK.Json/Parser::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Parser_Parse_m49285B0BD066EBA61990D9DBAA900AD380E05E15 (String_t* ___0_jsonString, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		// using (var instance = new Parser(jsonString))
		String_t* L_0 = ___0_jsonString;
		Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* L_1 = (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C*)il2cpp_codegen_object_new(Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		Parser__ctor_m9BE59139F16DFAC491D3A03DCB162B051070807C(L_1, L_0, NULL);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0010:
			{// begin finally (depth: 1)
				{
					Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* L_2 = V_0;
					if (!L_2)
					{
						goto IL_0019;
					}
				}
				{
					Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* L_3 = V_0;
					NullCheck(L_3);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_3);
				}

IL_0019:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			// return instance.ParseValue();
			Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* L_4 = V_0;
			NullCheck(L_4);
			RuntimeObject* L_5;
			L_5 = Parser_ParseValue_m1B7E05DCE0B6435C7679A4878A0371B990764B41(L_4, NULL);
			V_1 = L_5;
			goto IL_001a;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_001a:
	{
		// }
		RuntimeObject* L_6 = V_1;
		return L_6;
	}
}
// System.Void OneSignalSDK.Json/Parser::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Parser_Dispose_mC393F1F7B2BE6DAD7B54A9B07175D06683004616 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	{
		// json.Dispose();
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_0 = __this->___json_1;
		NullCheck(L_0);
		TextReader_Dispose_mDCB332EFA06970A9CC7EC4596FCC5220B9512616(L_0, NULL);
		// json = null;
		__this->___json_1 = (StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___json_1), (void*)(StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8*)NULL);
		// }
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> OneSignalSDK.Json/Parser::ParseObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* Parser_ParseObject_mC129607C90BA628D70512884CD7410E92EC7493D (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		// Dictionary<string, object> table = new Dictionary<string, object>();
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_0 = (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*)il2cpp_codegen_object_new(Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9(L_0, Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9_RuntimeMethod_var);
		V_0 = L_0;
		// json.Read();
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_1 = __this->___json_1;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_1);
	}

IL_0012:
	{
		// switch (NextToken)
		int32_t L_3;
		L_3 = Parser_get_NextToken_mE11FFDBF97BAC9AD22EE729FD0D55364388325D6(__this, NULL);
		V_2 = L_3;
		int32_t L_4 = V_2;
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)2)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) == ((int32_t)6)))
		{
			goto IL_0012;
		}
	}
	{
		goto IL_002a;
	}

IL_0026:
	{
		// return null;
		return (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*)NULL;
	}

IL_0028:
	{
		// return table;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_7 = V_0;
		return L_7;
	}

IL_002a:
	{
		// string name = ParseString();
		String_t* L_8;
		L_8 = Parser_ParseString_mB3FF8B4C52AF5BC6279CE7DD5FD058B874E0078B(__this, NULL);
		V_1 = L_8;
		// if (name == null)
		String_t* L_9 = V_1;
		if (L_9)
		{
			goto IL_0036;
		}
	}
	{
		// return null;
		return (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*)NULL;
	}

IL_0036:
	{
		// if (NextToken != TOKEN.COLON)
		int32_t L_10;
		L_10 = Parser_get_NextToken_mE11FFDBF97BAC9AD22EE729FD0D55364388325D6(__this, NULL);
		if ((((int32_t)L_10) == ((int32_t)5)))
		{
			goto IL_0041;
		}
	}
	{
		// return null;
		return (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*)NULL;
	}

IL_0041:
	{
		// json.Read();
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_11 = __this->___json_1;
		NullCheck(L_11);
		int32_t L_12;
		L_12 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_11);
		// table[name] = ParseValue();
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_13 = V_0;
		String_t* L_14 = V_1;
		RuntimeObject* L_15;
		L_15 = Parser_ParseValue_m1B7E05DCE0B6435C7679A4878A0371B990764B41(__this, NULL);
		NullCheck(L_13);
		Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341(L_13, L_14, L_15, Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341_RuntimeMethod_var);
		// break;
		goto IL_0012;
	}
}
// System.Collections.Generic.List`1<System.Object> OneSignalSDK.Json/Parser::ParseArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* Parser_ParseArray_m91F6F3E6E84BA32CF62544967F412C86A6C940F1 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	RuntimeObject* V_3 = NULL;
	{
		// List<object> array = new List<object>();
		List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* L_0 = (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*)il2cpp_codegen_object_new(List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690(L_0, List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_RuntimeMethod_var);
		V_0 = L_0;
		// json.Read();
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_1 = __this->___json_1;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_1);
		// var parsing = true;
		V_1 = (bool)1;
		goto IL_003f;
	}

IL_0016:
	{
		// TOKEN nextToken = NextToken;
		int32_t L_3;
		L_3 = Parser_get_NextToken_mE11FFDBF97BAC9AD22EE729FD0D55364388325D6(__this, NULL);
		V_2 = L_3;
		int32_t L_4 = V_2;
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)4)))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) == ((int32_t)6)))
		{
			goto IL_003f;
		}
	}
	{
		goto IL_0030;
	}

IL_002a:
	{
		// return null;
		return (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*)NULL;
	}

IL_002c:
	{
		// parsing = false;
		V_1 = (bool)0;
		// break;
		goto IL_003f;
	}

IL_0030:
	{
		// object value = ParseByToken(nextToken);
		int32_t L_7 = V_2;
		RuntimeObject* L_8;
		L_8 = Parser_ParseByToken_mC26F86B5A9BB6DC7A8F419619B74B628F3C9F738(__this, L_7, NULL);
		V_3 = L_8;
		// array.Add(value);
		List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* L_9 = V_0;
		RuntimeObject* L_10 = V_3;
		NullCheck(L_9);
		List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_inline(L_9, L_10, List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_RuntimeMethod_var);
	}

IL_003f:
	{
		// while (parsing)
		bool L_11 = V_1;
		if (L_11)
		{
			goto IL_0016;
		}
	}
	{
		// return array;
		List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* L_12 = V_0;
		return L_12;
	}
}
// System.Object OneSignalSDK.Json/Parser::ParseValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Parser_ParseValue_m1B7E05DCE0B6435C7679A4878A0371B990764B41 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// TOKEN nextToken = NextToken;
		int32_t L_0;
		L_0 = Parser_get_NextToken_mE11FFDBF97BAC9AD22EE729FD0D55364388325D6(__this, NULL);
		V_0 = L_0;
		// return ParseByToken(nextToken);
		int32_t L_1 = V_0;
		RuntimeObject* L_2;
		L_2 = Parser_ParseByToken_mC26F86B5A9BB6DC7A8F419619B74B628F3C9F738(__this, L_1, NULL);
		return L_2;
	}
}
// System.Object OneSignalSDK.Json/Parser::ParseByToken(OneSignalSDK.Json/Parser/TOKEN)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Parser_ParseByToken_mC26F86B5A9BB6DC7A8F419619B74B628F3C9F738 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, int32_t ___0_token, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___0_token;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_0, 1)))
		{
			case 0:
			{
				goto IL_0044;
			}
			case 1:
			{
				goto IL_0062;
			}
			case 2:
			{
				goto IL_004b;
			}
			case 3:
			{
				goto IL_0062;
			}
			case 4:
			{
				goto IL_0062;
			}
			case 5:
			{
				goto IL_0062;
			}
			case 6:
			{
				goto IL_0036;
			}
			case 7:
			{
				goto IL_003d;
			}
			case 8:
			{
				goto IL_0052;
			}
			case 9:
			{
				goto IL_0059;
			}
			case 10:
			{
				goto IL_0060;
			}
		}
	}
	{
		goto IL_0062;
	}

IL_0036:
	{
		// return ParseString();
		String_t* L_1;
		L_1 = Parser_ParseString_mB3FF8B4C52AF5BC6279CE7DD5FD058B874E0078B(__this, NULL);
		return L_1;
	}

IL_003d:
	{
		// return ParseNumber();
		RuntimeObject* L_2;
		L_2 = Parser_ParseNumber_m2C0B8EAD7D8ED45A37E1FB2ACD5E7A8212A2C714(__this, NULL);
		return L_2;
	}

IL_0044:
	{
		// return ParseObject();
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_3;
		L_3 = Parser_ParseObject_mC129607C90BA628D70512884CD7410E92EC7493D(__this, NULL);
		return L_3;
	}

IL_004b:
	{
		// return ParseArray();
		List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* L_4;
		L_4 = Parser_ParseArray_m91F6F3E6E84BA32CF62544967F412C86A6C940F1(__this, NULL);
		return L_4;
	}

IL_0052:
	{
		// return true;
		bool L_5 = ((bool)1);
		RuntimeObject* L_6 = Box(Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var, &L_5);
		return L_6;
	}

IL_0059:
	{
		// return false;
		bool L_7 = ((bool)0);
		RuntimeObject* L_8 = Box(Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}

IL_0060:
	{
		// return null;
		return NULL;
	}

IL_0062:
	{
		// return null;
		return NULL;
	}
}
// System.String OneSignalSDK.Json/Parser::ParseString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Parser_ParseString_mB3FF8B4C52AF5BC6279CE7DD5FD058B874E0078B (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringBuilder_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t* V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* V_3 = NULL;
	int32_t V_4 = 0;
	{
		// StringBuilder s = new StringBuilder();
		StringBuilder_t* L_0 = (StringBuilder_t*)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		StringBuilder__ctor_m1D99713357DE05DAFA296633639DB55F8C30587D(L_0, NULL);
		V_0 = L_0;
		// json.Read();
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_1 = __this->___json_1;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_1);
		// bool parsing = true;
		V_2 = (bool)1;
		goto IL_013d;
	}

IL_0019:
	{
		// if (json.Peek() == -1)
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_3 = __this->___json_1;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_3);
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_0143;
		}
	}
	{
		// c = NextChar;
		Il2CppChar L_5;
		L_5 = Parser_get_NextChar_mB5FC1831EF72916E114ECA94FBCB3D8CA1056ACF(__this, NULL);
		V_1 = L_5;
		Il2CppChar L_6 = V_1;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)34))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppChar L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)92))))
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0135;
	}

IL_0040:
	{
		// parsing = false;
		V_2 = (bool)0;
		// break;
		goto IL_013d;
	}

IL_0047:
	{
		// if (json.Peek() == -1)
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_8 = __this->___json_1;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_8);
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_005c;
		}
	}
	{
		// parsing = false;
		V_2 = (bool)0;
		// break;
		goto IL_013d;
	}

IL_005c:
	{
		// c = NextChar;
		Il2CppChar L_10;
		L_10 = Parser_get_NextChar_mB5FC1831EF72916E114ECA94FBCB3D8CA1056ACF(__this, NULL);
		V_1 = L_10;
		Il2CppChar L_11 = V_1;
		if ((!(((uint32_t)L_11) <= ((uint32_t)((int32_t)92)))))
		{
			goto IL_007c;
		}
	}
	{
		Il2CppChar L_12 = V_1;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)34))))
		{
			goto IL_00b0;
		}
	}
	{
		Il2CppChar L_13 = V_1;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)47))))
		{
			goto IL_00b0;
		}
	}
	{
		Il2CppChar L_14 = V_1;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)92))))
		{
			goto IL_00b0;
		}
	}
	{
		goto IL_012b;
	}

IL_007c:
	{
		Il2CppChar L_15 = V_1;
		if ((!(((uint32_t)L_15) <= ((uint32_t)((int32_t)102)))))
		{
			goto IL_0090;
		}
	}
	{
		Il2CppChar L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)98))))
		{
			goto IL_00bd;
		}
	}
	{
		Il2CppChar L_17 = V_1;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)102))))
		{
			goto IL_00c7;
		}
	}
	{
		goto IL_012b;
	}

IL_0090:
	{
		Il2CppChar L_18 = V_1;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)110))))
		{
			goto IL_00d2;
		}
	}
	{
		Il2CppChar L_19 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_19, ((int32_t)114))))
		{
			case 0:
			{
				goto IL_00dd;
			}
			case 1:
			{
				goto IL_012b;
			}
			case 2:
			{
				goto IL_00e8;
			}
			case 3:
			{
				goto IL_00f3;
			}
		}
	}
	{
		goto IL_012b;
	}

IL_00b0:
	{
		// s.Append(c);
		StringBuilder_t* L_20 = V_0;
		Il2CppChar L_21 = V_1;
		NullCheck(L_20);
		StringBuilder_t* L_22;
		L_22 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_20, L_21, NULL);
		// break;
		goto IL_013d;
	}

IL_00bd:
	{
		// s.Append('\b');
		StringBuilder_t* L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_t* L_24;
		L_24 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_23, 8, NULL);
		// break;
		goto IL_013d;
	}

IL_00c7:
	{
		// s.Append('\f');
		StringBuilder_t* L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_t* L_26;
		L_26 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_25, ((int32_t)12), NULL);
		// break;
		goto IL_013d;
	}

IL_00d2:
	{
		// s.Append('\n');
		StringBuilder_t* L_27 = V_0;
		NullCheck(L_27);
		StringBuilder_t* L_28;
		L_28 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_27, ((int32_t)10), NULL);
		// break;
		goto IL_013d;
	}

IL_00dd:
	{
		// s.Append('\r');
		StringBuilder_t* L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_t* L_30;
		L_30 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_29, ((int32_t)13), NULL);
		// break;
		goto IL_013d;
	}

IL_00e8:
	{
		// s.Append('\t');
		StringBuilder_t* L_31 = V_0;
		NullCheck(L_31);
		StringBuilder_t* L_32;
		L_32 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_31, ((int32_t)9), NULL);
		// break;
		goto IL_013d;
	}

IL_00f3:
	{
		// var hex = new char[4];
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_33 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)4);
		V_3 = L_33;
		// for (int i = 0; i < 4; i++)
		V_4 = 0;
		goto IL_010f;
	}

IL_00ff:
	{
		// hex[i] = NextChar;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_34 = V_3;
		int32_t L_35 = V_4;
		Il2CppChar L_36;
		L_36 = Parser_get_NextChar_mB5FC1831EF72916E114ECA94FBCB3D8CA1056ACF(__this, NULL);
		NullCheck(L_34);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(L_35), (Il2CppChar)L_36);
		// for (int i = 0; i < 4; i++)
		int32_t L_37 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_37, 1));
	}

IL_010f:
	{
		// for (int i = 0; i < 4; i++)
		int32_t L_38 = V_4;
		if ((((int32_t)L_38) < ((int32_t)4)))
		{
			goto IL_00ff;
		}
	}
	{
		// s.Append((char) Convert.ToInt32(new string(hex), 16));
		StringBuilder_t* L_39 = V_0;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_40 = V_3;
		String_t* L_41;
		L_41 = String_CreateString_mFBC28D2E3EB87D497F7E702E4FFAD65F635E44DF(NULL, L_40, NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		int32_t L_42;
		L_42 = Convert_ToInt32_mD1B3AFBDA26E52D0382434804364FEF8BA241FB4(L_41, ((int32_t)16), NULL);
		NullCheck(L_39);
		StringBuilder_t* L_43;
		L_43 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_39, ((int32_t)(uint16_t)L_42), NULL);
		// break;
		goto IL_013d;
	}

IL_012b:
	{
		// s.Append(c);
		StringBuilder_t* L_44 = V_0;
		Il2CppChar L_45 = V_1;
		NullCheck(L_44);
		StringBuilder_t* L_46;
		L_46 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_44, L_45, NULL);
		// break;
		goto IL_013d;
	}

IL_0135:
	{
		// s.Append(c);
		StringBuilder_t* L_47 = V_0;
		Il2CppChar L_48 = V_1;
		NullCheck(L_47);
		StringBuilder_t* L_49;
		L_49 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_47, L_48, NULL);
	}

IL_013d:
	{
		// while (parsing)
		bool L_50 = V_2;
		if (L_50)
		{
			goto IL_0019;
		}
	}

IL_0143:
	{
		// return s.ToString();
		StringBuilder_t* L_51 = V_0;
		NullCheck(L_51);
		String_t* L_52;
		L_52 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_51);
		return L_52;
	}
}
// System.Object OneSignalSDK.Json/Parser::ParseNumber()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Parser_ParseNumber_m2C0B8EAD7D8ED45A37E1FB2ACD5E7A8212A2C714 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	double V_1 = 0.0;
	int64_t V_2 = 0;
	{
		// string number = NextWord;
		String_t* L_0;
		L_0 = Parser_get_NextWord_m523714E46B1A48A04432D11FF5FC181B9CEE638C(__this, NULL);
		V_0 = L_0;
		// if (number.IndexOf('.') == -1 && number.IndexOf('E') == -1 && number.IndexOf('e') == -1)
		String_t* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = String_IndexOf_mE21E78F35EF4A7768E385A72814C88D22B689966(L_1, ((int32_t)46), NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = String_IndexOf_mE21E78F35EF4A7768E385A72814C88D22B689966(L_3, ((int32_t)69), NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = String_IndexOf_mE21E78F35EF4A7768E385A72814C88D22B689966(L_5, ((int32_t)101), NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
		{
			goto IL_0042;
		}
	}
	{
		// Int64.TryParse(number, System.Globalization.NumberStyles.Any,
		//     System.Globalization.CultureInfo.InvariantCulture, out parsedInt);
		String_t* L_7 = V_0;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_8;
		L_8 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		bool L_9;
		L_9 = Int64_TryParse_m64CEDECE4C3F16B715CA1057801018B2957AE0E3(L_7, ((int32_t)511), L_8, (&V_2), NULL);
		// return parsedInt;
		int64_t L_10 = V_2;
		int64_t L_11 = L_10;
		RuntimeObject* L_12 = Box(Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var, &L_11);
		return L_12;
	}

IL_0042:
	{
		// Double.TryParse(number, System.Globalization.NumberStyles.Any,
		//     System.Globalization.CultureInfo.InvariantCulture, out parsedDouble);
		String_t* L_13 = V_0;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_14;
		L_14 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		bool L_15;
		L_15 = Double_TryParse_m1D39DC22A45BC9A576B9D9130600BFD3CB6DA382(L_13, ((int32_t)511), L_14, (&V_1), NULL);
		// return parsedDouble;
		double L_16 = V_1;
		double L_17 = L_16;
		RuntimeObject* L_18 = Box(Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_il2cpp_TypeInfo_var, &L_17);
		return L_18;
	}
}
// System.Void OneSignalSDK.Json/Parser::EatWhitespace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Parser_EatWhitespace_mD5146E17EF15AFE2DF0C64426301D1EE6513C0A3 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_001c;
	}

IL_0002:
	{
		// json.Read();
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_0 = __this->___json_1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_0);
		// if (json.Peek() == -1)
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_2 = __this->___json_1;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_2);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0029;
		}
	}

IL_001c:
	{
		// while (Char.IsWhiteSpace(PeekChar))
		Il2CppChar L_4;
		L_4 = Parser_get_PeekChar_mA1D0A3B252CDD78BEA07DB4E6F898C66F71A7571(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Char_IsWhiteSpace_m02AEC6EA19513CAFC6882CFCA54C45794D2B5924(L_4, NULL);
		if (L_5)
		{
			goto IL_0002;
		}
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Char OneSignalSDK.Json/Parser::get_PeekChar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar Parser_get_PeekChar_mA1D0A3B252CDD78BEA07DB4E6F898C66F71A7571 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return Convert.ToChar(json.Peek()); }
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_0 = __this->___json_1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_0);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		Il2CppChar L_2;
		L_2 = Convert_ToChar_mF1B1B205DDEFDE52251235514E7DAFCAB37D1F24(L_1, NULL);
		return L_2;
	}
}
// System.Char OneSignalSDK.Json/Parser::get_NextChar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar Parser_get_NextChar_mB5FC1831EF72916E114ECA94FBCB3D8CA1056ACF (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return Convert.ToChar(json.Read()); }
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_0 = __this->___json_1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_0);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		Il2CppChar L_2;
		L_2 = Convert_ToChar_mF1B1B205DDEFDE52251235514E7DAFCAB37D1F24(L_1, NULL);
		return L_2;
	}
}
// System.String OneSignalSDK.Json/Parser::get_NextWord()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Parser_get_NextWord_m523714E46B1A48A04432D11FF5FC181B9CEE638C (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringBuilder_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t* V_0 = NULL;
	{
		// StringBuilder word = new StringBuilder();
		StringBuilder_t* L_0 = (StringBuilder_t*)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		StringBuilder__ctor_m1D99713357DE05DAFA296633639DB55F8C30587D(L_0, NULL);
		V_0 = L_0;
		goto IL_0023;
	}

IL_0008:
	{
		// word.Append(NextChar);
		StringBuilder_t* L_1 = V_0;
		Il2CppChar L_2;
		L_2 = Parser_get_NextChar_mB5FC1831EF72916E114ECA94FBCB3D8CA1056ACF(__this, NULL);
		NullCheck(L_1);
		StringBuilder_t* L_3;
		L_3 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_1, L_2, NULL);
		// if (json.Peek() == -1)
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_4 = __this->___json_1;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_4);
		if ((((int32_t)L_5) == ((int32_t)(-1))))
		{
			goto IL_0030;
		}
	}

IL_0023:
	{
		// while (!IsWordBreak(PeekChar))
		Il2CppChar L_6;
		L_6 = Parser_get_PeekChar_mA1D0A3B252CDD78BEA07DB4E6F898C66F71A7571(__this, NULL);
		bool L_7;
		L_7 = Parser_IsWordBreak_m9D8592F6FB2502FC2B225C42DB7AC1160D8F9BE3(L_6, NULL);
		if (!L_7)
		{
			goto IL_0008;
		}
	}

IL_0030:
	{
		// return word.ToString();
		StringBuilder_t* L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9;
		L_9 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		return L_9;
	}
}
// OneSignalSDK.Json/Parser/TOKEN OneSignalSDK.Json/Parser::get_NextToken()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Parser_get_NextToken_mE11FFDBF97BAC9AD22EE729FD0D55364388325D6 (Parser_t6F2C17AE4B45A0B404D1881D47B2AA8B2981D38C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral77D38C0623F92B292B925F6E72CF5CF99A20D4EB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB7C45DD316C68ABF3429C20058C2981C652192F2);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	String_t* V_1 = NULL;
	{
		// EatWhitespace();
		Parser_EatWhitespace_mD5146E17EF15AFE2DF0C64426301D1EE6513C0A3(__this, NULL);
		// if (json.Peek() == -1)
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_0 = __this->___json_1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0016;
		}
	}
	{
		// return TOKEN.NONE;
		return (int32_t)(0);
	}

IL_0016:
	{
		// switch (PeekChar)
		Il2CppChar L_2;
		L_2 = Parser_get_PeekChar_mA1D0A3B252CDD78BEA07DB4E6F898C66F71A7571(__this, NULL);
		V_0 = L_2;
		Il2CppChar L_3 = V_0;
		if ((!(((uint32_t)L_3) <= ((uint32_t)((int32_t)91)))))
		{
			goto IL_0096;
		}
	}
	{
		Il2CppChar L_4 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_4, ((int32_t)34))))
		{
			case 0:
			{
				goto IL_00d5;
			}
			case 1:
			{
				goto IL_00db;
			}
			case 2:
			{
				goto IL_00db;
			}
			case 3:
			{
				goto IL_00db;
			}
			case 4:
			{
				goto IL_00db;
			}
			case 5:
			{
				goto IL_00db;
			}
			case 6:
			{
				goto IL_00db;
			}
			case 7:
			{
				goto IL_00db;
			}
			case 8:
			{
				goto IL_00db;
			}
			case 9:
			{
				goto IL_00db;
			}
			case 10:
			{
				goto IL_00c7;
			}
			case 11:
			{
				goto IL_00d9;
			}
			case 12:
			{
				goto IL_00db;
			}
			case 13:
			{
				goto IL_00db;
			}
			case 14:
			{
				goto IL_00d9;
			}
			case 15:
			{
				goto IL_00d9;
			}
			case 16:
			{
				goto IL_00d9;
			}
			case 17:
			{
				goto IL_00d9;
			}
			case 18:
			{
				goto IL_00d9;
			}
			case 19:
			{
				goto IL_00d9;
			}
			case 20:
			{
				goto IL_00d9;
			}
			case 21:
			{
				goto IL_00d9;
			}
			case 22:
			{
				goto IL_00d9;
			}
			case 23:
			{
				goto IL_00d9;
			}
			case 24:
			{
				goto IL_00d7;
			}
		}
	}
	{
		Il2CppChar L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)91))))
		{
			goto IL_00b7;
		}
	}
	{
		goto IL_00db;
	}

IL_0096:
	{
		Il2CppChar L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)93))))
		{
			goto IL_00b9;
		}
	}
	{
		Il2CppChar L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)123))))
		{
			goto IL_00a7;
		}
	}
	{
		Il2CppChar L_8 = V_0;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)125))))
		{
			goto IL_00a9;
		}
	}
	{
		goto IL_00db;
	}

IL_00a7:
	{
		// return TOKEN.CURLY_OPEN;
		return (int32_t)(1);
	}

IL_00a9:
	{
		// json.Read();
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_9 = __this->___json_1;
		NullCheck(L_9);
		int32_t L_10;
		L_10 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_9);
		// return TOKEN.CURLY_CLOSE;
		return (int32_t)(2);
	}

IL_00b7:
	{
		// return TOKEN.SQUARED_OPEN;
		return (int32_t)(3);
	}

IL_00b9:
	{
		// json.Read();
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_11 = __this->___json_1;
		NullCheck(L_11);
		int32_t L_12;
		L_12 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_11);
		// return TOKEN.SQUARED_CLOSE;
		return (int32_t)(4);
	}

IL_00c7:
	{
		// json.Read();
		StringReader_t1A336148FF22A9584E759A9D720CC96C23E35DD8* L_13 = __this->___json_1;
		NullCheck(L_13);
		int32_t L_14;
		L_14 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_13);
		// return TOKEN.COMMA;
		return (int32_t)(6);
	}

IL_00d5:
	{
		// return TOKEN.STRING;
		return (int32_t)(7);
	}

IL_00d7:
	{
		// return TOKEN.COLON;
		return (int32_t)(5);
	}

IL_00d9:
	{
		// return TOKEN.NUMBER;
		return (int32_t)(8);
	}

IL_00db:
	{
		// switch (NextWord)
		String_t* L_15;
		L_15 = Parser_get_NextWord_m523714E46B1A48A04432D11FF5FC181B9CEE638C(__this, NULL);
		V_1 = L_15;
		String_t* L_16 = V_1;
		bool L_17;
		L_17 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_16, _stringLiteral77D38C0623F92B292B925F6E72CF5CF99A20D4EB, NULL);
		if (L_17)
		{
			goto IL_010b;
		}
	}
	{
		String_t* L_18 = V_1;
		bool L_19;
		L_19 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_18, _stringLiteralB7C45DD316C68ABF3429C20058C2981C652192F2, NULL);
		if (L_19)
		{
			goto IL_010e;
		}
	}
	{
		String_t* L_20 = V_1;
		bool L_21;
		L_21 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_20, _stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174, NULL);
		if (L_21)
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0114;
	}

IL_010b:
	{
		// return TOKEN.FALSE;
		return (int32_t)(((int32_t)10));
	}

IL_010e:
	{
		// return TOKEN.TRUE;
		return (int32_t)(((int32_t)9));
	}

IL_0111:
	{
		// return TOKEN.NULL;
		return (int32_t)(((int32_t)11));
	}

IL_0114:
	{
		// return TOKEN.NONE;
		return (int32_t)(0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.Json/Serializer::.ctor(System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer__ctor_m99E1E12C48B39230E7488D02026BF2572D0792DF (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, bool ___0_humanReadable, int32_t ___1_indentSpaces, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringBuilder_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Serializer(bool humanReadable, int indentSpaces)
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// builder = new StringBuilder();
		StringBuilder_t* L_0 = (StringBuilder_t*)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		StringBuilder__ctor_m1D99713357DE05DAFA296633639DB55F8C30587D(L_0, NULL);
		__this->___builder_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___builder_0), (void*)L_0);
		// this.humanReadable = humanReadable;
		bool L_1 = ___0_humanReadable;
		__this->___humanReadable_1 = L_1;
		// this.indentSpaces = indentSpaces;
		int32_t L_2 = ___1_indentSpaces;
		__this->___indentSpaces_2 = L_2;
		// indentLevel = 0;
		__this->___indentLevel_3 = 0;
		// }
		return;
	}
}
// System.String OneSignalSDK.Json/Serializer::MakeSerialization(System.Object,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Serializer_MakeSerialization_m58B5B1531741F5A9143E9B0B70807331BEBA61CA (RuntimeObject* ___0_obj, bool ___1_humanReadable, int32_t ___2_indentSpaces, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var instance = new Serializer(humanReadable, indentSpaces);
		bool L_0 = ___1_humanReadable;
		int32_t L_1 = ___2_indentSpaces;
		Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* L_2 = (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5*)il2cpp_codegen_object_new(Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Serializer__ctor_m99E1E12C48B39230E7488D02026BF2572D0792DF(L_2, L_0, L_1, NULL);
		// instance.SerializeValue(obj);
		Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* L_3 = L_2;
		RuntimeObject* L_4 = ___0_obj;
		NullCheck(L_3);
		Serializer_SerializeValue_m05F48CBD03A0580F94083F0EC60ACC24025F9847(L_3, L_4, NULL);
		// return instance.builder.ToString();
		NullCheck(L_3);
		StringBuilder_t* L_5 = L_3->___builder_0;
		NullCheck(L_5);
		String_t* L_6;
		L_6 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		return L_6;
	}
}
// System.Void OneSignalSDK.Json/Serializer::SerializeValue(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeValue_m05F48CBD03A0580F94083F0EC60ACC24025F9847 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_t1C522956D79B7DC92B5B01053DF1AC058C8B598D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral77D38C0623F92B292B925F6E72CF5CF99A20D4EB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB7C45DD316C68ABF3429C20058C2981C652192F2);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	String_t* V_2 = NULL;
	StringBuilder_t* G_B7_0 = NULL;
	StringBuilder_t* G_B6_0 = NULL;
	String_t* G_B8_0 = NULL;
	StringBuilder_t* G_B8_1 = NULL;
	{
		// if (value == null)
		RuntimeObject* L_0 = ___0_value;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		// builder.Append("null");
		StringBuilder_t* L_1 = __this->___builder_0;
		NullCheck(L_1);
		StringBuilder_t* L_2;
		L_2 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_1, _stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174, NULL);
		return;
	}

IL_0015:
	{
		// else if ((asStr = value as string) != null)
		RuntimeObject* L_3 = ___0_value;
		String_t* L_4 = ((String_t*)IsInstSealed((RuntimeObject*)L_3, String_t_il2cpp_TypeInfo_var));
		V_2 = L_4;
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		// SerializeString(asStr);
		String_t* L_5 = V_2;
		Serializer_SerializeString_mDC5E9C2B126BF45E499655700E82D80929FA3511(__this, L_5, NULL);
		return;
	}

IL_0027:
	{
		// else if (value is bool)
		RuntimeObject* L_6 = ___0_value;
		if (!((RuntimeObject*)IsInstSealed((RuntimeObject*)L_6, Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var)))
		{
			goto IL_0050;
		}
	}
	{
		// builder.Append((bool) value ? "true" : "false");
		StringBuilder_t* L_7 = __this->___builder_0;
		RuntimeObject* L_8 = ___0_value;
		G_B6_0 = L_7;
		if (((*(bool*)((bool*)(bool*)UnBox(L_8, Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var)))))
		{
			G_B7_0 = L_7;
			goto IL_0044;
		}
	}
	{
		G_B8_0 = _stringLiteral77D38C0623F92B292B925F6E72CF5CF99A20D4EB;
		G_B8_1 = G_B6_0;
		goto IL_0049;
	}

IL_0044:
	{
		G_B8_0 = _stringLiteralB7C45DD316C68ABF3429C20058C2981C652192F2;
		G_B8_1 = G_B7_0;
	}

IL_0049:
	{
		NullCheck(G_B8_1);
		StringBuilder_t* L_9;
		L_9 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(G_B8_1, G_B8_0, NULL);
		return;
	}

IL_0050:
	{
		// else if ((asList = value as IList) != null)
		RuntimeObject* L_10 = ___0_value;
		RuntimeObject* L_11 = ((RuntimeObject*)IsInst((RuntimeObject*)L_10, IList_t1C522956D79B7DC92B5B01053DF1AC058C8B598D_il2cpp_TypeInfo_var));
		V_0 = L_11;
		if (!L_11)
		{
			goto IL_0062;
		}
	}
	{
		// SerializeArray(asList);
		RuntimeObject* L_12 = V_0;
		Serializer_SerializeArray_m9D612F968FC32957BBCEA48C7554FB8B73CE6021(__this, L_12, NULL);
		return;
	}

IL_0062:
	{
		// else if ((asDict = value as IDictionary) != null)
		RuntimeObject* L_13 = ___0_value;
		RuntimeObject* L_14 = ((RuntimeObject*)IsInst((RuntimeObject*)L_13, IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220_il2cpp_TypeInfo_var));
		V_1 = L_14;
		if (!L_14)
		{
			goto IL_0074;
		}
	}
	{
		// SerializeObject(asDict);
		RuntimeObject* L_15 = V_1;
		Serializer_SerializeObject_m752864D4085775CF21F8A9866C857DCC72614491(__this, L_15, NULL);
		return;
	}

IL_0074:
	{
		// else if (value is char)
		RuntimeObject* L_16 = ___0_value;
		if (!((RuntimeObject*)IsInstSealed((RuntimeObject*)L_16, Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var)))
		{
			goto IL_008f;
		}
	}
	{
		// SerializeString(new string((char) value, 1));
		RuntimeObject* L_17 = ___0_value;
		String_t* L_18;
		L_18 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, ((*(Il2CppChar*)((Il2CppChar*)(Il2CppChar*)UnBox(L_17, Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var)))), 1, NULL);
		Serializer_SerializeString_mDC5E9C2B126BF45E499655700E82D80929FA3511(__this, L_18, NULL);
		return;
	}

IL_008f:
	{
		// SerializeOther(value);
		RuntimeObject* L_19 = ___0_value;
		Serializer_SerializeOther_mFFB73E1A04876CD6A2625EDD360AAC8B30DF155A(__this, L_19, NULL);
		// }
		return;
	}
}
// System.Void OneSignalSDK.Json/Serializer::AppendNewLineFunc()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, const RuntimeMethod* method) 
{
	{
		// builder.AppendLine();
		StringBuilder_t* L_0 = __this->___builder_0;
		NullCheck(L_0);
		StringBuilder_t* L_1;
		L_1 = StringBuilder_AppendLine_m3BC704C4E6A8531027D8C9287D0AB2AA0188AC4E(L_0, NULL);
		// builder.Append(' ', indentSpaces * indentLevel);
		StringBuilder_t* L_2 = __this->___builder_0;
		int32_t L_3 = __this->___indentSpaces_2;
		int32_t L_4 = __this->___indentLevel_3;
		NullCheck(L_2);
		StringBuilder_t* L_5;
		L_5 = StringBuilder_Append_mE20F6CD28FC8E8C9FD65987DBD32E6087CCE1CF3(L_2, ((int32_t)32), ((int32_t)il2cpp_codegen_multiply(L_3, L_4)), NULL);
		// }
		return;
	}
}
// System.Void OneSignalSDK.Json/Serializer::SerializeObject(System.Collections.IDictionary)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeObject_m752864D4085775CF21F8A9866C857DCC72614491 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_t37E7B9DC5B4EF41D190D607F92835BF1171C0E8E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_t6331596D5DD37C462B1B8D49CF6B319B00AB7131_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	{
		// bool first = true;
		V_0 = (bool)1;
		// builder.Append('{');
		StringBuilder_t* L_0 = __this->___builder_0;
		NullCheck(L_0);
		StringBuilder_t* L_1;
		L_1 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_0, ((int32_t)123), NULL);
		// ++indentLevel;
		int32_t L_2 = __this->___indentLevel_3;
		__this->___indentLevel_3 = ((int32_t)il2cpp_codegen_add(L_2, 1));
		// foreach (object e in obj.Keys)
		RuntimeObject* L_3 = ___0_obj;
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(2 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_4);
		RuntimeObject* L_5;
		L_5 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t6331596D5DD37C462B1B8D49CF6B319B00AB7131_il2cpp_TypeInfo_var, L_4);
		V_1 = L_5;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00ab:
			{// begin finally (depth: 1)
				{
					RuntimeObject* L_6 = V_1;
					V_3 = ((RuntimeObject*)IsInst((RuntimeObject*)L_6, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var));
					RuntimeObject* L_7 = V_3;
					if (!L_7)
					{
						goto IL_00bb;
					}
				}
				{
					RuntimeObject* L_8 = V_3;
					NullCheck(L_8);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_8);
				}

IL_00bb:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00a1_1;
			}

IL_002c_1:
			{
				// foreach (object e in obj.Keys)
				RuntimeObject* L_9 = V_1;
				NullCheck(L_9);
				RuntimeObject* L_10;
				L_10 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_9);
				V_2 = L_10;
				// if (first)
				bool L_11 = V_0;
				if (!L_11)
				{
					goto IL_0046_1;
				}
			}
			{
				// if (humanReadable) AppendNewLineFunc();
				bool L_12 = __this->___humanReadable_1;
				if (!L_12)
				{
					goto IL_0062_1;
				}
			}
			{
				// if (humanReadable) AppendNewLineFunc();
				Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26(__this, NULL);
				goto IL_0062_1;
			}

IL_0046_1:
			{
				// builder.Append(',');
				StringBuilder_t* L_13 = __this->___builder_0;
				NullCheck(L_13);
				StringBuilder_t* L_14;
				L_14 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_13, ((int32_t)44), NULL);
				// if (humanReadable) AppendNewLineFunc();
				bool L_15 = __this->___humanReadable_1;
				if (!L_15)
				{
					goto IL_0062_1;
				}
			}
			{
				// if (humanReadable) AppendNewLineFunc();
				Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26(__this, NULL);
			}

IL_0062_1:
			{
				// SerializeString(e.ToString());
				RuntimeObject* L_16 = V_2;
				NullCheck(L_16);
				String_t* L_17;
				L_17 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
				Serializer_SerializeString_mDC5E9C2B126BF45E499655700E82D80929FA3511(__this, L_17, NULL);
				// builder.Append(':');
				StringBuilder_t* L_18 = __this->___builder_0;
				NullCheck(L_18);
				StringBuilder_t* L_19;
				L_19 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_18, ((int32_t)58), NULL);
				// if (humanReadable) builder.Append(' ');
				bool L_20 = __this->___humanReadable_1;
				if (!L_20)
				{
					goto IL_0092_1;
				}
			}
			{
				// if (humanReadable) builder.Append(' ');
				StringBuilder_t* L_21 = __this->___builder_0;
				NullCheck(L_21);
				StringBuilder_t* L_22;
				L_22 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_21, ((int32_t)32), NULL);
			}

IL_0092_1:
			{
				// SerializeValue(obj[e]);
				RuntimeObject* L_23 = ___0_obj;
				RuntimeObject* L_24 = V_2;
				NullCheck(L_23);
				RuntimeObject* L_25;
				L_25 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220_il2cpp_TypeInfo_var, L_23, L_24);
				Serializer_SerializeValue_m05F48CBD03A0580F94083F0EC60ACC24025F9847(__this, L_25, NULL);
				// first = false;
				V_0 = (bool)0;
			}

IL_00a1_1:
			{
				// foreach (object e in obj.Keys)
				RuntimeObject* L_26 = V_1;
				NullCheck(L_26);
				bool L_27;
				L_27 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_26);
				if (L_27)
				{
					goto IL_002c_1;
				}
			}
			{
				goto IL_00bc;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00bc:
	{
		// --indentLevel;
		int32_t L_28 = __this->___indentLevel_3;
		__this->___indentLevel_3 = ((int32_t)il2cpp_codegen_subtract(L_28, 1));
		// if (humanReadable && obj.Count > 0) AppendNewLineFunc();
		bool L_29 = __this->___humanReadable_1;
		if (!L_29)
		{
			goto IL_00e1;
		}
	}
	{
		RuntimeObject* L_30 = ___0_obj;
		NullCheck(L_30);
		int32_t L_31;
		L_31 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t37E7B9DC5B4EF41D190D607F92835BF1171C0E8E_il2cpp_TypeInfo_var, L_30);
		if ((((int32_t)L_31) <= ((int32_t)0)))
		{
			goto IL_00e1;
		}
	}
	{
		// if (humanReadable && obj.Count > 0) AppendNewLineFunc();
		Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26(__this, NULL);
	}

IL_00e1:
	{
		// builder.Append('}');
		StringBuilder_t* L_32 = __this->___builder_0;
		NullCheck(L_32);
		StringBuilder_t* L_33;
		L_33 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_32, ((int32_t)125), NULL);
		// }
		return;
	}
}
// System.Void OneSignalSDK.Json/Serializer::SerializeArray(System.Collections.IList)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeArray_m9D612F968FC32957BBCEA48C7554FB8B73CE6021 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, RuntimeObject* ___0_anArray, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_t37E7B9DC5B4EF41D190D607F92835BF1171C0E8E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_t1C522956D79B7DC92B5B01053DF1AC058C8B598D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	{
		// builder.Append('[');
		StringBuilder_t* L_0 = __this->___builder_0;
		NullCheck(L_0);
		StringBuilder_t* L_1;
		L_1 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_0, ((int32_t)91), NULL);
		// ++indentLevel;
		int32_t L_2 = __this->___indentLevel_3;
		__this->___indentLevel_3 = ((int32_t)il2cpp_codegen_add(L_2, 1));
		// bool first = true;
		V_0 = (bool)1;
		// for (int i = 0; i < anArray.Count; i++)
		V_1 = 0;
		goto IL_0066;
	}

IL_0022:
	{
		// object obj = anArray[i];
		RuntimeObject* L_3 = ___0_anArray;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		RuntimeObject* L_5;
		L_5 = InterfaceFuncInvoker1< RuntimeObject*, int32_t >::Invoke(0 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1C522956D79B7DC92B5B01053DF1AC058C8B598D_il2cpp_TypeInfo_var, L_3, L_4);
		V_2 = L_5;
		// if (first)
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		// if (humanReadable) AppendNewLineFunc();
		bool L_7 = __this->___humanReadable_1;
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		// if (humanReadable) AppendNewLineFunc();
		Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26(__this, NULL);
		goto IL_0059;
	}

IL_003d:
	{
		// builder.Append(',');
		StringBuilder_t* L_8 = __this->___builder_0;
		NullCheck(L_8);
		StringBuilder_t* L_9;
		L_9 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_8, ((int32_t)44), NULL);
		// if (humanReadable) AppendNewLineFunc();
		bool L_10 = __this->___humanReadable_1;
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		// if (humanReadable) AppendNewLineFunc();
		Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26(__this, NULL);
	}

IL_0059:
	{
		// SerializeValue(obj);
		RuntimeObject* L_11 = V_2;
		Serializer_SerializeValue_m05F48CBD03A0580F94083F0EC60ACC24025F9847(__this, L_11, NULL);
		// first = false;
		V_0 = (bool)0;
		// for (int i = 0; i < anArray.Count; i++)
		int32_t L_12 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_12, 1));
	}

IL_0066:
	{
		// for (int i = 0; i < anArray.Count; i++)
		int32_t L_13 = V_1;
		RuntimeObject* L_14 = ___0_anArray;
		NullCheck(L_14);
		int32_t L_15;
		L_15 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t37E7B9DC5B4EF41D190D607F92835BF1171C0E8E_il2cpp_TypeInfo_var, L_14);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0022;
		}
	}
	{
		// --indentLevel;
		int32_t L_16 = __this->___indentLevel_3;
		__this->___indentLevel_3 = ((int32_t)il2cpp_codegen_subtract(L_16, 1));
		// if (humanReadable && anArray.Count > 0) AppendNewLineFunc();
		bool L_17 = __this->___humanReadable_1;
		if (!L_17)
		{
			goto IL_0094;
		}
	}
	{
		RuntimeObject* L_18 = ___0_anArray;
		NullCheck(L_18);
		int32_t L_19;
		L_19 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t37E7B9DC5B4EF41D190D607F92835BF1171C0E8E_il2cpp_TypeInfo_var, L_18);
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_0094;
		}
	}
	{
		// if (humanReadable && anArray.Count > 0) AppendNewLineFunc();
		Serializer_AppendNewLineFunc_m1C3D039E09D1EBB75C8E5016A7754802E882CF26(__this, NULL);
	}

IL_0094:
	{
		// builder.Append(']');
		StringBuilder_t* L_20 = __this->___builder_0;
		NullCheck(L_20);
		StringBuilder_t* L_21;
		L_21 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_20, ((int32_t)93), NULL);
		// }
		return;
	}
}
// System.Void OneSignalSDK.Json/Serializer::SerializeString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeString_mDC5E9C2B126BF45E499655700E82D80929FA3511 (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, String_t* ___0_str, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5962E944D7340CE47999BF097B4AFD70C1501FB9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F3238CD8C342B06FB9AB185C610175C84625462);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral848E5ED630B3142F565DD995C6E8D30187ED33CD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7C3FCA8C63E127B542B38A5CA5E3FEEDDD1B122);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB78F235D4291950A7D101307609C259F3E1F033F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA666908BB15F4E1D2649752EC5DCBD0D5C64699);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE727BF366E3CC855B808D806440542BF7152AF19);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF18840F490E42D3CE48CDCBF47229C1C240F8ABE);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	int32_t V_3 = 0;
	{
		// builder.Append('\"');
		StringBuilder_t* L_0 = __this->___builder_0;
		NullCheck(L_0);
		StringBuilder_t* L_1;
		L_1 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_0, ((int32_t)34), NULL);
		// char[] charArray = str.ToCharArray();
		String_t* L_2 = ___0_str;
		NullCheck(L_2);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_3;
		L_3 = String_ToCharArray_m0699A92AA3E744229EF29CB9D943C47DF4FE5B46(L_2, NULL);
		V_0 = L_3;
		// for (int i = 0; i < charArray.Length; i++)
		V_1 = 0;
		goto IL_012d;
	}

IL_001c:
	{
		// char c = charArray[i];
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint16_t L_7 = (uint16_t)(L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		Il2CppChar L_8 = V_2;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_8, 8)))
		{
			case 0:
			{
				goto IL_007b;
			}
			case 1:
			{
				goto IL_00cd;
			}
			case 2:
			{
				goto IL_00a7;
			}
			case 3:
			{
				goto IL_00e0;
			}
			case 4:
			{
				goto IL_0091;
			}
			case 5:
			{
				goto IL_00ba;
			}
		}
	}
	{
		Il2CppChar L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)34))))
		{
			goto IL_004f;
		}
	}
	{
		Il2CppChar L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_0065;
		}
	}
	{
		goto IL_00e0;
	}

IL_004f:
	{
		// builder.Append("\\\"");
		StringBuilder_t* L_11 = __this->___builder_0;
		NullCheck(L_11);
		StringBuilder_t* L_12;
		L_12 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_11, _stringLiteral848E5ED630B3142F565DD995C6E8D30187ED33CD, NULL);
		// break;
		goto IL_0129;
	}

IL_0065:
	{
		// builder.Append("\\\\");
		StringBuilder_t* L_13 = __this->___builder_0;
		NullCheck(L_13);
		StringBuilder_t* L_14;
		L_14 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_13, _stringLiteralF18840F490E42D3CE48CDCBF47229C1C240F8ABE, NULL);
		// break;
		goto IL_0129;
	}

IL_007b:
	{
		// builder.Append("\\b");
		StringBuilder_t* L_15 = __this->___builder_0;
		NullCheck(L_15);
		StringBuilder_t* L_16;
		L_16 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_15, _stringLiteral5962E944D7340CE47999BF097B4AFD70C1501FB9, NULL);
		// break;
		goto IL_0129;
	}

IL_0091:
	{
		// builder.Append("\\f");
		StringBuilder_t* L_17 = __this->___builder_0;
		NullCheck(L_17);
		StringBuilder_t* L_18;
		L_18 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_17, _stringLiteralA7C3FCA8C63E127B542B38A5CA5E3FEEDDD1B122, NULL);
		// break;
		goto IL_0129;
	}

IL_00a7:
	{
		// builder.Append("\\n");
		StringBuilder_t* L_19 = __this->___builder_0;
		NullCheck(L_19);
		StringBuilder_t* L_20;
		L_20 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_19, _stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51, NULL);
		// break;
		goto IL_0129;
	}

IL_00ba:
	{
		// builder.Append("\\r");
		StringBuilder_t* L_21 = __this->___builder_0;
		NullCheck(L_21);
		StringBuilder_t* L_22;
		L_22 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_21, _stringLiteralB78F235D4291950A7D101307609C259F3E1F033F, NULL);
		// break;
		goto IL_0129;
	}

IL_00cd:
	{
		// builder.Append("\\t");
		StringBuilder_t* L_23 = __this->___builder_0;
		NullCheck(L_23);
		StringBuilder_t* L_24;
		L_24 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_23, _stringLiteral7F3238CD8C342B06FB9AB185C610175C84625462, NULL);
		// break;
		goto IL_0129;
	}

IL_00e0:
	{
		// int codepoint = Convert.ToInt32(c);
		Il2CppChar L_25 = V_2;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		int32_t L_26;
		L_26 = Convert_ToInt32_mDBBE9318A7CCE1560974CE93F5BFED9931CF0052(L_25, NULL);
		V_3 = L_26;
		// if ((codepoint >= 32) && (codepoint <= 126))
		int32_t L_27 = V_3;
		if ((((int32_t)L_27) < ((int32_t)((int32_t)32))))
		{
			goto IL_0100;
		}
	}
	{
		int32_t L_28 = V_3;
		if ((((int32_t)L_28) > ((int32_t)((int32_t)126))))
		{
			goto IL_0100;
		}
	}
	{
		// builder.Append(c);
		StringBuilder_t* L_29 = __this->___builder_0;
		Il2CppChar L_30 = V_2;
		NullCheck(L_29);
		StringBuilder_t* L_31;
		L_31 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_29, L_30, NULL);
		goto IL_0129;
	}

IL_0100:
	{
		// builder.Append("\\u");
		StringBuilder_t* L_32 = __this->___builder_0;
		NullCheck(L_32);
		StringBuilder_t* L_33;
		L_33 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_32, _stringLiteralDA666908BB15F4E1D2649752EC5DCBD0D5C64699, NULL);
		// builder.Append(codepoint.ToString("x4"));
		StringBuilder_t* L_34 = __this->___builder_0;
		String_t* L_35;
		L_35 = Int32_ToString_m967AECC237535C552A97A80C7875E31B98496CA9((&V_3), _stringLiteralE727BF366E3CC855B808D806440542BF7152AF19, NULL);
		NullCheck(L_34);
		StringBuilder_t* L_36;
		L_36 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_34, L_35, NULL);
	}

IL_0129:
	{
		// for (int i = 0; i < charArray.Length; i++)
		int32_t L_37 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_37, 1));
	}

IL_012d:
	{
		// for (int i = 0; i < charArray.Length; i++)
		int32_t L_38 = V_1;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_39 = V_0;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)((int32_t)(((RuntimeArray*)L_39)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		// builder.Append('\"');
		StringBuilder_t* L_40 = __this->___builder_0;
		NullCheck(L_40);
		StringBuilder_t* L_41;
		L_41 = StringBuilder_Append_m71228B30F05724CD2CD96D9611DCD61BFB96A6E1(L_40, ((int32_t)34), NULL);
		// }
		return;
	}
}
// System.Void OneSignalSDK.Json/Serializer::SerializeOther(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Serializer_SerializeOther_mFFB73E1A04876CD6A2625EDD360AAC8B30DF155A (Serializer_t99E9F0C039FC8B46E85C6D18EDBE2EFC68781CF5* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int16_tB8EF286A9C33492FA6E6D6E67320BE93E794A175_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SByte_tFEFFEF5D2FEBF5207950AE6FAC150FC53B668DB5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2C3D4826D5236B3C9A914C5CE2E3D8CEA48AC7CE);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	double V_1 = 0.0;
	{
		// if (value is float)
		RuntimeObject* L_0 = ___0_value;
		if (!((RuntimeObject*)IsInstSealed((RuntimeObject*)L_0, Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var)))
		{
			goto IL_002d;
		}
	}
	{
		// builder.Append(((float) value).ToString("R", System.Globalization.CultureInfo.InvariantCulture));
		StringBuilder_t* L_1 = __this->___builder_0;
		RuntimeObject* L_2 = ___0_value;
		V_0 = ((*(float*)((float*)(float*)UnBox(L_2, Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var))));
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_3;
		L_3 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		String_t* L_4;
		L_4 = Single_ToString_mF468A56B3A746EFD805E0604EE7A2873DA157ADE((&V_0), _stringLiteral2C3D4826D5236B3C9A914C5CE2E3D8CEA48AC7CE, L_3, NULL);
		NullCheck(L_1);
		StringBuilder_t* L_5;
		L_5 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_1, L_4, NULL);
		return;
	}

IL_002d:
	{
		// else if (value is int
		//          || value is uint
		//          || value is long
		//          || value is sbyte
		//          || value is byte
		//          || value is short
		//          || value is ushort
		//          || value is ulong)
		RuntimeObject* L_6 = ___0_value;
		if (((RuntimeObject*)IsInstSealed((RuntimeObject*)L_6, Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)))
		{
			goto IL_006d;
		}
	}
	{
		RuntimeObject* L_7 = ___0_value;
		if (((RuntimeObject*)IsInstSealed((RuntimeObject*)L_7, UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_il2cpp_TypeInfo_var)))
		{
			goto IL_006d;
		}
	}
	{
		RuntimeObject* L_8 = ___0_value;
		if (((RuntimeObject*)IsInstSealed((RuntimeObject*)L_8, Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var)))
		{
			goto IL_006d;
		}
	}
	{
		RuntimeObject* L_9 = ___0_value;
		if (((RuntimeObject*)IsInstSealed((RuntimeObject*)L_9, SByte_tFEFFEF5D2FEBF5207950AE6FAC150FC53B668DB5_il2cpp_TypeInfo_var)))
		{
			goto IL_006d;
		}
	}
	{
		RuntimeObject* L_10 = ___0_value;
		if (((RuntimeObject*)IsInstSealed((RuntimeObject*)L_10, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var)))
		{
			goto IL_006d;
		}
	}
	{
		RuntimeObject* L_11 = ___0_value;
		if (((RuntimeObject*)IsInstSealed((RuntimeObject*)L_11, Int16_tB8EF286A9C33492FA6E6D6E67320BE93E794A175_il2cpp_TypeInfo_var)))
		{
			goto IL_006d;
		}
	}
	{
		RuntimeObject* L_12 = ___0_value;
		if (((RuntimeObject*)IsInstSealed((RuntimeObject*)L_12, UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_il2cpp_TypeInfo_var)))
		{
			goto IL_006d;
		}
	}
	{
		RuntimeObject* L_13 = ___0_value;
		if (!((RuntimeObject*)IsInstSealed((RuntimeObject*)L_13, UInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_il2cpp_TypeInfo_var)))
		{
			goto IL_007b;
		}
	}

IL_006d:
	{
		// builder.Append(value);
		StringBuilder_t* L_14 = __this->___builder_0;
		RuntimeObject* L_15 = ___0_value;
		NullCheck(L_14);
		StringBuilder_t* L_16;
		L_16 = StringBuilder_Append_m3A7D629DAA5E0E36B8A617A911E34F79AF84AE63(L_14, L_15, NULL);
		return;
	}

IL_007b:
	{
		// else if (value is double
		//          || value is decimal)
		RuntimeObject* L_17 = ___0_value;
		if (((RuntimeObject*)IsInstSealed((RuntimeObject*)L_17, Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_il2cpp_TypeInfo_var)))
		{
			goto IL_008b;
		}
	}
	{
		RuntimeObject* L_18 = ___0_value;
		if (!((RuntimeObject*)IsInstSealed((RuntimeObject*)L_18, Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F_il2cpp_TypeInfo_var)))
		{
			goto IL_00b0;
		}
	}

IL_008b:
	{
		// builder.Append(Convert.ToDouble(value)
		//     .ToString("R", System.Globalization.CultureInfo.InvariantCulture));
		StringBuilder_t* L_19 = __this->___builder_0;
		RuntimeObject* L_20 = ___0_value;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		double L_21;
		L_21 = Convert_ToDouble_m86FF4F837721833186E883102C056A35F0860EB0(L_20, NULL);
		V_1 = L_21;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_22;
		L_22 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		String_t* L_23;
		L_23 = Double_ToString_m7E3930DDFB35B1919FE538A246A59C3FC62AF789((&V_1), _stringLiteral2C3D4826D5236B3C9A914C5CE2E3D8CEA48AC7CE, L_22, NULL);
		NullCheck(L_19);
		StringBuilder_t* L_24;
		L_24 = StringBuilder_Append_m08904D74E0C78E5F36DCD9C9303BDD07886D9F7D(L_19, L_23, NULL);
		return;
	}

IL_00b0:
	{
		// SerializeString(value.ToString());
		RuntimeObject* L_25 = ___0_value;
		NullCheck(L_25);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		Serializer_SerializeString_mDC5E9C2B126BF45E499655700E82D80929FA3511(__this, L_26, NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.UnityMainThreadDispatch::Send(System.Threading.SendOrPostCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMainThreadDispatch_Send_mD724C3333BC0BF58B2A8256E466D8F32231CF76F (SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E* ___0_callback, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void Send(SendOrPostCallback callback) => _context.Send(callback, null);
		SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* L_0 = ((UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_StaticFields*)il2cpp_codegen_static_fields_for(UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_il2cpp_TypeInfo_var))->____context_0;
		SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E* L_1 = ___0_callback;
		NullCheck(L_0);
		VirtualActionInvoker2< SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E*, RuntimeObject* >::Invoke(4 /* System.Void System.Threading.SynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object) */, L_0, L_1, NULL);
		return;
	}
}
// System.Void OneSignalSDK.UnityMainThreadDispatch::Post(System.Threading.SendOrPostCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMainThreadDispatch_Post_m142E52EBFF48DC316B6D09D0485FB1C432214C42 (SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E* ___0_callback, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void Post(SendOrPostCallback callback) => _context.Post(callback, null);
		SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* L_0 = ((UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_StaticFields*)il2cpp_codegen_static_fields_for(UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_il2cpp_TypeInfo_var))->____context_0;
		SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E* L_1 = ___0_callback;
		NullCheck(L_0);
		VirtualActionInvoker2< SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E*, RuntimeObject* >::Invoke(5 /* System.Void System.Threading.SynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object) */, L_0, L_1, NULL);
		return;
	}
}
// System.Void OneSignalSDK.UnityMainThreadDispatch::_initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMainThreadDispatch__initialize_m4E19ED351179F0D3BA1096740320718AC7481243 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* G_B2_0 = NULL;
	SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* G_B1_0 = NULL;
	{
		// private static void _initialize() => _context = _context ?? SynchronizationContext.Current;
		SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* L_0 = ((UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_StaticFields*)il2cpp_codegen_static_fields_for(UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_il2cpp_TypeInfo_var))->____context_0;
		SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000e;
		}
	}
	{
		SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* L_2;
		L_2 = SynchronizationContext_get_Current_m8DE6D3020745B7955249A2470A23EC0ECBB02A82(NULL);
		G_B2_0 = L_2;
	}

IL_000e:
	{
		((UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_StaticFields*)il2cpp_codegen_static_fields_for(UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_il2cpp_TypeInfo_var))->____context_0 = G_B2_0;
		Il2CppCodeGenWriteBarrier((void**)(&((UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_StaticFields*)il2cpp_codegen_static_fields_for(UnityMainThreadDispatch_t2791D12B08651D1F1325AA78B06CFAA969DF7A12_il2cpp_TypeInfo_var))->____context_0), (void*)G_B2_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_Multicast(SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* __this, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* ___0_current, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* currentDelegate = reinterpret_cast<SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___0_current, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_OpenInst(SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* __this, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* ___0_current, const RuntimeMethod* method)
{
	NullCheck(___0_current);
	typedef void (*FunctionPointerType) (PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_current, method);
}
void SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_OpenStatic(SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* __this, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* ___0_current, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_current, method);
}
void SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_OpenStaticInvoker(SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* __this, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* ___0_current, const RuntimeMethod* method)
{
	InvokerActionInvoker1< PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* >::Invoke(__this->___method_ptr_0, method, NULL, ___0_current);
}
void SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_ClosedStaticInvoker(SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* __this, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* ___0_current, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___0_current);
}
// System.Void OneSignalSDK.User.Models.SubscriptionChangedDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubscriptionChangedDelegate__ctor_mEF3302A0E1CC3E8D4902CB9EBCAD601E65694FF0 (SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___1_method);
	__this->___method_3 = ___1_method;
	__this->___m_target_2 = ___0_object;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___0_object);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___1_method);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___1_method))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___1_method))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_OpenInst;
		}
		else
		{
			if (___0_object == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5_Multicast;
}
// System.Void OneSignalSDK.User.Models.SubscriptionChangedDelegate::Invoke(OneSignalSDK.User.Models.PushSubscriptionState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubscriptionChangedDelegate_Invoke_m073A6A6BAE3EC448403FE2A2951DDE811D43AED5 (SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* __this, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* ___0_current, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_current, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult OneSignalSDK.User.Models.SubscriptionChangedDelegate::BeginInvoke(OneSignalSDK.User.Models.PushSubscriptionState,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* SubscriptionChangedDelegate_BeginInvoke_m625675C1BF9FCB5A65244CB3DF2198C97AAB440B (SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* __this, PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* ___0_current, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___1_callback, RuntimeObject* ___2_object, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___0_current;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___1_callback, (RuntimeObject*)___2_object);
}
// System.Void OneSignalSDK.User.Models.SubscriptionChangedDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubscriptionChangedDelegate_EndInvoke_mC381A634181E2C955457387F00174AA40403E455 (SubscriptionChangedDelegate_t2D838EBFDFC830787E744E39211E11C0C8CAF693* __this, RuntimeObject* ___0_result, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___0_result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.User.Models.PushSubscriptionState::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PushSubscriptionState__ctor_m017AD4E1C48437A42A4BCEB842F01DD9F73BCD01 (PushSubscriptionState_tD14984AAF4404DE631EEA8225EDF2ADE610D9BEA* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_Multicast(NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* __this, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* ___0_notification, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* retVal = NULL;
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* currentDelegate = reinterpret_cast<NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13*>(delegatesToInvoke[i]);
		typedef Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* (*FunctionPointerType) (RuntimeObject*, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B*, const RuntimeMethod*);
		retVal = ((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___0_notification, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
	return retVal;
}
Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_OpenInst(NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* __this, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* ___0_notification, const RuntimeMethod* method)
{
	NullCheck(___0_notification);
	typedef Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* (*FunctionPointerType) (Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___method_ptr_0)(___0_notification, method);
}
Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_OpenStatic(NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* __this, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* ___0_notification, const RuntimeMethod* method)
{
	typedef Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* (*FunctionPointerType) (Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___method_ptr_0)(___0_notification, method);
}
Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_OpenStaticInvoker(NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* __this, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* ___0_notification, const RuntimeMethod* method)
{
	return InvokerFuncInvoker1< Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B*, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* >::Invoke(__this->___method_ptr_0, method, NULL, ___0_notification);
}
Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_ClosedStaticInvoker(NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* __this, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* ___0_notification, const RuntimeMethod* method)
{
	return InvokerFuncInvoker2< Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B*, RuntimeObject*, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___0_notification);
}
// System.Void OneSignalSDK.Notifications.NotificationWillShowDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotificationWillShowDelegate__ctor_m729B11DF786451F7F6EE1C5AEF4E9AA75007485F (NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___1_method);
	__this->___method_3 = ___1_method;
	__this->___m_target_2 = ___0_object;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___0_object);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___1_method);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___1_method))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___1_method))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_OpenInst;
		}
		else
		{
			if (___0_object == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E_Multicast;
}
// OneSignalSDK.Notifications.Models.Notification OneSignalSDK.Notifications.NotificationWillShowDelegate::Invoke(OneSignalSDK.Notifications.Models.Notification)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* NotificationWillShowDelegate_Invoke_m4D69F1F027938712E638818AA132B9BB9F39363E (NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* __this, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* ___0_notification, const RuntimeMethod* method) 
{
	typedef Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* (*FunctionPointerType) (RuntimeObject*, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_notification, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult OneSignalSDK.Notifications.NotificationWillShowDelegate::BeginInvoke(OneSignalSDK.Notifications.Models.Notification,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NotificationWillShowDelegate_BeginInvoke_m34ADC0F539131C74580E9392FE460CE25B408EBE (NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* __this, Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* ___0_notification, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___1_callback, RuntimeObject* ___2_object, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___0_notification;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___1_callback, (RuntimeObject*)___2_object);
}
// OneSignalSDK.Notifications.Models.Notification OneSignalSDK.Notifications.NotificationWillShowDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* NotificationWillShowDelegate_EndInvoke_m904FA2FC04EB22BE5C47AE62F6F103C695A7DFF6 (NotificationWillShowDelegate_t073F2DB148B86DED817B6106A57EE72B94C01F13* __this, RuntimeObject* ___0_result, const RuntimeMethod* method) 
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___0_result, 0);
	return (Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B*)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_Multicast(NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* __this, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* ___0_result, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* currentDelegate = reinterpret_cast<NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___0_result, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_OpenInst(NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* __this, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* ___0_result, const RuntimeMethod* method)
{
	NullCheck(___0_result);
	typedef void (*FunctionPointerType) (NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_result, method);
}
void NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_OpenStatic(NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* __this, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* ___0_result, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_result, method);
}
void NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_OpenStaticInvoker(NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* __this, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* ___0_result, const RuntimeMethod* method)
{
	InvokerActionInvoker1< NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* >::Invoke(__this->___method_ptr_0, method, NULL, ___0_result);
}
void NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_ClosedStaticInvoker(NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* __this, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* ___0_result, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___0_result);
}
// System.Void OneSignalSDK.Notifications.NotificationClickedDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotificationClickedDelegate__ctor_m1705C28B9F07F8C103F7C31D9383DCEF44416A72 (NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___1_method);
	__this->___method_3 = ___1_method;
	__this->___m_target_2 = ___0_object;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___0_object);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___1_method);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___1_method))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___1_method))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_OpenInst;
		}
		else
		{
			if (___0_object == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074_Multicast;
}
// System.Void OneSignalSDK.Notifications.NotificationClickedDelegate::Invoke(OneSignalSDK.Notifications.Models.NotificationClickedResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotificationClickedDelegate_Invoke_m1B7B2FE4C05955195E0F7B0AA825B2DFCB2EA074 (NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* __this, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* ___0_result, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_result, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult OneSignalSDK.Notifications.NotificationClickedDelegate::BeginInvoke(OneSignalSDK.Notifications.Models.NotificationClickedResult,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NotificationClickedDelegate_BeginInvoke_m92ABCADC3ED1F9B7A0E424EBE77B3B1A1E829210 (NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* __this, NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* ___0_result, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___1_callback, RuntimeObject* ___2_object, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___0_result;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___1_callback, (RuntimeObject*)___2_object);
}
// System.Void OneSignalSDK.Notifications.NotificationClickedDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotificationClickedDelegate_EndInvoke_m31C9D6D51AF94B3092B3043EFCE2048B42C804C7 (NotificationClickedDelegate_tE48D8684AC54DB37E04EF31E61EA776ADD0A3332* __this, RuntimeObject* ___0_result, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___0_result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29_Multicast(PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, bool ___0_permission, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* currentDelegate = reinterpret_cast<PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, bool, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___0_permission, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29_OpenInst(PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, bool ___0_permission, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (bool, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_permission, method);
}
void PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29_OpenStatic(PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, bool ___0_permission, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (bool, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_permission, method);
}
void PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29_OpenStaticInvoker(PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, bool ___0_permission, const RuntimeMethod* method)
{
	InvokerActionInvoker1< bool >::Invoke(__this->___method_ptr_0, method, NULL, ___0_permission);
}
void PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29_ClosedStaticInvoker(PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, bool ___0_permission, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, bool >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___0_permission);
}
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED (PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, bool ___0_permission, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_reverse_pinvoke_function_ptr(__this));
	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___0_permission));

}
// System.Void OneSignalSDK.Notifications.PermissionChangedDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionChangedDelegate__ctor_mFD33D3C6CDFFF844A92B3BDAFCAD2C98C218E750 (PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___1_method);
	__this->___method_3 = ___1_method;
	__this->___m_target_2 = ___0_object;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___0_object);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___1_method);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___1_method))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___1_method))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		if (___0_object == NULL)
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
		__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
		__this->___method_code_6 = (intptr_t)__this->___m_target_2;
	}
	__this->___extra_arg_5 = (intptr_t)&PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29_Multicast;
}
// System.Void OneSignalSDK.Notifications.PermissionChangedDelegate::Invoke(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionChangedDelegate_Invoke_mE42F4C1159F4B9E93EAA99A10A1E33C212A9CA29 (PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, bool ___0_permission, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, bool, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_permission, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult OneSignalSDK.Notifications.PermissionChangedDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PermissionChangedDelegate_BeginInvoke_m9FDECF5A75F7B7C8EB620DC8D22203CC48DC16B0 (PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, bool ___0_permission, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___1_callback, RuntimeObject* ___2_object, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var, &___0_permission);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___1_callback, (RuntimeObject*)___2_object);
}
// System.Void OneSignalSDK.Notifications.PermissionChangedDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionChangedDelegate_EndInvoke_mB103E215707B23A791ED2A9483F85FBC5E582585 (PermissionChangedDelegate_tE03C753A6443DFDA064622FF5FE294891406ACED* __this, RuntimeObject* ___0_result, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___0_result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.Notifications.Models.ActionButton::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionButton__ctor_m3E105EF7912352576B788EB4E5B0C10500519DAF (ActionButton_tD0F4E6CE1C60DE2866BA6361EB6DA58461452F04* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.Notifications.Models.BackgroundImageLayout::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BackgroundImageLayout__ctor_m9298306566C0AFB9DFC04176B91F344AC9A454A7 (BackgroundImageLayout_tD6FC701C0DDF4BD63EDB891962009F9E14E4E2F2* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.Notifications.Models.NotificationBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotificationBase__ctor_m2860AACFC6907FC61442BB28CFE72D45A3573264 (NotificationBase_tEAE8E5ACEC71A0A416C4AAB702F39C734AAF3686* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.Notifications.Models.Notification::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Notification__ctor_mB78313D7086E32F041B34F83D010051A4A534138 (Notification_t1392B37A2D11BDF8BAC550AAC857899FD9BA374B* __this, const RuntimeMethod* method) 
{
	{
		NotificationBase__ctor_m2860AACFC6907FC61442BB28CFE72D45A3573264(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.Notifications.Models.NotificationAction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotificationAction__ctor_m11F6F18F901A8E07A34012423CE90131FD313B1F (NotificationAction_tCFE5BF25A39173707A04C574311BAA53CC8C1591* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.Notifications.Models.NotificationClickedResult::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotificationClickedResult__ctor_m589F1242108E92D3530D0CC876B62D62E20AEFD9 (NotificationClickedResult_tD915626EC064A3FEB67ACB463836165300AC566C* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_Multicast(InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* __this, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* ___0_message, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* currentDelegate = reinterpret_cast<InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___0_message, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_OpenInst(InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* __this, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* ___0_message, const RuntimeMethod* method)
{
	NullCheck(___0_message);
	typedef void (*FunctionPointerType) (InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_message, method);
}
void InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_OpenStatic(InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* __this, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* ___0_message, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_message, method);
}
void InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_OpenStaticInvoker(InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* __this, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* ___0_message, const RuntimeMethod* method)
{
	InvokerActionInvoker1< InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* >::Invoke(__this->___method_ptr_0, method, NULL, ___0_message);
}
void InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_ClosedStaticInvoker(InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* __this, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* ___0_message, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___0_message);
}
// System.Void OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InAppMessageLifecycleDelegate__ctor_m1A037771A206CB303800408EC1B531ECE7ACF9E7 (InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___1_method);
	__this->___method_3 = ___1_method;
	__this->___m_target_2 = ___0_object;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___0_object);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___1_method);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___1_method))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___1_method))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_OpenInst;
		}
		else
		{
			if (___0_object == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725_Multicast;
}
// System.Void OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate::Invoke(OneSignalSDK.InAppMessages.Models.InAppMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InAppMessageLifecycleDelegate_Invoke_m4A72F4F00AC5912EEBCBABD3CF49C6CE2A4AE725 (InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* __this, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* ___0_message, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_message, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate::BeginInvoke(OneSignalSDK.InAppMessages.Models.InAppMessage,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InAppMessageLifecycleDelegate_BeginInvoke_mCFEFF7F2C7D80597D2E6FBC4666C2B63052D1865 (InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* __this, InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* ___0_message, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___1_callback, RuntimeObject* ___2_object, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___0_message;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___1_callback, (RuntimeObject*)___2_object);
}
// System.Void OneSignalSDK.InAppMessages.InAppMessageLifecycleDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InAppMessageLifecycleDelegate_EndInvoke_m7F124CBDA1A79920308DF953A37E26E50C8F0A70 (InAppMessageLifecycleDelegate_t39ABD41196E566CC05884B925347CD35F25081DD* __this, RuntimeObject* ___0_result, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___0_result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_Multicast(InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* __this, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* ___0_result, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* currentDelegate = reinterpret_cast<InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___0_result, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_OpenInst(InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* __this, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* ___0_result, const RuntimeMethod* method)
{
	NullCheck(___0_result);
	typedef void (*FunctionPointerType) (InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_result, method);
}
void InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_OpenStatic(InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* __this, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* ___0_result, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_result, method);
}
void InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_OpenStaticInvoker(InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* __this, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* ___0_result, const RuntimeMethod* method)
{
	InvokerActionInvoker1< InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* >::Invoke(__this->___method_ptr_0, method, NULL, ___0_result);
}
void InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_ClosedStaticInvoker(InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* __this, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* ___0_result, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___0_result);
}
// System.Void OneSignalSDK.InAppMessages.InAppMessageClickedDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InAppMessageClickedDelegate__ctor_m0CFC49C490314B140CAE01763D18EB405636A4CA (InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___1_method);
	__this->___method_3 = ___1_method;
	__this->___m_target_2 = ___0_object;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___0_object);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___1_method);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___1_method))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___1_method))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_OpenInst;
		}
		else
		{
			if (___0_object == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F_Multicast;
}
// System.Void OneSignalSDK.InAppMessages.InAppMessageClickedDelegate::Invoke(OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InAppMessageClickedDelegate_Invoke_m8BC27CBB3C040DC07840ADC592E0AF167B73992F (InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* __this, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* ___0_result, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_result, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult OneSignalSDK.InAppMessages.InAppMessageClickedDelegate::BeginInvoke(OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InAppMessageClickedDelegate_BeginInvoke_mC33D35346B49B2BF3146E790BA37EEC1F1D8D56B (InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* __this, InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* ___0_result, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___1_callback, RuntimeObject* ___2_object, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___0_result;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___1_callback, (RuntimeObject*)___2_object);
}
// System.Void OneSignalSDK.InAppMessages.InAppMessageClickedDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InAppMessageClickedDelegate_EndInvoke_m239D80FFA6E671BC6148EE69B7D18D852966E819 (InAppMessageClickedDelegate_t8BBFB1A3950BC7C9732D9D2B5E50161E05FA4A84* __this, RuntimeObject* ___0_result, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___0_result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String OneSignalSDK.InAppMessages.Models.InAppMessageAction::get_ClickName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InAppMessageAction_get_ClickName_mB9BA05E950A6A1FA8B070BAD85A66E7FD2066AC7 (InAppMessageAction_tCE4215F332A511CE8DA5B85CC5A15D22EFB1D430* __this, const RuntimeMethod* method) 
{
	{
		// public string ClickName => click_name;
		String_t* L_0 = __this->___click_name_0;
		return L_0;
	}
}
// System.String OneSignalSDK.InAppMessages.Models.InAppMessageAction::get_ClickUrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InAppMessageAction_get_ClickUrl_m14B0126EC220A546F11CD023090BF05172C1B71E (InAppMessageAction_tCE4215F332A511CE8DA5B85CC5A15D22EFB1D430* __this, const RuntimeMethod* method) 
{
	{
		// public string ClickUrl => click_url;
		String_t* L_0 = __this->___click_url_1;
		return L_0;
	}
}
// System.Boolean OneSignalSDK.InAppMessages.Models.InAppMessageAction::get_IsFirstClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InAppMessageAction_get_IsFirstClick_mBD101B7A7AD940494AD1540CF5205C880CC29381 (InAppMessageAction_tCE4215F332A511CE8DA5B85CC5A15D22EFB1D430* __this, const RuntimeMethod* method) 
{
	{
		// public bool IsFirstClick => first_click;
		bool L_0 = __this->___first_click_2;
		return L_0;
	}
}
// System.Boolean OneSignalSDK.InAppMessages.Models.InAppMessageAction::get_ClosesMessage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InAppMessageAction_get_ClosesMessage_mB987735CD5F9329A7629804D6C8D71CDAE4410A2 (InAppMessageAction_tCE4215F332A511CE8DA5B85CC5A15D22EFB1D430* __this, const RuntimeMethod* method) 
{
	{
		// public bool ClosesMessage => closes_message;
		bool L_0 = __this->___closes_message_3;
		return L_0;
	}
}
// System.Void OneSignalSDK.InAppMessages.Models.InAppMessageAction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InAppMessageAction__ctor_mFC28FFD67728ECE79F12C7F1EC741F36DF719600 (InAppMessageAction_tCE4215F332A511CE8DA5B85CC5A15D22EFB1D430* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.InAppMessages.Models.InAppMessage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InAppMessage__ctor_m79C91EC28E66B99708C36336A24CAB4947379F9C (InAppMessage_t00B9E40035D5E8CEB5B4D726B778EC7E89067449* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.InAppMessages.Models.InAppMessageClickedResult::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InAppMessageClickedResult__ctor_mACC7F9B3D42E61648D396832042E78411F97EF0A (InAppMessageClickedResult_tEF21BC422A87456B0BA465F004A0C4229DC4D0D5* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::add_LogIntercept(System.Action`1<System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_add_LogIntercept_m61BE636BC5C5E5829B311939EB1EE0C3E3517003 (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_0 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_1 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_2 = NULL;
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_0 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___LogIntercept_0;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_1 = V_0;
		V_1 = L_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_2 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)Castclass((RuntimeObject*)L_4, Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var));
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_5 = V_2;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_6 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*>((&((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___LogIntercept_0), L_5, L_6);
		V_0 = L_7;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_8 = V_0;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_8) == ((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::remove_LogIntercept(System.Action`1<System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_remove_LogIntercept_mC9FBC2FA351BAFE0190493C8E0C963C43C75027C (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_0 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_1 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_2 = NULL;
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_0 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___LogIntercept_0;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_1 = V_0;
		V_1 = L_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_2 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)Castclass((RuntimeObject*)L_4, Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var));
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_5 = V_2;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_6 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*>((&((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___LogIntercept_0), L_5, L_6);
		V_0 = L_7;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_8 = V_0;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_8) == ((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::add_WarnIntercept(System.Action`1<System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_add_WarnIntercept_m0C178174F2632C5CB898FA381C3A8E8F721CE393 (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_0 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_1 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_2 = NULL;
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_0 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___WarnIntercept_1;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_1 = V_0;
		V_1 = L_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_2 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)Castclass((RuntimeObject*)L_4, Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var));
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_5 = V_2;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_6 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*>((&((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___WarnIntercept_1), L_5, L_6);
		V_0 = L_7;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_8 = V_0;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_8) == ((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::remove_WarnIntercept(System.Action`1<System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_remove_WarnIntercept_m7669E8015F8C1DCFD990ACB466656FA4D5FA0100 (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_0 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_1 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_2 = NULL;
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_0 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___WarnIntercept_1;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_1 = V_0;
		V_1 = L_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_2 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)Castclass((RuntimeObject*)L_4, Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var));
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_5 = V_2;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_6 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*>((&((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___WarnIntercept_1), L_5, L_6);
		V_0 = L_7;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_8 = V_0;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_8) == ((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::add_ErrorIntercept(System.Action`1<System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_add_ErrorIntercept_m9D17C17A2FD46E3316179B2565D56BEA75608342 (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_0 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_1 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_2 = NULL;
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_0 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___ErrorIntercept_2;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_1 = V_0;
		V_1 = L_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_2 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)Castclass((RuntimeObject*)L_4, Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var));
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_5 = V_2;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_6 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*>((&((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___ErrorIntercept_2), L_5, L_6);
		V_0 = L_7;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_8 = V_0;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_8) == ((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::remove_ErrorIntercept(System.Action`1<System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_remove_ErrorIntercept_m436DDAFBD3CB3820D20B069022F07D09383BDF7D (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_0 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_1 = NULL;
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* V_2 = NULL;
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_0 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___ErrorIntercept_2;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_1 = V_0;
		V_1 = L_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_2 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)Castclass((RuntimeObject*)L_4, Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87_il2cpp_TypeInfo_var));
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_5 = V_2;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_6 = V_1;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*>((&((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___ErrorIntercept_2), L_5, L_6);
		V_0 = L_7;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_8 = V_0;
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_8) == ((RuntimeObject*)(Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::Info(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_Info_m531A4499060F346EAB9413566E49445F27E3D6CB (String_t* ___0_message, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (LogIntercept != null)
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_0 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___LogIntercept_0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// LogIntercept(message);
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_1 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___LogIntercept_0;
		String_t* L_2 = ___0_message;
		NullCheck(L_1);
		Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_inline(L_1, L_2, NULL);
		return;
	}

IL_0013:
	{
		// else if (OneSignal.Default.Debug.LogLevel >= LogLevel.Info)
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_3;
		L_3 = OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4(NULL);
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = VirtualFuncInvoker0< RuntimeObject* >::Invoke(9 /* OneSignalSDK.Debug.IDebugManager OneSignalSDK.OneSignal::get_Debug() */, L_3);
		NullCheck(L_4);
		int32_t L_5;
		L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* OneSignalSDK.Debug.Models.LogLevel OneSignalSDK.Debug.IDebugManager::get_LogLevel() */, IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var, L_4);
		if ((((int32_t)L_5) < ((int32_t)4)))
		{
			goto IL_0030;
		}
	}
	{
		// UnityEngine.Debug.Log(_formatMessage(message));
		String_t* L_6 = ___0_message;
		String_t* L_7;
		L_7 = SDKDebug__formatMessage_m0CCD6BF7BE05E151DAAE8ADD438321725DB6818F(L_6, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(L_7, NULL);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::Warn(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_Warn_m1580DF9F5739B31951AEEB5F2457F7C76331FC40 (String_t* ___0_message, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (WarnIntercept != null)
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_0 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___WarnIntercept_1;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// WarnIntercept(message);
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_1 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___WarnIntercept_1;
		String_t* L_2 = ___0_message;
		NullCheck(L_1);
		Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_inline(L_1, L_2, NULL);
		return;
	}

IL_0013:
	{
		// else if (OneSignal.Default.Debug.LogLevel >= LogLevel.Warn)
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_3;
		L_3 = OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4(NULL);
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = VirtualFuncInvoker0< RuntimeObject* >::Invoke(9 /* OneSignalSDK.Debug.IDebugManager OneSignalSDK.OneSignal::get_Debug() */, L_3);
		NullCheck(L_4);
		int32_t L_5;
		L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* OneSignalSDK.Debug.Models.LogLevel OneSignalSDK.Debug.IDebugManager::get_LogLevel() */, IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var, L_4);
		if ((((int32_t)L_5) < ((int32_t)3)))
		{
			goto IL_0030;
		}
	}
	{
		// UnityEngine.Debug.LogWarning(_formatMessage(message));
		String_t* L_6 = ___0_message;
		String_t* L_7;
		L_7 = SDKDebug__formatMessage_m0CCD6BF7BE05E151DAAE8ADD438321725DB6818F(L_6, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarning_m33EF1B897E0C7C6FF538989610BFAFFEF4628CA9(L_7, NULL);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void OneSignalSDK.Debug.Utilities.SDKDebug::Error(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SDKDebug_Error_m47AE8403BECFDE427D13E676064C7AD491769786 (String_t* ___0_message, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (ErrorIntercept != null)
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_0 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___ErrorIntercept_2;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// ErrorIntercept(message);
		Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* L_1 = ((SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_StaticFields*)il2cpp_codegen_static_fields_for(SDKDebug_t19F65E5CEC92C0A172CA9A968E83403EAEB27E03_il2cpp_TypeInfo_var))->___ErrorIntercept_2;
		String_t* L_2 = ___0_message;
		NullCheck(L_1);
		Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_inline(L_1, L_2, NULL);
		return;
	}

IL_0013:
	{
		// else  if (OneSignal.Default.Debug.LogLevel >= LogLevel.Error)
		OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85* L_3;
		L_3 = OneSignal_get_Default_m30ACD9671BB02F331AB116FBF747392E96B73EC4(NULL);
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = VirtualFuncInvoker0< RuntimeObject* >::Invoke(9 /* OneSignalSDK.Debug.IDebugManager OneSignalSDK.OneSignal::get_Debug() */, L_3);
		NullCheck(L_4);
		int32_t L_5;
		L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* OneSignalSDK.Debug.Models.LogLevel OneSignalSDK.Debug.IDebugManager::get_LogLevel() */, IDebugManager_tB24537C234B2F52A0D16520BD80493EFFF8BA548_il2cpp_TypeInfo_var, L_4);
		if ((((int32_t)L_5) < ((int32_t)2)))
		{
			goto IL_0030;
		}
	}
	{
		// UnityEngine.Debug.LogError(_formatMessage(message));
		String_t* L_6 = ___0_message;
		String_t* L_7;
		L_7 = SDKDebug__formatMessage_m0CCD6BF7BE05E151DAAE8ADD438321725DB6818F(L_6, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_7, NULL);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.String OneSignalSDK.Debug.Utilities.SDKDebug::_formatMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SDKDebug__formatMessage_m0CCD6BF7BE05E151DAAE8ADD438321725DB6818F (String_t* ___0_message, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFB5DAA5265160CE753212EA77C3196536EF9342);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static string _formatMessage(string message) => "[OneSignal] " + message;
		String_t* L_0 = ___0_message;
		String_t* L_1;
		L_1 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(_stringLiteralBFB5DAA5265160CE753212EA77C3196536EF9342, L_0, NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OneSignal_set_AppId_mA7AFE519FA396A621BAED79D13C63854ADE77F7F_inline (String_t* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static string AppId { get; private set; }
		String_t* L_0 = ___0_value;
		((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___U3CAppIdU3Ek__BackingField_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___U3CAppIdU3Ek__BackingField_4), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OneSignal_set_DidInitialize_m8EFFB9C9D7E997672E5EA5309160463F7961C634_inline (bool ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static bool DidInitialize { get; private set; }
		bool L_0 = ___0_value;
		((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___U3CDidInitializeU3Ek__BackingField_5 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OneSignal_get_AppId_mE3F51B18F55331DD5DAA72C6652E0E1EB99A12A3_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static string AppId { get; private set; }
		String_t* L_0 = ((OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_StaticFields*)il2cpp_codegen_static_fields_for(OneSignal_t314CE4D3704637587AA437654E7AE245275B8A85_il2cpp_TypeInfo_var))->___U3CAppIdU3Ek__BackingField_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_obj, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___0_item;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___0_item;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
