using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static playerController;

public class LinesDrawer : MonoBehaviour
{
    [Header("Line Prefabs")]
    [SerializeField] GameObject lineMousePrefabs;
    [SerializeField] GameObject WarninglinePrefabs;

    [Header("Line Properties")]
    [SerializeField] Gradient lineColor;
    [SerializeField] float linePointsMinDistance;
    [SerializeField] float lineWidth;
    [SerializeField] float lineMaxLength = 90;    

    [Header("Check Input Properties")]
    [SerializeField] List<Collider2D> PlayerColliders;
    [SerializeField] public List<Collider2D> EndCollider;
    [SerializeField] float CheckColliderRadius;
    [SerializeField] LayerMask cantDrawOverLayer;
    [SerializeField] LineRenderer warningLine;

    [Header("List Line Transform")]
    [SerializeField] Transform LineController;

    [Header("Public Infomation")]
    public List<Line> tempLines;    
    

    RaycastHit2D hit;
    Line currentLine;
    Camera cam;    
    playerController PlayerController;
    int playerNumber;


    private void Awake()
    {
        LineController = GameObject.FindGameObjectWithTag("ListLineInScene").transform;
    }
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        Input.multiTouchEnabled = false;
        for (int i = 0; i < LineController.childCount; i++)
        {
            Destroy(LineController.GetChild(i).gameObject);
        }
        playerNumber = PlayerColliders.Count;
    }

    // Update is called once per frame
    void Update()
    {        
        if (Input.GetMouseButtonDown(0) && !UiGameplay.Instance.tutorial)
        {            
            Collider2D[] listColInput = Physics2D.OverlapCircleAll(cam.ScreenToWorldPoint(Input.mousePosition), CheckColliderRadius);
            foreach (Collider2D col in listColInput)
            {
                if (PlayerColliders.Contains(col))
                {
                    Debug.Log(col.name);
                    switch (col.tag)
                    {
                        case "DrawCollider":
                            BeginDraw(lineMousePrefabs);                                                    
                            break;                        
                    }
                    PlayerController = col.GetComponentInParent<playerController>();        
                    break;
                }
            }
        }
        if (currentLine != null)
            Draw();

        if (Input.GetMouseButtonUp(0))
            EndDraw(PlayerController);
    }
    // Begin Draw
    void BeginDraw(GameObject linePrefabs)
    {
        SoundManager.Instance.PlaySound(SoundManager.SoundID.Draw,0);
        if (playerNumber <= 0)
        {
            return;
        }
        currentLine = Instantiate(linePrefabs, LineController).GetComponent<Line>();
        currentLine.SetLineColor(lineColor);
        currentLine.SetPointsMinDistance(linePointsMinDistance);
        currentLine.SetLineWidth(lineWidth);        
    }
    //Draw
    void Draw()
    {        
        Vector2 mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
        if (currentLine.GetLineRendererCount() > 0)
        {
            hit = Physics2D.Linecast(currentLine.GetLastPoint(), mousePosition, cantDrawOverLayer);            
            if (hit)
            {
                warningLine.gameObject.SetActive(true);
                warningLine.startWidth = lineWidth;
                warningLine.endWidth = lineWidth;
                warningLine.positionCount = 2;
                warningLine.SetPosition(0, currentLine.GetLastPoint());
                warningLine.SetPosition(1, mousePosition);
                return;
            }
            else
            {
                warningLine.gameObject.SetActive(false);
            }
        }        
        currentLine.AddPoint(mousePosition);
        if (currentLine.LengthLine() > lineMaxLength)
        {
            Destroy(currentLine.gameObject);
        }
    }
    

    //End Draw
    void EndDraw(playerController plControl)
    {
        SoundManager.Instance.StopSound(SoundManager.SoundID.Draw);
        if (currentLine != null)
        {
            if (warningLine.gameObject.activeInHierarchy)
            {
                warningLine.gameObject.SetActive(false);
            }
            if (currentLine.pointsCount < 2)
            {
                Destroy(currentLine.gameObject);                
            }
            else
            {                
                Collider2D[] listColLastPoint = Physics2D.OverlapCircleAll(currentLine.GetLastPoint(), CheckColliderRadius);                
                foreach (Collider2D col in listColLastPoint)
                {                    
                    if (EndCollider.Contains(col))
                    {
                        ButtonManager.Instance.GAMEPLAY_Back.interactable = false;
                        UiGameplay.Instance.TempLine = currentLine.gameObject;
                        playerNumber--;                                                
                        plControl.runPoint = currentLine.points;
                        plControl.setPlayerState(PlayerState.running);
                        /*col.enabled = false;*/
                        tempLines.Add(currentLine);
                        currentLine = null;
                        Debug.Log("end draw");                        
                        return;
                    }
                }                
                Destroy(currentLine.gameObject);
                /*warningLine.gameObject.SetActive(false);*/                
            }
        }
    }    
}
