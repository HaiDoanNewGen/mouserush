using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class autoScaleGameObject : MonoBehaviour
{
    private void Awake()
    {
        if (Screen.height != 1920.0f || Screen.width != 1080f)
        {
            float numScale = (Screen.height / 1920.0f) > (Screen.width / 1080.0f) ? Screen.width / 1080.0f : Screen.height / 1920.0f;
            float convertScaleX = /*GetComponent<RectTransform>().localScale.x - */(numScale * GetComponent<RectTransform>().localScale.x);
            float convertScaleY = /*GetComponent<RectTransform>().localScale.y - */(numScale * GetComponent<RectTransform>().localScale.y);
            this.gameObject.GetComponent<RectTransform>().localScale = new Vector2(convertScaleX, convertScaleY);
        }
    }
}
