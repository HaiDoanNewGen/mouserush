using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneScale : MonoBehaviour
{
    private Vector2 ScreenResolution;
    // Start is called before the first frame update
    void Start()
    {
        ScreenResolution = new Vector2(Screen.width, Screen.height);
        MatchPlaneToScreenSize();
    }

    private void OnEnable()
    {
        //check if screen change size
        if(ScreenResolution.x != Screen.width || ScreenResolution.y != Screen.height)
        {
            MatchPlaneToScreenSize();
            ScreenResolution.x = Screen.width;
            ScreenResolution.y = Screen.height;
        }
    }

    private void MatchPlaneToScreenSize()
    {
        float planeToCamDistance = Vector3.Distance(transform.position, Camera.main.transform.position);
        float planeHeightScale = (2.0f * Mathf.Tan(0.5f * Camera.main.fieldOfView * Mathf.Deg2Rad) * planeToCamDistance)*2.0f;
        float planeWidthScale = planeHeightScale * Camera.main.aspect;
        transform.localScale = new Vector3(planeWidthScale+5, 1, planeHeightScale+5);
    }
}
