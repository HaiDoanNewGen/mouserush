using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static AdsManager;

public class WatchVideoReward : Singleton<WatchVideoReward>
{
    public static WatchVideoReward instance;    
    /*public int countAdBomb, countAdWater;*/
    


    void Awake()
    {
        instance = this;       
    }    


    public void increaseC_Restart()
    {
        int c_Restart = PlayerPrefs.GetInt("c_Restart", 0);
        c_Restart++;
        PlayerPrefs.SetInt("c_Restart", c_Restart);
        if (GameManager.Instance.Enable_replay_ads && c_Restart >= GameManager.Instance.Lvlreset_time_ads)
        {
            if (AdsManager.Instance.checkInterval())
            {
                checkReplayAds();
            }
        }
    }
    public void adsByLevel(int levelAt)
    {
        if (!GameManager.Instance.Enable_interstitial_ads || GameManager.Instance.Show_insterstitial_after_finished_levels == null)
        {
            return;
        }
        if (GameManager.Instance.Show_insterstitial_after_finished_levels.Contains(levelAt.ToString()))
        {
            checkAdsByLevel();
        }
    }

    void checkAdsByLevel()
    {
        if (AdsManager.Instance.hasInternet)
        {
            checkInterstitialAdsByType(InterstitialAds.defaultAds);
        }
    }
    void checkReplayAds()
    {
        if (AdsManager.Instance.hasInternet)
        {
            if (AdsManager.Instance.InterstitialInnit(AdsManager.Instance.getInterstitialAdsId(InterstitialAds.defaultAds)))
            {
                PlayerPrefs.SetInt("c_Restart", 0);
                PlayerPrefs.SetString("show_interstitial", DateTime.Now.ToString());
            }
        }
    }

    public void checkInterstitialAdsByType(InterstitialAds type)
    {
        if (type == InterstitialAds.fromGamePlayAds && !GameManager.Instance.Enable_ads_from_gameplay)
        {
            return;
        }
        if (type == InterstitialAds.fromHomeAds && !GameManager.Instance.Enable_ads_from_home)
        {
            return;
        }
        if (!AdsManager.Instance.checkInterval())
        {
            return;
        }
        if (AdsManager.Instance.InterstitialInnit(AdsManager.Instance.getInterstitialAdsId(type)))
        {
            PlayerPrefs.SetString("show_interstitial", DateTime.Now.ToString());
        }
    }

    

}
