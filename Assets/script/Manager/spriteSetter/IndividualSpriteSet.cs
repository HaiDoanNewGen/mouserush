using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.U2D.Animation;

public class IndividualSpriteSet : MonoBehaviour
{
    public int index;
    public bool indexSeted = false;
    public bool skinFalse = true;
    [SerializeField]List<SpriteResolver> resolvers = new List<SpriteResolver>();

    public void setUpIndividualSkin(int itemIndex)
    {
        index = itemIndex;
        foreach(var a in resolvers)
        {
            a.SetCategoryAndLabel(a.GetCategory(), a.GetCategory() + "_" + (index + 1));
        }
        indexSeted = true;
    }

    private void Update()
    {
        if (!indexSeted)
        {
            return;
        }
        else if(skinFalse)
        {
            foreach (var a in resolvers)
            {
                if(a.GetLabel()!= a.GetCategory() + "_" + (index + 1))
                {
                    a.SetCategoryAndLabel(a.GetCategory(), a.GetCategory() + "_" + (index + 1));
                    return;
                }                    
            }
            skinFalse = false;
            UiShop.Instance.checkItem();
            this.enabled = false;
        }       
    }
}
