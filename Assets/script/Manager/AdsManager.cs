using Mycom.Tracker.Unity;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using UnityEngine.SceneManagement;
using OneSignalSDK;

public class AdsManager : Singleton<AdsManager>
{
    [SerializeField]public GameObject blockPannel;
    [SerializeField]UILoadScreen splash;
    
    string rewardAdUnitId = "ca-app-pub-4810228587303243/7337788136";
    string InterstitialAdUnitId = "ca-app-pub-4810228587303243/6727144544";
    string bannerAdUnitId = "ca-app-pub-4810228587303243/4292552890";
    string AppOpenAdUnitId = "ca-app-pub-4810228587303243/5030919491";
    public bool gameIsOn, hasInternet/*, showingAds = false*/;

    int retryAttempt;
    public bool openAdsLoadFailed = false;
    bool adsPlaying = false;
    bool canShowOpen = false;

    InterstitialAd interstitialAd;

    AppOpenAd appOpenAd;
    BannerView _bannerView;
    RewardedAd rewardedAd;

    public enum InterstitialAds
    {
        defaultAds,
        fromHomeAds,
        fromGamePlayAds,
        none
    }

    public string getInterstitialAdsId(InterstitialAds InterstitialAdsType)
    {
        switch (InterstitialAdsType)
        {
            case InterstitialAds.fromGamePlayAds:
                return InterstitialAdUnitId;
            case InterstitialAds.fromHomeAds:
                return InterstitialAdUnitId;
            case InterstitialAds.none:
                return null;
            default:
                return InterstitialAdUnitId;
        }
    }

    public bool InterstitialInnit(string adsUnitId) // check if ads watched
    {
        bool adsInnit;
        if (adsUnitId == null)
        {
            return false;
        }
        if (!rewardedAd.CanShowAd() || rewardedAd == null)
        {
            LoadInterAd();
            return false;
        }
        if (interstitialAd.CanShowAd())
        {
            ShowInterAd();
            adsInnit = true;
        }
        else
        {
            adsInnit = false;
        }
        return adsInnit;
    }

    public bool checkInterval()
    {
        DateTime interval = DateTime.Parse(PlayerPrefs.GetString("show_interstitial", DateTime.Now.AddSeconds(-GameManager.Instance.Show_interstitial_interval).ToString()));
        if (DateTime.Now >= interval.AddSeconds(GameManager.Instance.Show_interstitial_interval))
        {
            return true;
        }
        return false;
    }

    public enum rewardAdsType
    {
        skin,
        level,
        hint
    }
    public rewardAdsType adsToCallBack;
    public int itemIndex;
    public ShopElementItem itemToBuy;
    [Obsolete]
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        gameIsOn = true;
        StartCoroutine(CheckInternetConnection());
#if UNITY_EDITOR
#else
            Debug.unityLogger.filterLogType = LogType.Warning;
            Debug.unityLogger.filterLogType = LogType.Error;
#endif
        var myTrackerConfig = MyTracker.MyTrackerConfig;

        // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID from app.onesignal.com
        OneSignal.Default.Initialize("062184d7-df3c-4081-835b-ec0dfee297e4");
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize((InitializationStatus initStatus) =>
        {
            // This callback is called once the MobileAds SDK is initialized.
        });


        LoadInterAd();
        LoadRewardedAd();
        LoadBanerAd();
        LoadAppOpenAd();
#if UNITY_ANDROID
        MyTracker.Init("94486491233024571824");
#endif
        if (splash!=null)
            splash.loadSplash();
    }    
    
    public enum adsType
    {
        inter,
        open,        
        reward
    }

    public bool getAdsStat(adsType AdsType)
    {
        bool result = false;
        switch (AdsType)
        {            
            case adsType.inter:
                result = interstitialAd.CanShowAd();
                break;
            case adsType.open:
                result = appOpenAd.CanShowAd();
                break;
            case adsType.reward:
                result = rewardedAd.CanShowAd();
                break;
            default:
                result = false;
                break;
        }
        return result;
    }

    [Obsolete]
    IEnumerator CheckInternetConnection() //set lai. gameIsOn thanh` true;
    {
        while (gameIsOn)
        {
            yield return new WaitForSeconds(1.5f);
            const string echoServer = "http://www.example.com";

            bool result;
            using (var request = UnityWebRequest.Head(echoServer))
            {
                request.timeout = 3;
                yield return request.SendWebRequest();
                result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
            }
            if (result)
                hasInternet = true;
            else
            {
                hasInternet = false;
                /*blockPannel.SetActive(!hasInternet);*/
            }

        }
    }

    /*public void turnOffBlockPannel()
    {
        if (hasInternet)
        {
            blockPannel.SetActive(!hasInternet);
        }
    }*/
    

    
    #region Banner
    public void CreateBannerView()
    {
        Debug.Log("Creating banner view");

        // If we already have a banner, destroy the old one.
        if (_bannerView != null)
        {
            DestroyAd();
        }

        // Create a 320x50 banner at bottom of the screen
        AdSize adaptiveSize = AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);
        _bannerView = new BannerView(bannerAdUnitId, adaptiveSize, AdPosition.Bottom);


    }
    public void LoadBanerAd()
    {
        // create an instance of a banner view first.
        if (_bannerView == null)
        {
            CreateBannerView();
        }
        // create our request used to load the ad.
        var adRequest = new AdRequest.Builder()
            .AddKeyword("unity-admob-sample")
            .Build();

        // send the request to load the ad.
        Debug.Log("Loading banner ad.");
        _bannerView.LoadAd(adRequest);
        ListenToAdEvents();
    }
    private void ListenToAdEvents()
    {
        // Raised when an ad is loaded into the banner view.
        _bannerView.OnBannerAdLoaded += () =>
        {
            retryAttempt = 0;

        };
        // Raised when an ad fails to load into the banner view.
        _bannerView.OnBannerAdLoadFailed += (LoadAdError error) =>
        {
            if (hasInternet == true)
            {
                retryAttempt++;
                double retryDelay = Math.Pow(2, Math.Min(6, retryAttempt));

                /*Invoke("LoadInterstitial", (float)retryDelay);*/
                StartCoroutine(reLoadAds(retryDelay));

            }



        };
        // Raised when the ad is estimated to have earned money.
        _bannerView.OnAdPaid += (AdValue adValue) =>
        {

        };
        // Raised when an impression is recorded for an ad.
        _bannerView.OnAdImpressionRecorded += () =>
        {

        };
        // Raised when a click is recorded for an ad.
        _bannerView.OnAdClicked += () =>
        {

        };
        // Raised when an ad opened full screen content.
        _bannerView.OnAdFullScreenContentOpened += () =>
        {

        };
        // Raised when the ad closed full screen content.
        _bannerView.OnAdFullScreenContentClosed += () =>
        {

        };
    }

    IEnumerator reLoadAds(double retryDelay)
    {
        yield return new WaitForSeconds((float)retryDelay);
        LoadBanerAd();
    }


    public void DestroyAd()
    {
        if (_bannerView != null)
        {
            Debug.Log("Destroying banner ad.");
            _bannerView.Destroy();
            _bannerView = null;
        }
    }
    #endregion
    #region inter

    public void ShowInterAd()
    {
        if (hasInternet == false)
        {
            return;
        }
        if (!interstitialAd.CanShowAd())
        {
            LoadInterAd();
        }        
        //Debug.LogError("inter ad: " + interAd.CanShowAd() );
        if (interstitialAd != null && interstitialAd.CanShowAd())
        {
            /*Debug.Log("show");*/
            canShowOpen = false;
            interstitialAd.Show();            
        }
        else
        {
            canShowOpen = false;
            Debug.LogError("Interstitial ad is not ready yet.");
        }
    }


    public void LoadInterAd()
    {        
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
            interstitialAd = null;
        }

        Debug.Log("Loading the interstitial ad.");

        // create our request used to load the ad.
        var adRequest = new AdRequest.Builder()
                .AddKeyword("unity-admob-sample")
                .Build();


        // Debug.LogError("id: " + key);
        // send the request to load the ad.
        InterstitialAd.Load(InterstitialAdUnitId, adRequest,
            (InterstitialAd ad, LoadAdError error) =>
            {
                // if error is not null, the load request failed.
                if (error != null || ad == null)
                {

                    return;
                }



                interstitialAd = ad;
                ad.OnAdPaid += (AdValue adValue) =>
                {

                };
                // Raised when an impression is recorded for an ad.
                ad.OnAdImpressionRecorded += () =>
                {

                };
                // Raised when a click is recorded for an ad.
                ad.OnAdClicked += () =>
                {

                };
                // Raised when an ad opened full screen content.
                ad.OnAdFullScreenContentOpened += () =>
                {

                };
                // Raised when the ad closed full screen content.
                ad.OnAdFullScreenContentClosed += () =>
                {                    
                    LoadInterAd();                    
                };
                // Raised when the ad failed to open full screen content.
                ad.OnAdFullScreenContentFailed += (AdError error) =>
                {
                    Debug.LogError("inter: " + hasInternet);
                    if (hasInternet == true)
                    {
                        Debug.LogError("Load Inter");
                        LoadInterAd();
                    }


                };


            });
    }


    public void DestroyInterstitialAd()
    {
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }
    }

    #endregion
    #region reward
    public void ShowRewardedAd()
    {
        const string rewardMsg =
            "Rewarded ad rewarded the user. Type: {0}, amount: {1}.";

        if (!rewardedAd.CanShowAd())
        {
            LoadRewardedAd();
        }
        if (rewardedAd != null && rewardedAd.CanShowAd() && GameManager.Instance.Enable_reward_ads)
        {
            rewardedAd.Show((Reward reward) =>
            {
                // TODO: Reward the user.
                Debug.Log(String.Format(rewardMsg, reward.Type, reward.Amount));
            });
            /*showingAds = true;*/            
            adsPlaying = true;
        }        
    }
    public void LoadRewardedAd()
    {
        // Clean up the old ad before loading a new one.
        if (rewardedAd != null)
        {
            rewardedAd.Destroy();
            rewardedAd = null;
        }

        Debug.Log("Loading the rewarded ad.");

        // create our request used to load the ad.
        var adRequest = new AdRequest.Builder().Build();

        // send the request to load the ad.
        RewardedAd.Load(rewardAdUnitId, adRequest,
            (RewardedAd ad, LoadAdError error) =>
            {
                // if error is not null, the load request failed.
                if (error != null || ad == null)
                {

                    return;
                }                
                rewardedAd = ad;
                RegisterEventHandlersReward(ad);
            });
    }
    private void RegisterEventHandlersReward(RewardedAd ad)
    {
        // Raised when the ad is estimated to have earned money.
        ad.OnAdPaid += (AdValue adValue) =>
        {

        };
        // Raised when an impression is recorded for an ad.
        ad.OnAdImpressionRecorded += () =>
        {
            Debug.Log("Rewarded ad recorded an impression.");
        };
        // Raised when a click is recorded for an ad.
        ad.OnAdClicked += () =>
        {
            Debug.Log("Rewarded ad was clicked.");
        };
        // Raised when an ad opened full screen content.
        ad.OnAdFullScreenContentOpened += () =>
        {
            Debug.Log("Rewarded ad full screen content opened.");
        };
        // Raised when the ad closed full screen content.
        ad.OnAdFullScreenContentClosed += () =>
        {
            Debug.Log("Rewarded ad full screen content closed.");
            //trao thuong                        
        };
        // Raised when the ad failed to open full screen content.
        ad.OnAdFullScreenContentFailed += (AdError error) =>
        {
            if (hasInternet == true)
            {
                Debug.LogError("Load reward");
                LoadRewardedAd();
            }


        };
    }

    public void RewardInnit()
    {
        switch (adsToCallBack)
        {
            case rewardAdsType.level:
                UiLevel.Instance.clickedLevel.GetComponent<LevelElement>().UnlockLevel();
                UiLevel.Instance.LevelPopup(false);
                break;
            case rewardAdsType.skin:
                GameData.updateListAdItem(itemIndex, 1);
                itemToBuy.checkState();
                UiShop.Instance.shopPopUp(false, 0, null);
                break;
            case rewardAdsType.hint:
                HintManager.Instance.toggleHint(true);
                PlayerPrefs.SetInt("Hint_Level_" + GameData.GetCurrentLevel(), 1);
                break;
        }
        adsPlaying = false;
    }

    #endregion
    #region AppOpen

    public void ShowAppOpenAd()
    {        
        if (appOpenAd != null && appOpenAd.CanShowAd())
        {            
            Debug.Log("Showing app open ad.");            
            appOpenAd.Show();
            canShowOpen = false;
        }
        else
        {
            LoadAppOpenAd();
        }

    }
    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            .AddKeyword("unity-admob-sample")
            .Build();
    }


    public void LoadAppOpenAd()
    {
        if (appOpenAd != null)
        {
            appOpenAd.Destroy();
            appOpenAd = null;
        }        
        AppOpenAd.Load(AppOpenAdUnitId, ScreenOrientation.Portrait, CreateAdRequest(),
            (AppOpenAd ad, LoadAdError error) =>
            {
              // if error is not null, the load request failed.
              if (error != null || ad == null)
                {
                    openAdsLoadFailed = true;
                    return;
                }

                Debug.Log("App open ad loaded with response : "
                          + ad.GetResponseInfo());

                appOpenAd = ad;

                ad.OnAdPaid += (AdValue adValue) =>
                {

                };
              // Raised when an impression is recorded for an ad.
              ad.OnAdImpressionRecorded += () =>
                {

                };
              // Raised when a click is recorded for an ad.
              ad.OnAdClicked += () =>
                {

                };
              // Raised when an ad opened full screen content.
              ad.OnAdFullScreenContentOpened += () =>
                {

                };
              // Raised when the ad closed full screen content.
              ad.OnAdFullScreenContentClosed += () =>
                {
                    
                };
              // Raised when the ad failed to open full screen content.
              ad.OnAdFullScreenContentFailed += (AdError error) =>
                {
                    if (hasInternet == true)
                    {
                        Debug.LogError("Load appopent");
                        LoadAppOpenAd();
                    }

                };
            });
    }   

    private void OnApplicationPause(bool pause)
    {
        if (adsPlaying && pause == false)
        {
            RewardInnit();
            return;
        }
        if(!canShowOpen && pause == false)
        {
            canShowOpen= true;
            return;
        }
        if (hasInternet)
        {
            if (canShowOpen && pause == false)                
                ShowAppOpenAd();
            return;
        }
            
    }
    

    public void DestroyAppOpenAd()
    {
        if (this.appOpenAd != null)
        {
            this.appOpenAd.Destroy();
            this.appOpenAd = null;
        }
    }
    #endregion   
}
