using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;

public class HintManager : Singleton<HintManager>
{    
    [SerializeField] GameObject hintByLevel;

    public void SetHintByLevel(GameObject hint)
    {
        if (hint == null)
            return;
        hintByLevel = hint;
    }

    public void toggleHint(bool stat)
    {
        if (hintByLevel == null)
            return;
        hintByLevel.SetActive(stat);
    }    

    public void CheckForHintStat(int levelIndex)
    {
        if (hintByLevel == null)
            return;
        if (PlayerPrefs.GetInt("Hint_Level_" + levelIndex,0) == 1)
        {
            toggleHint(true);
        }
        else
        {
            toggleHint(false);
        }
        
    }
}
