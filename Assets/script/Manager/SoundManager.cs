using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    public enum SoundID
    {
        Draw,
        Success,
        Fail,
        Click,
        Eat,
        Cat,
        Trap,
        Mouse,
        Run,
        MouseHouse,
        BG1,
        BG2,
        Star1,
        Star2,
        Star3,
        bookFlip
    }
    public enum SoundType
    {
        OneShot,
        Loop,
        Theme,
        Ends,
        UI
    }

    [System.Serializable]
    public class Sound
    {
        public SoundID soundID;
        public SoundType soundType;
        public AudioClip clip;
        public bool loop;
    }    

    /*uisound = Btn + endanim
    endsound = win + lose
    loop = draw,run,mouse
    oneshot = click,eat,trap,house,cat
    theme = bg*/

    /*[SerializeField] ButtonManager btnManager;*/
    [SerializeField] GameObject theme;
    [SerializeField] GameObject loop;
    [SerializeField] GameObject oneshot;
    [SerializeField] GameObject ui;
    [SerializeField] GameObject end;


    
    AudioSource themeSound;
    AudioSource OneShotSource;
    AudioSource LoopSource;
    AudioSource uiSource;
    AudioSource endSource;
    [SerializeField] Sound[] sounds;    
    // Start is called before the first frame update
    SoundID currentSoundType;

    private void Awake()
    {
        themeSound = theme.GetComponent<AudioSource>();
        OneShotSource = oneshot.GetComponent<AudioSource>();
        LoopSource = loop.GetComponent<AudioSource>();
        uiSource = ui.GetComponent<AudioSource>();
        endSource = end.GetComponent<AudioSource>();
    }


    private void Start()
    {        
        
        OneShotSource.volume = GameData.GetVolumeValue();
        themeSound.volume = GameData.GetVolumeValue();
        LoopSource.volume = GameData.GetVolumeValue();
        uiSource.volume = GameData.GetVolumeValue();
        endSource.volume = GameData.GetVolumeValue();

        Debug.Log(themeSound.gameObject.name);
        Debug.Log(OneShotSource.gameObject.name);
        Debug.Log(LoopSource.gameObject.name);

        if (UIHome.Instance.gameObject.activeInHierarchy)
        {
            PlaySound(SoundID.BG1, 0);
        }
    }
    

    public void PlaySound(SoundID soundID,float delayTime)
    {        
        foreach (var x in sounds)
        {
            if (x.soundID == soundID)
            {
                switch (x.soundType)
                {
                    case SoundType.OneShot:
                        playerMachine(OneShotSource, x, delayTime);
                        break;
                    case SoundType.Loop:
                        playerMachine(LoopSource, x, delayTime);
                        break;
                    case SoundType.Theme:
                        playerMachine(themeSound, x, delayTime);
                        break;
                    case SoundType.Ends:
                        playerMachine(endSource, x, delayTime);
                        break;
                    case SoundType.UI:
                        playerMachine(uiSource, x, delayTime);
                        break;
                }
            }
        }                
    }    
    void playerMachine(AudioSource player,Sound sound, float delayTime)
    {
        player.clip = sound.clip;
        if (delayTime != 0)
        {
            player.PlayDelayed(delayTime);
        }
        else
        {
            player.Play();
        }

    }

    public void changeThemeSound(SoundID ID)
    {
        foreach(var x in sounds)
        {
            if(x.soundID == ID)
            {
                themeSound.Stop();
                themeSound.clip = x.clip;
                themeSound.Play();                
            }
        }
    }

    public void soundSwitch(bool soundStat)
    {
        int volume;
        volume = soundStat == true ? 1 : 0; 
        GameData.SetVolumeValue(volume);
        themeSound.volume = GameData.GetVolumeValue();
        OneShotSource.volume = GameData.GetVolumeValue();
        LoopSource.volume = GameData.GetVolumeValue();
        uiSource.volume = GameData.GetVolumeValue();
        endSource.volume = GameData.GetVolumeValue();
    }

    public void StopSound(SoundID ID)
    {        
        foreach (var a in sounds)
        {
            if(a.soundID == ID )
            {
                switch (a.soundType)
                {
                    case SoundType.OneShot:
                        OneShotSource.Stop();
                        break;
                    case SoundType.Loop:
                        LoopSource.Stop();
                        break;
                    case SoundType.Theme:
                        themeSound.Stop();
                        break;
                    case SoundType.Ends:
                        endSource.Stop();
                        break;
                    case SoundType.UI:
                        uiSource.Stop();
                        break;
                }
            }
        }
    }
}
