using Firebase;
using Firebase.Analytics;
using System;
using UnityEngine;

public class FirebaseInit : Singleton<FirebaseInit>
{
    private void Awake()
    {
        if (FirebaseInit.Instance == null)
            FirebaseInit.Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

        if (AdsManager.Instance.hasInternet)
        {
            checkDateTime();
        }
    }

    void checkDateTime()
    {
        DateTime date = DateTime.Parse(PlayerPrefs.GetString("PlayerLogIn", DateTime.Now.ToString()));
        DateTime currentDate = DateTime.Now;
        if(currentDate >= date.AddDays(1))
        {
            PlayerPrefs.SetString("PlayerLogIn", DateTime.Now.ToString());
            playerLogin();
        }
    }

    private void OnEnable()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void WatchAds()
    {
        Debug.Log("log event watch ads");
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAdImpression);
    }

    /*#region WatchAds

    public void WatchAdsRewardCoin()
    {
        FirebaseAnalytics.LogEvent("USER_WATCH_ADS_COIN");
    }
    public void WatchAdsRewardBomb()
    {
        FirebaseAnalytics.LogEvent("USER_WATCH_ADS_BOM");
    }
    public void WatchAdsRewardWater()
    {
        FirebaseAnalytics.LogEvent("USER_WATCH_ADS_WATER");
    }
    public void WatchAdsDoubleReward()
    {
        FirebaseAnalytics.LogEvent("USER_WATCH_ADS_X2_REWARD");
    }
    #endregion*/

    #region PlayGame    
    
    
    public void LevelXPassed(int level)
    {
        Debug.Log("log event pass level");
        try
        {
            FirebaseAnalytics.LogEvent("USER_WIN", "LEVEL", level);

        } catch (Exception e)
        {
            Debug.Log("zzz " + e);
        }
    }

    public void playerLost(int level)
    {

        Debug.Log("log event lost level");
        FirebaseAnalytics.LogEvent("USER_LOST", "LEVEL", level);
    }

    public void playerStuckAtLevel(int level)
    {
        Debug.Log("log event stuck level");
        FirebaseAnalytics.LogEvent("USER_STUCK_At","LEVEL", + level);
    }
    /*public void skipLevel(int level)
    {
        FirebaseAnalytics.LogEvent("USER_SKIP","LEVEL", level);
    }*/
    #endregion

    #region Daily

    public void playerLogin()
    {
        Debug.Log("log event login");
        FirebaseAnalytics.LogEvent("USER_LOGIN");
    }

    /*public void CollectDailyCheckIn()
    {                    
        FirebaseAnalytics.LogEvent("USER_COLLECT_DAILY");
    }      
    #endregion

    #region Shop
    public void SuccesBuyItem()
    {
        FirebaseAnalytics.LogEvent("USER_SUCCESS_BUY_ITEM");
    }
    public void InAppPurchase()
    {
        FirebaseAnalytics.LogEvent("USER_PAYMONEY");
    }
    public void BuyingRemoveAds()
    {
        FirebaseAnalytics.LogEvent("USER_REMOVE_ADS");
    }
    #endregion

    #region achivement
    public void ClaimAchievement()
    {
        FirebaseAnalytics.LogEvent("USER_CLAIM_ACHIEVEMENT");
    }*/
    #endregion


}
