using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickEffect_Trail : MonoBehaviour/*, IDragHandler*/
{

    [SerializeField] ParticleSystem mouseVfx;    

    Vector2 mouse;


    // Update is called once per frame
    void Update()
    {
        Mouse();
    }

    void Mouse()
    {
        if (Input.touchCount == 1)
        {
            Touch screenTouch = Input.GetTouch(0);
            if(screenTouch.phase == TouchPhase.Began || screenTouch.phase == TouchPhase.Moved)
            {
                mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mouseVfx.transform.position = new Vector3(mouse.x, mouse.y, 0f);
                mouseVfx.Play();
            }
        }
    }

    /*public void OnDrag(PointerEventData eventData)
    {
        mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseVfx.transform.position = new Vector3(mouse.x, mouse.y, 0f);
        mouseVfx.Play();
    }*/
}
