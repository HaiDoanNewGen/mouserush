using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingBGAnim : MonoBehaviour
{
    [Range(-1f, 1f)]
    public float scrollSpeed = 0.5f;
    private float Offset;
    private Material mat;

    private void Start()
    {
        mat = GetComponent<Renderer>().material;
        var tr = GetComponent<Renderer>();
        tr.sortingLayerName = "BackGroundLoop";
    }

    private void Update()
    {
        Offset += (Time.deltaTime * scrollSpeed) / 10f;
        mat.SetTextureOffset("_MainTex", new Vector2(Offset, -Offset));
    }
}
