using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class TransitionAnim : MonoBehaviour
{
    [SerializeField] float InTargetAlpha;
    [SerializeField] float OutTargetAlpha;    
    [SerializeField] float animDuration = 0.5f;
    [SerializeField] bool playOnEnable = false;
    [SerializeField] FadeStyle style = FadeStyle.FadeIn;
    [SerializeField] GameObject loadingText;

    private void OnEnable()
    {
        if (playOnEnable)
        {
            startTransition(style,false);
        }                  
    }
    public enum FadeStyle
    {
        FadeIn,
        FadeOut
    }
    public void startTransition(FadeStyle style,bool afterStat)
    {        
        if (style == FadeStyle.FadeOut)
        {
            StartCoroutine(transitionner(OutTargetAlpha, animDuration, afterStat));
            StartCoroutine(loading(true));
        }
        else
        {
            StartCoroutine(transitionner(InTargetAlpha, animDuration, afterStat));
            StartCoroutine(loading(false));
        }
    }    
    IEnumerator transitionner(float targetAlpha,float animDuration,bool afterstat)
    {        
        GetComponent<Image>().DOFade(targetAlpha, animDuration);
        yield return new WaitForSeconds(animDuration);
        if(!afterstat)
            this.gameObject.SetActive(false);
    }

    IEnumerator loading(bool stat)
    {        
        yield return new WaitForSeconds(0.5f);
        loadingText.SetActive(stat);
    }
}
