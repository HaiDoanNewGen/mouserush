using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnOffAnimationToggle : MonoBehaviour
{
    [SerializeField] List<OnOffAnimList> listAnim;
    bool Toggle = true;
    public void ToggleBtn()
    {
        Debug.Log(Toggle);
        if (Toggle)
        {
            foreach(var a in listAnim)
            {
                a.OnAnim.ForceResetAnim();
            }
            Toggle = false;
        }
        else
        {
            foreach (var a in listAnim)
            {
                a.OffAnim.ForceResetAnim();
            }
            Toggle = true;
        }
    }

    private void OnDisable()
    {
        foreach(var a in listAnim)
        {
            a.gameObject.transform.localPosition = a.defaultPos;
            a.gameObject.transform.localRotation = a.defaultRotate;
        }
        Toggle = true;
    }
}
