using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickEffect : MonoBehaviour
{

    [SerializeField] GameObject mouseVfx;
    List<GameObject> vfxPool = new List<GameObject>();

    Vector2 mouse;

    // Start is called before the first frame update
    void Start()
    {
        mouseVfx.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Mouse();
    }

    void Mouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (vfxPool.Count > 0)
            {
                foreach(var x in vfxPool)
                {
                    if (!x.activeInHierarchy)
                    {                        
                        x.transform.position = new Vector3(mouse.x, mouse.y, 0f);
                        x.SetActive(true);
                        StartCoroutine(delayTurnOff(x));
                        return;
                    }
                }
                GameObject tempVfx = Instantiate(mouseVfx, new Vector3(mouse.x, mouse.y, 0f), Quaternion.Euler(0, 0, 0), this.transform);
                tempVfx.SetActive(true);
                vfxPool.Add(tempVfx);
                StartCoroutine(delayTurnOff(tempVfx));
            }
            else
            {
                GameObject tempVfx = Instantiate(mouseVfx, new Vector3(mouse.x, mouse.y, 0f), Quaternion.Euler(0, 0, 0), this.transform);
                tempVfx.SetActive(true);
                vfxPool.Add(tempVfx);
                StartCoroutine(delayTurnOff(tempVfx));
            }
            
        }
        
    }
    IEnumerator delayTurnOff(GameObject vfx)
    {
        yield return new WaitForSeconds(0.5f);
        vfx.SetActive(false);
    }
}
