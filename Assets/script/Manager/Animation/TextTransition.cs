using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TextTransition : MonoBehaviour
{
    [SerializeField] float InTargetAlpha;
    [SerializeField] float OutTargetAlpha;
    public enum FadeStyle
    {
        FadeIn,
        FadeOut
    }
    public void startTransition(FadeStyle style)
    {
        if (style == FadeStyle.FadeOut)
        {
            GetComponent<TextMeshProUGUI>().DOFade(OutTargetAlpha, 0.5f);
        }
        else
        {
            GetComponent<TextMeshProUGUI>().DOFade(InTargetAlpha, 0.5f);
        }
    }
}
