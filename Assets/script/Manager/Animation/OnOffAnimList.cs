using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnOffAnimList : MonoBehaviour
{
    public Vector2 defaultPos;
    public Quaternion defaultRotate;

    [SerializeField] AnimationController onAnim;
    [SerializeField] AnimationController offAnim;

    public AnimationController OnAnim { get => onAnim; set => onAnim = value; }
    public AnimationController OffAnim { get => offAnim; set => offAnim = value; }

    private void Awake()
    {
        defaultPos = this.gameObject.transform.localPosition;
        defaultRotate = this.gameObject.transform.localRotation;
    }
}
