﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public enum AnimationType
{
    MoveX,
    MoveY,
    MoveXAndY,
    Rotate,
    Scale,
    Shake,
}

public class AnimationController : MonoBehaviour
{
    [SerializeField] bool PlayOnEnable;         
    public AnimationType type;
    [SerializeField] float AnimDuration;
    [SerializeField] Ease animEase;
    [SerializeField] int loop;
    [SerializeField] LoopType loopType;

    [Header("Move")]
    [SerializeField] float moveY;
    [SerializeField] float moveX;

    [Header("Rotate")]
    [SerializeField] RotateMode rotateMode;
    [SerializeField] Vector3 numRotate;

    [Header("Scale")]
    [SerializeField] Vector3 numScale;

    [Header("Shake")]
    [SerializeField] float duration;
    [SerializeField] float strength;
    [SerializeField] int vibration;
    [SerializeField] float randomness;
    [SerializeField] bool snapping;


    /*    public Vector2 DefaultPos;
        public Vector3 DefaultScale;
        public Quaternion DefaultRotation;*/

    /*Sequence sequence;*/
    Tween tweener;
       

    private void OnEnable()
    {        
        if(PlayOnEnable)
        {
            if (tweener != null)
                tweener.Restart();
            else
                ForceResetAnim();
        }
    }

    public void ForceResetAnim()
    {
        /*if(needToResetDefaultPose)
            resetDefaultPose();*/
        /*TypeOfAnimation();*/
        /*TypeOfAnimation();*/
        /*if(sequence == null)
        {
            Debug.Log("uuuu " + this.gameObject.name + " seq null");
        }*/
        TypeOfAnimation();
    }

    private void TypeOfAnimation()
    {
        /*Sequence tempSequence = DOTween.Sequence();*/
        Tween tempTweener = null;
        switch (type)
        {
            case AnimationType.MoveX:
                {
                    /*tempSequence.Append(this.transform.DOLocalMoveX(moveX, AnimDuration).SetEase(animEase).SetLoops(loop, loopType));*/
                    tempTweener = this.transform.DOLocalMoveX(moveX, AnimDuration).SetEase(animEase).SetLoops(loop, loopType);
                    this.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
                    break;
                }
            case AnimationType.MoveY:
                {
                    /*tempSequence.Append(this.transform.DOLocalMoveY(moveY, AnimDuration).SetEase(animEase).SetLoops(loop, loopType));*/
                    tempTweener = this.transform.DOLocalMoveY(moveY, AnimDuration).SetEase(animEase).SetLoops(loop, loopType);
                    this.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
                    break;
                }
            case AnimationType.Rotate:
                {
                    /*tempSequence.Append(this.transform.DOLocalRotate(numRotate, AnimDuration, rotateMode).SetEase(animEase).SetLoops(loop, loopType));*/
                    tempTweener = this.transform.DOLocalRotate(numRotate, AnimDuration, rotateMode).SetEase(animEase).SetLoops(loop, loopType);
                    break;
                }
            case AnimationType.Scale:
                {
                    /*tempSequence.Append(this.transform.DOScale(numScale, AnimDuration).SetEase(animEase).SetLoops(loop, loopType));*/
                    tempTweener = this.transform.DOScale(numScale, AnimDuration).SetEase(animEase).SetLoops(loop, loopType);
                    break;
                }
            case AnimationType.Shake:
                {
                    /*tempSequence.Append(this.transform.DOShakePosition(duration, strength, vibration, randomness, snapping).SetEase(animEase).SetLoops(loop, loopType));*/
                    tempTweener = this.transform.DOShakePosition(duration, strength, vibration, randomness, snapping).SetEase(animEase).SetLoops(loop, loopType);
                    break;
                }
        }
        /*sequence = tempSequence;*/
        tweener = tempTweener;
        if (loop != -1)
            StartCoroutine(CompleteSquene());
    }    
    IEnumerator CompleteSquene()
    {
        yield return new WaitForSeconds(AnimDuration);
        /*Debug.Log(this.gameObject.name + " aaa "+"duration "+AnimDuration);*/
        /*sequence.Complete();*/
        tweener.Complete();
    }

    /*public void resetDefaultPose()
    {
        transform.localPosition = DefaultPos;
        transform.rotation = DefaultRotation;
        transform.localScale = DefaultScale;
    }*/
    private void OnDisable()
    {
        /*sequence.Pause();*/
        tweener.Pause();
    }
}
