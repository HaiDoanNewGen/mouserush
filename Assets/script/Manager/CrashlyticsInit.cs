
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Import Firebase
using Firebase;
using Firebase.Extensions;
using System;
using System.Threading.Tasks;
using Firebase.RemoteConfig;
using UnityEngine.UI;
using Firebase.Crashlytics;
using Firebase.Analytics;

public class CrashlyticsInit : MonoBehaviour
{    
    // Use this for initialization 
    void Start()
    {
        Debug.Log("zzz: start innnitialize crashlytics");
        //Firebase.FirebaseApp.LogLevel = Firebase.LogLevel.Debug;

        // Initialize Firebase
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                Debug.Log("zzz firebase dependency available");
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.                                               
                Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
                Debug.Log("zzz ");
                InitializeFirebase();
                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
        Firebase.FirebaseApp.LogLevel = Firebase.LogLevel.Debug;
        //Crashlytics.IsCrashlyticsCollectionEnabled = true;
        //Debug.Log("::"+Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue("key_string").StringValue);

    }
    void InitializeFirebase()
    {
        Debug.Log("zzz remote config innitial");
        //TestAdd
        System.Collections.Generic.Dictionary<string, object> defaults =
            new System.Collections.Generic.Dictionary<string, object>();

        //// These are the values that are used if we haven't fetched data from the
        //// server
        //// yet, or if we ask for values that the server doesn't have:
        defaults.Add("config_test_string", "default local string");
        defaults.Add("config_test_int", 1);
        defaults.Add("config_test_float", 1.0);
        defaults.Add("config_test_bool", false);
        Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults);
        Debug.Log("Remote config ready!");
        //ShowData();
        FetchFireBase();
    }
    public void FetchFireBase()
    {
        FetchDataAsync();
    }
    public void ShowData()
    {
        GameManager gameManager = Singleton<GameManager>.Instance;

        print("Get Remote Config Key+++++++++");

        try
        {
            //IDictionary<string, ConfigValue> values = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.AllValues;
            //print(Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue("key_gift_point").DoubleValue);
            var z = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue("show_insterstitial_after_finished_levels").StringValue;
            //DebugFirebaseConfigValue(z);
            print($"zzzz :::{z}");
        }
        catch (Exception ex)
        {
            print(ex);
        }


        var Banner = FirebaseRemoteConfig.DefaultInstance.GetValue("enable_banner_ads").BooleanValue;
        var Interstitial = FirebaseRemoteConfig.DefaultInstance.GetValue("enable_interstitial_ads").BooleanValue;
        var OpenAppAds = FirebaseRemoteConfig.DefaultInstance.GetValue("enable_open_app_ads").BooleanValue;
        var ReplayAds = FirebaseRemoteConfig.DefaultInstance.GetValue("enable_replay_ads").BooleanValue;
        var RewardAds = FirebaseRemoteConfig.DefaultInstance.GetValue("enable_reward_ads").BooleanValue;
        var levelList = FirebaseRemoteConfig.DefaultInstance.GetValue("show_insterstitial_after_finished_levels").StringValue;
        var adsTimeBreak = FirebaseRemoteConfig.DefaultInstance.GetValue("show_interstitial_interval").DoubleValue;
        var adsFromGameplay = FirebaseRemoteConfig.DefaultInstance.GetValue("enableAdsFromGameplay").BooleanValue;
        var adsFromHome = FirebaseRemoteConfig.DefaultInstance.GetValue("enableAdsFromHome").BooleanValue;
        var lvlResetTime = FirebaseRemoteConfig.DefaultInstance.GetValue("LvlReset_Times_Ads").DoubleValue;

        //Set value
        GameManager.Instance.Enable_banner_ads = Banner;        
        GameManager.Instance.Enable_interstitial_ads = Interstitial;        
        GameManager.Instance.Enable_open_app_ads = OpenAppAds;
        GameManager.Instance.Enable_replay_ads = ReplayAds;
        GameManager.Instance.Enable_reward_ads = RewardAds;
        GameManager.Instance.Show_insterstitial_after_finished_levels = levelList.Split(',');
        GameManager.Instance.Enable_ads_from_gameplay = adsFromGameplay;
        GameManager.Instance.Enable_ads_from_home = adsFromHome;
        GameManager.Instance.Lvlreset_time_ads = (int)lvlResetTime;
        GameManager.Instance.Show_interstitial_interval = adsTimeBreak;
        

        print($"zzzz ::: set val Done ");
        

    }

    public void DebugFirebaseConfigValue(ConfigValue item)
    {
        try
        {
            print("Debug ConfigValue Start -----------------");
            print("Source:" + item.Source);
            print("StringValue:" + item.StringValue);
            print("DoubleValue:" + item.DoubleValue);
            print("Debug ConfigValue End -----------------");
        }
        catch (Exception ex)
        {
            print("DebugFirebaseConfigValue Exception:" + ex.Message);
        }

    }

    // Start a fetch request.
    public Task FetchDataAsync()
    {        
        // FetchAsync only fetches new data if the current data is older than the provided
        // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
        // By default the timespan is 12 hours, and for production apps, this is a good
        // number.  For this example though, it's set to a timespan of zero, so that
        // changes in the console will always show up immediately.
        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.FetchAsync(
            TimeSpan.Zero);


        return fetchTask.ContinueWith(FetchComplete);
    }

    void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            Debug.Log("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            Debug.Log("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            Debug.Log("Fetch completed successfully!");
        }

        var info = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.Info;
        switch (info.LastFetchStatus)
        {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.FetchAndActivateAsync();
                Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).",
                    info.FetchTime));
                ShowData();
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                InitializeFirebase();
                switch (info.LastFetchFailureReason)
                {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.Log("Fetch failed for unknown reason");
                        break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                        break;
                }
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.Log("zzz: "+"Latest Fetch call still pending.");
                break;
        }
    }
}