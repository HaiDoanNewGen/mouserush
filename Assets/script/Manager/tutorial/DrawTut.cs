using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class DrawTut : MonoBehaviour
{
    [SerializeField] GameObject handTut;
    [SerializeField] Image lineTut;

    [SerializeField] Transform direction;

    bool increase = true;

    void Start()
    {
        if (!PlayerPrefs.HasKey("FirstTimeOpen"))
        {
            PlayerPrefs.SetInt("FirstTimeOpen", 1); 
            handTut.SetActive(true);
            lineTut.enabled = true;
            //play the animation
            handTut.transform.DOMove(direction.position, 3f).SetEase(Ease.Flash).SetLoops(-1,LoopType.Yoyo);
            StartCoroutine(ChangeSomeValue(0, 1, 3f));
        }
        else
        {
            handTut.SetActive(false);
            lineTut.enabled = false;
            this.enabled = false;
        }
    }

    private void Update()
    {
        if (Input.touchCount == 1 && !UiGameplay.Instance.tutorial)
        {
            Touch screenTouch = Input.GetTouch(0);
            if (screenTouch.phase == TouchPhase.Moved)
            {
                handTut.SetActive(false);
                lineTut.enabled = false;
                this.enabled = false;
            }
        }
    }

    public IEnumerator ChangeSomeValue(float oldValue, float newValue, float duration)
    {
        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            lineTut.fillAmount = Mathf.Lerp(oldValue, newValue, t / duration);
            yield return null;
        }
        lineTut.fillAmount = newValue;
        increase = !increase;
        startAgain();
    }

    void startAgain()
    {
        int oldInt,newInt;
        if (increase)
        {
            oldInt = 0;
            newInt = 1;
        }
        else
        {
            oldInt = 1;
            newInt = 0;
        }
        StartCoroutine(ChangeSomeValue(oldInt, newInt, 3f));
    }
}
