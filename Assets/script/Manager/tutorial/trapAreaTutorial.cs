using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trapAreaTutorial : MonoBehaviour
{
    public enum tutType
    {
        cat,
        trap,
        flower,
        broom
    }

    [SerializeField] tutType TutorialType;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("tutType" + TutorialType.ToString()) == 1)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerPrefs.SetInt("tutType" + TutorialType.ToString(), 1);
        }
    }
}
