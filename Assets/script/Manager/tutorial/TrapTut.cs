using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TrapTut : MonoBehaviour
{
    [Serializable]
    public struct TrapTutorial
    {
        public List<Sprite> tutImages;
        public int levelIndex;
    }


    [SerializeField] Button NextBtn;
    [SerializeField] Button PrevBtn;
    [SerializeField] Button Exit;
    [SerializeField] List<TrapTutorial> TutorialList;
    [SerializeField] Image tutImage;
    [SerializeField] TextMeshProUGUI tutNum;
    int imageIndex;
    List<Sprite> listTutImages;

    Vector2 clipboardDefaultPos;

    private void Awake()
    {
        clipboardDefaultPos = transform.GetChild(0).GetChild(0).localPosition;
    }

    public void checkForTut()
    {        
        if (PlayerPrefs.GetInt("TrapTut" + GameData.GetCurrentLevel()) == 1)
            return;
        else
        {
            List<int> levelTuts = new List<int>();
            foreach (var i in TutorialList)
            {
                levelTuts.Add(i.levelIndex);
            }
            if (!levelTuts.Contains(GameData.GetCurrentLevel()))
            {
                return;
            }
        }
        imageIndex = 0;
        PrevBtn.interactable = false;
        NextBtn.interactable = true;
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(0).GetChild(0).GetComponent<AnimationController>().ForceResetAnim();
        UiGameplay.Instance.tutorial = true;
        
        foreach (var a in TutorialList)
        {
            if (GameData.GetCurrentLevel() == a.levelIndex)
            {
                //show tut
                listTutImages = a.tutImages;
                tutImage.sprite = a.tutImages[imageIndex];
                break;
            }
        }
        updateTutText();
    }

    public void pageNav(bool next)
    {
        SoundManager.Instance.PlaySound(SoundManager.SoundID.bookFlip,0);
        if (next)
        {
            Debug.Log("uuu nav next");
            PrevBtn.interactable = true;
            imageIndex++;
            updateTutText();
            if (imageIndex == listTutImages.Count-1)
                NextBtn.interactable = false;
            tutImage.sprite = listTutImages[imageIndex];
        }
        else
        {
            Debug.Log("uuu nav prev");
            NextBtn.interactable = true;
            imageIndex--;
            updateTutText();
            if (imageIndex == 0)
                PrevBtn.interactable = false;
            tutImage.sprite = listTutImages[imageIndex];
        }
    }
    void updateTutText()
    {
        tutNum.text = (imageIndex+1).ToString() + "/" + listTutImages.Count;
    }

    public void turnOffTut()
    {
        SoundManager.Instance.PlaySound(SoundManager.SoundID.Click, 0);
        UiGameplay.Instance.tutorial = false;
        transform.GetChild(0).GetChild(0).localPosition = clipboardDefaultPos;
        transform.GetChild(0).gameObject.SetActive(false);
    }    
}
