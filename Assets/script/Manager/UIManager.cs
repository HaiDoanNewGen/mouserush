using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static AdsManager;

public class UIManager : Singleton<UIManager>
{    
    public int LevelPrefabCount;

    [SerializeField] GameObject UIHOME;
    [SerializeField] GameObject UIGAMEPLAY;
    [SerializeField] GameObject UISETTING;
    [SerializeField] GameObject UICOMPLETE;
    [SerializeField] GameObject UISHOP;
    [SerializeField] GameObject UILEVEL;

    [SerializeField] WatchVideoReward vidReward;
    [SerializeField] GameObject blockScreen;
    InterstitialAds currentAds = InterstitialAds.defaultAds;

    [Serializable]
    public enum UiType
    {
        Home,
        Gameplay,
        Setting,
        Shop,
        Complete,
        Level
    }

    /*private void Awake()
    {
        LevelPrefabCount = new List<GameObject>(Resources.LoadAll<GameObject>("LevelPrefabs"));
    }*/

    public GameObject getCurrentLevelPrefab()
    {
        string tempIndex;
        if(GameData.GetCurrentLevel()+1<10)
            tempIndex = "0"+ (GameData.GetCurrentLevel() + 1).ToString();
        else
            tempIndex = (GameData.GetCurrentLevel() + 1).ToString();

        return Resources.Load<GameObject>("LevelPrefabs/LEVEL_" + tempIndex);
    }


    //rewrite this whole shiet
    IEnumerator DelayToggleObj(GameObject offObj,GameObject delayObj,float duration)
    {
        blockScreen.SetActive(true);
        yield return new WaitForSeconds(duration);
        blockScreen.SetActive(false);
        offObj.SetActive(false);
        if (delayObj != null)
        {
            delayObj.SetActive(true);
            /*vidReward.checkInterstitialAdsByType(currentAds);*/
        }
    }

    public void UiToggleTo(UiType TypeUi)
    {
        switch (TypeUi)
        {
            case UiType.Home:
                if (UILEVEL.activeInHierarchy)
                {
                    UiLevel.Instance.Transition.gameObject.SetActive(true);
                    UiLevel.Instance.Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeOut,false);
                    StartCoroutine(DelayToggleObj(UILEVEL, UIHOME, 0.5f));
                    /*currentAds = InterstitialAds.none;*/
                }
                else
                { //frome shop (diff scene) rework on this (change scene)
                    //unload async shop scene
                    UiShop.Instance.Transition.gameObject.SetActive(true);
                    UiShop.Instance.Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeOut, false);
                    StartCoroutine(DelayToggleObj(UISHOP, UIHOME, 0.5f));
                    /*currentAds = InterstitialAds.defaultAds;*/
                }
                break;
            case UiType.Gameplay:
                // load async gameplay scene
                UiLevel.Instance.Transition.gameObject.SetActive(true);
                UiLevel.Instance.Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeOut, false);
                StartCoroutine(DelayToggleObj(UILEVEL, UIGAMEPLAY, 0.5f));
                /*currentAds = InterstitialAds.none;*/
                break;
            case UiType.Setting:
                break;
            case UiType.Level:
                if (UIHOME.activeInHierarchy)
                {
                    UIHome.Instance.Transition.gameObject.SetActive(true);
                    UIHome.Instance.Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeOut, false);
                    StartCoroutine(DelayToggleObj(UIHOME, UILEVEL, 0.5f));
                    /*currentAds = InterstitialAds.fromHomeAds;*/
                }
                else
                {
                    //unload async gameplay
                    UiGameplay.Instance.Transition.gameObject.SetActive(true);
                    UiGameplay.Instance.Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeOut, false);
                    StartCoroutine(DelayToggleObj(UIGAMEPLAY, UILEVEL, 0.5f));
                    /*currentAds = InterstitialAds.fromGamePlayAds;*/
                }
                
                break;
            case UiType.Shop:
                //load async shop 
                UIHome.Instance.Transition.gameObject.SetActive(true);
                UIHome.Instance.Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeOut, false);
                StartCoroutine(DelayToggleObj(UIHOME, UISHOP, 0.5f));
                /*currentAds = InterstitialAds.fromHomeAds;*/
                break;
            case UiType.Complete:
                Debug.Log("turn on UIcomplete");
                if (UICOMPLETE.activeInHierarchy)
                {
                    StartCoroutine(DelayToggleObj(UICOMPLETE, null,0));
                    return;
                }
                UICOMPLETE.SetActive(true);
                /*currentAds = InterstitialAds.none;*/
                break;
        }
    }    
}
