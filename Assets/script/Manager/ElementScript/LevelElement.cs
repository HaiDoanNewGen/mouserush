using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelElement : MonoBehaviour
{
    [SerializeField] GameObject listStar;
    [SerializeField] GameObject listStarHolder;
    [SerializeField] TextMeshProUGUI LevelNumber;
    [SerializeField] GameObject lockedImage;
    public int level;
    public bool levelLocked = true;    

    public void CheckForLevelStat()
    {
        if (GameData.getCheeseProcess() >= UiLevel.Instance.levelDatabase.getDataByLevel(level).cheeseToUnlock && levelLocked)
        {            
            UnlockLevel();
        }
        if (GameData.GetLevelCheeseObtained(level) > listStar.transform.childCount)
        {
            Debug.LogError("star by level more than list star");
            return;
        }
        for (int i = 0; i < GameData.GetLevelCheeseObtained(level); i++)
        {
            listStar.transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void UnlockLevel()
    {
        Debug.Log(level);
        GameData.UpdateLevelState(level, true);
        levelLocked = false;
        lockedImage.SetActive(levelLocked);
        listStar.SetActive(!levelLocked);
        listStarHolder.SetActive(!levelLocked);
    }

    public void elementSetup(int levelNumb, int starByLvl, bool playable)
    {
        level = levelNumb-1;
        levelLocked = !playable;
        if (starByLvl > listStar.transform.childCount)
        {
            Debug.LogError("star by level more than list star");
            return;
        }
        for(int i = 0; i < starByLvl; i++)
        {
            listStar.transform.GetChild(i).gameObject.SetActive(true);
        }
        LevelNumber.text = levelNumb.ToString();
        lockedImage.SetActive(!playable);
        listStar.SetActive(playable);
        listStarHolder.SetActive(playable);
    }
}
