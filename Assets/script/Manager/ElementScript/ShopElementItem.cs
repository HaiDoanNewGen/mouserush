using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

using UnityEngine.UI;

public class ShopElementItem : MonoBehaviour
{
    [SerializeField] Button elementButton;    
    [SerializeField] GameObject LockedPannel;
    /*[SerializeField] GameObject adsIcon;*/
    [SerializeField] GameObject priceGroup;
    [SerializeField] TextMeshProUGUI cheeseToUnlock;
    [SerializeField] TextMeshProUGUI stateText;
    [SerializeField] Color textUsingColor;
    ItemState state;
    int Itemindex;
    int itemPrice;    
    /*[SerializeField] List<UnityEngine.U2D.Animation.SpriteResolver> listIdleSpriteResolver = new List<UnityEngine.U2D.Animation.SpriteResolver>();*/
    

    public void checkState()
    {        
        if (GameData.GetLevelState(UiShop.Instance.shopData.getProcessToUnlock(Itemindex)) == true && state == ItemState.locked)
        {
            GameData.updateItemState(Itemindex, ItemState.unlocked);
            state = ItemState.unlocked;            
        }
        if (GameData.getAdItemProcess(Itemindex) >= itemPrice && state == ItemState.unlocked)
        {            
            GameData.AddItemOwned(Itemindex);            
            state = ItemState.owned;            
        }
        else if (GameData.getOwnedItem().Contains(Itemindex))
        {
            state = ItemState.owned;
            if (GameData.getSelectedItem() == Itemindex)
            {
                state = ItemState.selected;
            }            
        }
        switch (state)
        {
            case ItemState.locked:
                LockedPannel.SetActive(true);
                stateText.text = "Locked";
                textSwitchState(true);
                break;
            case ItemState.unlocked:
                LockedPannel.SetActive(false);
                priceGroup.SetActive(true);
                if (itemPrice <= 0)
                {
                    stateText.text = "Claim";
                    textSwitchState(true);
                }
                priceGroup.GetComponentInChildren<TextMeshProUGUI>().text = GameData.getAdItemProcess(Itemindex) + "/" + itemPrice.ToString();
                textSwitchState(false);
                break;
            case ItemState.owned:
                LockedPannel.SetActive(false);
                stateText.text = "Owned";
                stateText.color = Color.white;

                textSwitchState(true);
                break;
            case ItemState.selected:
                LockedPannel.SetActive(false);
                stateText.text = "Selected";
                stateText.color = textUsingColor;
                textSwitchState(true);
                break;
        }
        buttonSetup(state);
    } 

    public void textSwitchState(bool StateOn)
    {
        if (StateOn)
        {
            stateText.gameObject.SetActive(true);
            priceGroup.SetActive(false);
        }
        else
        {
            stateText.gameObject.SetActive(false);
            priceGroup.SetActive(true);
        }
    }

    public void elementSetup(string price, ItemState State,int ItemIndex)
    {
        state = State;
        Itemindex = ItemIndex;
        itemPrice = int.Parse(price);           
        /*Instantiate(ImgObject, ElementImageField.transform);*/
        priceGroup.GetComponentInChildren<TextMeshProUGUI>().text = GameData.getAdItemProcess(ItemIndex)+"/"+price;
        buttonSetup(State);
        /*UiShop.Instance.SkinSettup = false;*/
    }       

    public void buttonSetup(ItemState stat)
    {
        switch (stat)
        {
            case ItemState.locked:
                cheeseToUnlock.text = "Unlock at level: " + UiShop.Instance.shopData.getProcessToUnlock(Itemindex).ToString();
                break;
            case ItemState.unlocked:
                // buy it
                elementButton.onClick.RemoveAllListeners();
                elementButton.onClick.AddListener(() => 
                {
                    buyItem(Itemindex);
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
                });
                break;
            case ItemState.owned:
                //change to selected
                elementButton.onClick.RemoveAllListeners();
                elementButton.onClick.AddListener(() =>
                {
                    selectItem(Itemindex);
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
                });
                break;
        }
    }

    public void buyItem(int index)
    {
        // change to watch video to buy skin
        /*if (itemPrice > GameData.GetCheese())
        {
            return;
        }
        if(itemPrice == 0)
        {
            
            return;
        }*/        
        if (AdsManager.Instance.hasInternet)
        {
            UiShop.Instance.shopPopUp(true, Itemindex, this.GetComponent<ShopElementItem>());
        }
        else
        {
            AdsManager.Instance.blockPannel.SetActive(true);
        }
        /*GameData.AddItemOwned(Itemindex);*/
        /*UiShop.Instance.updateCheeseText();*/
        checkState();
    }

    public void selectItem(int index)
    {
        Debug.Log("select item "+index);
        GameData.setSelectedItem(index);
        UiShop.Instance.SelectedItem.GetComponent<ShopElementItem>().checkState();
        /*UiShop.Instance.SelectedItem.SetActive(false);*/
        UiShop.Instance.SelectedItem = this.gameObject;
        checkState();
    }
}
