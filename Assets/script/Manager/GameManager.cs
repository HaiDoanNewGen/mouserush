using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{

    public bool Enable_banner_ads { get => enable_banner_ads; set => enable_banner_ads = value; }
    public bool Enable_interstitial_ads { get => enable_interstitial_ads; set => enable_interstitial_ads = value; }
    public bool Enable_open_app_ads { get => enable_open_app_ads; set => enable_open_app_ads = value; }
    public bool Enable_replay_ads { get => enable_replay_ads; set => enable_replay_ads = value; }
    public bool Enable_reward_ads { get => enable_reward_ads; set => enable_reward_ads = value; }
    public string[] Show_insterstitial_after_finished_levels { get => show_insterstitial_after_finished_levels; set => show_insterstitial_after_finished_levels = value; }
    public double Show_interstitial_interval { get => show_interstitial_interval; set => show_interstitial_interval = value; }
    public bool Enable_ads_from_gameplay { get => enable_ads_from_gameplay; set => enable_ads_from_gameplay = value; }
    public bool Enable_ads_from_home { get => enable_ads_from_home; set => enable_ads_from_home = value; }
    public int Lvlreset_time_ads { get => lvlreset_time_ads; set => lvlreset_time_ads = value; }

    public bool hasChangeLanguage;

    private bool enable_banner_ads = true, enable_interstitial_ads = true, enable_open_app_ads = true, enable_replay_ads = true, enable_reward_ads = true;
    private string[] show_insterstitial_after_finished_levels;
    private double show_interstitial_interval = 40;
    private bool enable_ads_from_gameplay = true, enable_ads_from_home = true;

    private int lvlreset_time_ads = 3;

    private void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;        
    }    
}
