using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static AdsManager;

public class ButtonManager : Singleton<ButtonManager>
{    

    [Header("HOME")]        
    /*[SerializeField] Button[] SettingButton;*/
    [SerializeField] Button PlayBtn;
    [SerializeField] Button ShopBtn;    
    [Space]
    [Header("LEVEL")]
    [SerializeField] Button LEVEL_Back;
    /*[SerializeField] Button LEVEL_Yes1;
    [SerializeField] Button LEVEL_Yes2;
    [SerializeField] Button LEVEL_Nothanks;*/
    [Space]
    [Header("COMPLETE")]
    [SerializeField] Button COMPLETE_Back;
    [SerializeField] Button COMPLETE_Reset;
    [SerializeField] Button COMPLETE_Next;
    [Space]
    [Header("GAMEPLAY")]
    [SerializeField]public Button GAMEPLAY_Back;
    [SerializeField]public Button GAMEPLAY_Reset;
    [SerializeField]public Button GAMEPLAY_Hint;
    [Space]
    [Header("SHOP")]
    [SerializeField] Button SHOP_Back;
    [Space]
    [Header("SETTING")]
    [SerializeField] Button SETTING_Sound;
    [SerializeField] Button SETTING_Vibrate;
    [SerializeField] Button SETTING_Notification;
    /*[Space]
    [Header("BLOCK")]*/
    /*[SerializeField] Button BLOCK_Retry;*/
    

    private void Start()
    {
        ButtonInnit();

        if (GameData.GetVibrate())
        {
            ButtonInteract(true, SETTING_Vibrate.gameObject);
        }
        else
        {
            ButtonInteract(false, SETTING_Vibrate.gameObject);
        }

        if (GameData.GetVolumeValue() == 1)
        {
            ButtonInteract(true, SETTING_Sound.gameObject);
        }
        else
        {
            ButtonInteract(false, SETTING_Sound.gameObject);
        }
    }           

    public void ButtonInnit()
    {
        // home
        PlayBtn.onClick.AddListener(() =>
        {
            UIManager.Instance.UiToggleTo(UIManager.UiType.Level);
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
            if (AdsManager.Instance.hasInternet)
            {
                WatchVideoReward.instance.checkInterstitialAdsByType(InterstitialAds.fromHomeAds);
            }            
        });

        ShopBtn.onClick.AddListener(() => 
        {
            UIManager.Instance.UiToggleTo(UIManager.UiType.Shop);
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
            if (AdsManager.Instance.hasInternet)
            {
                WatchVideoReward.instance.checkInterstitialAdsByType(InterstitialAds.fromHomeAds);
            }            
        });

        //level
        LEVEL_Back.onClick.AddListener(() =>
        {
            UIManager.Instance.UiToggleTo(UIManager.UiType.Home);
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
        });        

        //complete
        COMPLETE_Back.onClick.AddListener(() => 
        {            
            UiGameplay.Instance.destroyLevelField();
            UIManager.Instance.UiToggleTo(UIManager.UiType.Complete);
            UIManager.Instance.UiToggleTo(UIManager.UiType.Level);        
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
            if (AdsManager.Instance.hasInternet)
            {
                WatchVideoReward.instance.checkInterstitialAdsByType(InterstitialAds.fromGamePlayAds);
            }            
        });
        
        COMPLETE_Reset.onClick.AddListener(() =>
        {
            UiComplete.Instance.gameObject.SetActive(false);
            UiGameplay.Instance.ResetGameplay(true);
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
        });

        COMPLETE_Next.onClick.AddListener(() =>
        {
            UiGameplay.Instance.NextGameplay();
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
        });

        // gameplay
        GAMEPLAY_Back.onClick.AddListener(() =>
        {
            UIManager.Instance.UiToggleTo(UIManager.UiType.Level);
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
            if (AdsManager.Instance.hasInternet)
            {
                WatchVideoReward.instance.checkInterstitialAdsByType(InterstitialAds.fromGamePlayAds);
            }            
        });
        GAMEPLAY_Reset.onClick.AddListener(() =>
        {
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
            StartCoroutine(buttonCoolDown(GAMEPLAY_Reset, 1f));
            UiGameplay.Instance.ResetGameplay(true);
        });
        GAMEPLAY_Hint.onClick.AddListener(() =>
        {
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click, 0);
            /*HintManager.Instance.toggleHint(true);*/
            if (AdsManager.Instance.hasInternet)
            {
                if (AdsManager.Instance.getAdsStat(AdsManager.adsType.reward))
                {
                    AdsManager.Instance.adsToCallBack = AdsManager.rewardAdsType.hint;
                    AdsManager.Instance.ShowRewardedAd();
                }
                else
                {
                    AdsManager.Instance.LoadRewardedAd();
                }
            }
            else
            {
                AdsManager.Instance.blockPannel.SetActive(true);
            }
        });
        // Shop
        SHOP_Back.onClick.AddListener(() =>
        {
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
            UIManager.Instance.UiToggleTo(UIManager.UiType.Home);
        });

        // Setting
        SETTING_Sound.onClick.AddListener(() =>
        {
            if (GameData.GetVolumeValue() == 1)
            {
                SoundManager.Instance.soundSwitch(false);
                ButtonInteract(false,SETTING_Sound.gameObject);
            }
            else
            {
                SoundManager.Instance.soundSwitch(true);
                ButtonInteract(true, SETTING_Sound.gameObject);
            }
            
        });
        SETTING_Vibrate.onClick.AddListener(() => 
        {
            vibrateSwitch();
        });
        SETTING_Notification.onClick.AddListener(() => 
        {
            if (SETTING_Notification.transform.GetChild(0).gameObject.activeInHierarchy)
            {
                SETTING_Notification.transform.GetChild(0).gameObject.SetActive(false);
                SETTING_Notification.transform.GetChild(1).gameObject.SetActive(true);
            }
            else
            {
                SETTING_Notification.transform.GetChild(0).gameObject.SetActive(true);
                SETTING_Notification.transform.GetChild(1).gameObject.SetActive(false);
            }
        });
        /*//BLOCK
        BLOCK_Retry.onClick.AddListener(() => 
        {
            if (AdsManager.Instance.hasInternet)
            {
                AdsManager.Instance.turnOffBlockPannel();
            }
        });*/
    }
    

    public void ButtonInteract(bool btnStat, GameObject btnObj)
    {
        if (btnStat)
        {
            btnObj.transform.GetChild(0).gameObject.SetActive(true);
            btnObj.transform.GetChild(1).gameObject.SetActive(false);
        }
        else
        {
            btnObj.transform.GetChild(0).gameObject.SetActive(false);
            btnObj.transform.GetChild(1).gameObject.SetActive(true);
        }
    }    

    IEnumerator buttonCoolDown(Button cooldownBtn, float coolDownTime)
    {
        cooldownBtn.interactable = false;
        yield return new WaitForSeconds(coolDownTime);
        cooldownBtn.interactable = true;
    }

    public void vibrateSwitch()
    {
        bool stat;
        stat = GameData.GetVibrate() ? false : true;
        GameData.SetVibrate(stat);
        ButtonInteract(stat, SETTING_Vibrate.gameObject);
    }

    public void commingSoon(bool endGame)
    {
        if (endGame)
        {
            COMPLETE_Next.interactable = false;
            COMPLETE_Next.transform.GetChild(1).gameObject.SetActive(true);
            COMPLETE_Next.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Comming Soon";
            COMPLETE_Next.transform.GetChild(0).gameObject.SetActive(false);
        }
        else
        {
            COMPLETE_Next.interactable = true;
            COMPLETE_Next.transform.GetChild(1).gameObject.SetActive(false);
            COMPLETE_Next.transform.GetChild(0).gameObject.SetActive(true);
        }
    }
}
