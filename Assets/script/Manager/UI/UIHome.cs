using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIHome : Singleton<UIHome>
{
    [SerializeField] public Image Transition;    


    private void OnEnable()
    {        
        Transition.gameObject.SetActive(true);
        Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeIn, false);
    }

    private void Start()
    {
        AdsManager.Instance.ShowAppOpenAd();        
    }    
}
