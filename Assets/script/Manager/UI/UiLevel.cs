using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UiLevel : Singleton<UiLevel>
{
    [SerializeField] public Image Transition;
    [SerializeField] GameObject listLevelField;     //instance level here
    [SerializeField]public LevelData levelDatabase;    
    [SerializeField] GameObject LvlPrefab;    
    List<GameObject> listBtn = new List<GameObject>();
    [SerializeField] GameObject PopUp;
    [SerializeField] GameObject Title;
    GameObject templvl;    
    public GameObject clickedLevel;
    Vector2 TitlePos;
    public LevelData getLevelDatabase()
    {
        return levelDatabase;
    }
    private void Awake()
    {
        TitlePos = Title.transform.localPosition;
    }
    private void OnEnable()
    {
        for(int i =0;i<listBtn.Count;i++)
        {
            listBtn[i].transform.GetChild(0).localScale = new Vector3(0, 0, 0);
        }
        Transition.gameObject.SetActive(true);
        Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeIn, false);
        StartCoroutine(delayTransition());
        if (listLevelField.transform.childCount > 0)
        {
            for(int i = 0; i < listLevelField.transform.childCount; i++)
            {
                listLevelField.transform.GetChild(i).GetComponentInChildren<LevelElement>().CheckForLevelStat();
            }
        }        
    }

    private void Start()
    {
        GenerateLevels();        
    }

    public void GenerateLevels()
    {        
        for(int i = 0; i < UIManager.Instance.LevelPrefabCount; i++)
        {
            templvl = Instantiate(LvlPrefab, listLevelField.transform);
            templvl.GetComponentInChildren<LevelElement>().elementSetup(i + 1, GameData.GetLevelCheeseObtained(i), GameData.GetLevelState(i));
            templvl.GetComponentInChildren<LevelElement>().CheckForLevelStat();
            listBtn.Add(templvl);
            LevelButtonInnit();            
        }        
    }
    public void LevelButtonInnit()
    {
        foreach (var a in listBtn)
        {
            a.GetComponentInChildren<Button>().onClick.AddListener(() =>
            {
                SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
                if (a.GetComponentInChildren<LevelElement>().levelLocked)
                {
                    //popup
                    LevelPopup(a);
                    clickedLevel = a;
                }
                else
                {                     
                    GameData.SetCurrentLevel(a.GetComponentInChildren<LevelElement>().level);
                    UIManager.Instance.UiToggleTo(UIManager.UiType.Gameplay);
                }
            });
        }
    }
    public void LevelPopup(bool stat)
    {
        PopUp.GetComponent<PopUpManager>().Ptype = PopUpManager.popupType.level;
        PopUp.SetActive(stat);
        //xem quang cao de unlock level        
    }

    void resetTitleAnim()
    {        
        Title.GetComponent<AnimationController>().ForceResetAnim();
        Title.GetComponent<TextTransition>().startTransition(TextTransition.FadeStyle.FadeOut);
        foreach(var a in listBtn)
        {            
            a.GetComponentInChildren<AnimationController>().ForceResetAnim();
        }        
    }
    IEnumerator delayTransition()
    {               
        Title.GetComponent<TextMeshProUGUI>().alpha = 0;
        Title.transform.localPosition = TitlePos;
        yield return new WaitForSeconds(0.25f);
        resetTitleAnim();
        yield return new WaitForSeconds(0.75f);
        Debug.Log(listLevelField.GetComponent<RectTransform>().localPosition + "hhh");
        listLevelField.GetComponent<RectTransform>().DOAnchorPosY(-listLevelField.GetComponent<RectTransform>().rect.height / 2, 0.75f);
    }
    
}
