using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UiGameplay : Singleton<UiGameplay>
{
    [SerializeField]public Image Transition;
    [SerializeField] GameObject TitleLevel;
    [SerializeField] GameObject TitleBox;
    [SerializeField] GameObject LevelField;    
    [SerializeField] CheeseCollector cheeseCollector;
    [SerializeField] TrapTut Tutorial;
    public bool tutorial;
    GameObject tempLevel;
    GameObject tempLine;
    Vector3 tempPos;
    

    public GameObject TempLine { get => tempLine; set => tempLine = value; }

    private void Start()
    {
        tempPos = TitleLevel.transform.localPosition;
    }

    private void OnEnable()
    {        
        SoundManager.Instance.changeThemeSound(SoundManager.SoundID.BG2);
        Transition.gameObject.SetActive(true);
        Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeIn, false);
        if (tempLevel != null)
        {
            Destroy(tempLevel);
        }
        if (tempLine != null)
        {
            Destroy(tempLine);
        }
        GameplaySetup(UIManager.Instance.getCurrentLevelPrefab(), GameData.GetCurrentLevel());        
    }   

    public void GameplaySetup(GameObject levelPrefabs,int levelNum)
    {
        if (!LevelField.activeInHierarchy)
            LevelField.SetActive(true);
        ButtonManager.Instance.GAMEPLAY_Reset.interactable = true;
        ButtonManager.Instance.GAMEPLAY_Hint.interactable = true;
        MouseHouse.Instance.UpdateHouseByLevel(GameData.GetCurrentLevel());
        TitleLevel.GetComponent<TextMeshProUGUI>().text = "LEVEL: " + (levelNum+1).ToString();
        tempLevel = Instantiate(levelPrefabs, LevelField.transform);
        HintManager.Instance.SetHintByLevel(tempLevel.GetComponent<levelAssetsController>().HintLine);
        HintManager.Instance.CheckForHintStat(levelNum);
        /*tempLevel.GetComponent<levelAssetsController>().jerryPrefabs.GetComponent<SpriteSetter>().setSkin(GameData.getSelectedItem());*/
        StartCoroutine(delayHideTitle());
        Tutorial.checkForTut();
    }

    public void ResetGameplay(bool adsCount)
    {
        destroyLevelField();
        if(adsCount)
            WatchVideoReward.instance.increaseC_Restart();
        GameplaySetup(UIManager.Instance.getCurrentLevelPrefab(), GameData.GetCurrentLevel());
        if(ButtonManager.Instance.GAMEPLAY_Back.interactable == false)
        {
            ButtonManager.Instance.GAMEPLAY_Back.interactable = true;
        }
        StartCoroutine(delayHideTitle());
    }

    public void NextGameplay()
    {        
        if (GameData.getCheeseProcess() < UiLevel.Instance.getLevelDatabase().levelDataLists[GameData.GetCurrentLevel() + 1].cheeseToUnlock)
        {
            //show text 
            UiComplete.Instance.NotEnoughCheeseToPass();
            return;
        }
        UIManager.Instance.UiToggleTo(UIManager.UiType.Complete);
        destroyLevelField();
        GameData.SetCurrentLevel(GameData.GetCurrentLevel() + 1);
        GameplaySetup(UIManager.Instance.getCurrentLevelPrefab(), GameData.GetCurrentLevel());        
    }

    IEnumerator delayHideTitle()
    {
        TitleBox.transform.localPosition = tempPos;
        yield return new WaitForSeconds(2f);
        TitleBox.GetComponent<AnimationController>().ForceResetAnim();
    }

    public void EndGameToggle(bool isWinning)
    {
        ButtonManager.Instance.GAMEPLAY_Reset.interactable = false;
        ButtonManager.Instance.GAMEPLAY_Hint.interactable = false;
        PlayerPrefs.SetInt("Hint_Level_" + GameData.GetCurrentLevel(), 0);
        if (isWinning)
        {
            //DO STUFF
            Debug.Log("======WIN GAME======");
            CheeseCollector.Instance.updateCheeseObtained();
            GameData.setCheeseProcess(GameData.GetCheese());
            //turn on complete ui
            if (AdsManager.Instance.hasInternet)
            {
                if (FirebaseInit.Instance == null)
                {
                    Debug.LogError("firebaseinnit null");
                }
                else
                {
                    FirebaseInit.Instance.LevelXPassed(GameData.GetCurrentLevel());
                }
                WatchVideoReward.instance.adsByLevel(GameData.GetCurrentLevel());
            }
            StartCoroutine(delayUiComplete(1.5f,isWinning));
        }
        else
        {
            //ALSO DO STUFF
            Debug.Log("====Lose Game====");
            //animation delay then
            StartCoroutine(delayUiComplete(1.5f, isWinning));
        }
        /*Destroy(tempLevel.GetComponent<levelAssetsController>().jerryPrefabs,1.5f);
        Destroy(tempLevel.GetComponent<levelAssetsController>().Tom,1.5f);*/

    }

    IEnumerator delayUiComplete(float delay,bool isWinning)
    {
        yield return new WaitForSeconds(delay);
        UIManager.Instance.UiToggleTo(UIManager.UiType.Complete);
        UiComplete.Instance.SetEnding(isWinning);        
        LevelField.SetActive(false);        
    }

    private void OnDisable()
    {
        SoundManager.Instance.changeThemeSound(SoundManager.SoundID.BG1);
        destroyLevelField();
        Tutorial.turnOffTut();
    }

    public void destroyLevelField()
    {
        Destroy(TempLine);
        Destroy(tempLevel);
    }

    private void OnApplicationPause(bool pause)
    {
        /*if (AdsManager.Instance.showingAds)
        {
            AdsManager.Instance.showingAds = false;
            return;
        }*/
        if(UiComplete.Instance!=null)
            if(!UiComplete.Instance.gameObject.activeInHierarchy)
                ResetGameplay(false);
    }
}
