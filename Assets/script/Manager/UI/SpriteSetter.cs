using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.U2D.Animation;


public class SpriteSetter : MonoBehaviour
{
    [Header("Idle Skin")]    
    [SerializeField] List<SpriteResolver> listIdleSpriteLib = new List<SpriteResolver>();    

    [Space]
    [Header("Run Skin")]    
    [SerializeField] List<SpriteResolver> listRunSpriteLib = new List<SpriteResolver>();    

    [Space]
    [Header("End Skin")]    
    [SerializeField] List<SpriteResolver> listEndSpriteLib = new List<SpriteResolver>();

    public void setSkin(int skinIndex)
    {
        if (listIdleSpriteLib.Count > 0)
        {
            foreach (var a in listIdleSpriteLib)
            {
                a.SetCategoryAndLabel(a.GetCategory(), a.GetCategory() + "_" + (skinIndex + 1));
                /*Debug.Log("catagory " + a.GetCategory() + "_" + (skinIndex + 1));*/
            }
        }
        if(listRunSpriteLib.Count > 0)
        {
            foreach (var a in listRunSpriteLib)
            {
                a.SetCategoryAndLabel(a.GetCategory(), a.GetCategory() + "_" + (skinIndex + 1));
            }
        }        
    }    

    public void setEndSkin(int endSkinIndex)
    {
        foreach(var a in listEndSpriteLib)
        {
            a.SetCategoryAndLabel(a.GetCategory(), a.GetCategory() + "_" + (endSkinIndex + 1));
        }
    }
}
