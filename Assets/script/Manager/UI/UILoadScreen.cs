using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Threading;
using UnityEngine.SceneManagement;
using TMPro;

public class UILoadScreen : MonoBehaviour
{
    [SerializeField] Image progress;
    [SerializeField] float duration;
    [SerializeField] public Image Transition;
    /*[SerializeField] GameObject Home;
    [SerializeField] GameObject soundManager;*/
    [SerializeField] GameObject transitionObj;
    [SerializeField] TextMeshProUGUI LoadPercent;
    AsyncOperation asyncLoad;
    /*bool startLoading = false;*/
    bool transitionOn = false;
    /*private void Start()
    {
        loadSplash();
    }*/

    public void loadSplash()
    {
        /*transitionObj.SetActive(true);
        Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeIn,true);*/
        StartCoroutine(ChangeSomeValue(0, 1, duration));
    }
    
    public IEnumerator ChangeSomeValue(float oldValue, float newValue, float duration)
    {
        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            float percentage = Mathf.Lerp(oldValue, newValue, t / duration);
            progress.fillAmount = percentage;
            LoadPercent.text = Mathf.Round(percentage*100).ToString()+"%";
            yield return null;
        }
        progress.fillAmount = newValue;        
        StartCoroutine(LoadYourAsyncScene());
    }

    /*public IEnumerator toTheGame()
    {
        transitionObj.SetActive(true);
        Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeOut);
        yield return new WaitForSeconds(0.5f);        
        *//*soundManager.SetActive(true);
        Home.SetActive(true);*//*
        gameObject.SetActive(false);
    }*/

    IEnumerator LoadYourAsyncScene()
    {
        progress.fillAmount = 0.1f;

        /*asyncLoad = SceneManager.LoadSceneAsync("MainScene");*/
        asyncLoad = SceneManager.LoadSceneAsync("MainScene");        
        // Wait until the asynchronous scene fully loads
        /*asyncLoad.allowSceneActivation = true;*/
        while (!asyncLoad.isDone)
        {
            float loadProgess = Mathf.Clamp01(asyncLoad.progress / 0.9f);
            progress.fillAmount = loadProgess;
            LoadPercent.text = Mathf.Round(loadProgess*100).ToString() + "%";
            yield return null;
        }                
    }

    private void Update()
    {
        /*if (progress.fillAmount >= 0.8 && !transitionOn)
        {
            transitionOn = true;
            transitionObj.SetActive(true);
            Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeOut, true);
        }*/
        if (AdsManager.Instance.getAdsStat(AdsManager.adsType.open))
        {
            StopAllCoroutines();
            StartCoroutine(LoadYourAsyncScene());
            this.enabled = false;
        }
        else if (AdsManager.Instance.openAdsLoadFailed)
        {
            StopAllCoroutines();
            StartCoroutine(LoadYourAsyncScene());
            this.enabled = false;
        }
    }
}
