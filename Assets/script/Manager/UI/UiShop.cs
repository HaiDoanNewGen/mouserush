using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class UiShop : Singleton<UiShop>
{
    [SerializeField]public Image Transition;
    [SerializeField]TextMeshProUGUI CheeseAmount;
    [SerializeField]public ShopDatabase shopData;
    [SerializeField] GameObject ShopItemField;
    [SerializeField] GameObject itemPrefabs;
    [SerializeField]public GameObject popUp;
    [SerializeField] LevelData levelDatabase;

    public GameObject SelectedItem;
    int itemChecked = 0;

    private void OnEnable()
    {
        Transition.gameObject.SetActive(true);
        if (itemChecked == shopData.getShopdataSize())
        {
            Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeIn, false);
        }
        updateCheeseText();
        if (ShopItemField.transform.childCount > 0)
        {
            for(int i =0;i< ShopItemField.transform.childCount; i++)
            {
                ShopItemField.transform.GetChild(i).GetComponent<ShopElementItem>().checkState();
            }
        }
    }

    private void Start()
    {
        for (int i = 0; i < UIManager.Instance.LevelPrefabCount; i++)
        {
            if (GameData.getCheeseProcess() >= levelDatabase.getDataByLevel(i).cheeseToUnlock && !GameData.GetLevelState(i))
            {
                GameData.UpdateLevelState(i, true);
            }
        }
        GameData.CreateAdList(shopData.getShopdataSize());
        shopSettup();
    }
    
    public void shopSettup()
    {
        for(int i = 0; i < shopData.getShopdataSize(); i++)
        {
            GameObject a = Instantiate(itemPrefabs, ShopItemField.transform);            
            if (GameData.getItemStat(i) == ItemState.selected)
            {
                Debug.Log("index " + i);
                SelectedItem = a;
            }
            a.GetComponent<ShopElementItem>().elementSetup(shopData.getShopdataByIndex(i).pricesAd.ToString(),GameData.getItemStat(i),i);
            a.GetComponent<ShopElementItem>().checkState();
        }
        for (int i = 0; i < ShopItemField.transform.childCount; i++)
        {
            ShopItemField.transform.GetChild(i).GetComponent<IndividualSpriteSet>().setUpIndividualSkin(i);
        }
    }

    public void checkItem()
    {
        itemChecked++;
        Debug.Log("checked item: " + itemChecked+" zzz");
        if (itemChecked == shopData.getShopdataSize())
        {
            Transition.GetComponent<TransitionAnim>().startTransition(TransitionAnim.FadeStyle.FadeIn, false);
        }
    }

    public void shopPopUp(bool stat,int itemIndex, ShopElementItem element)
    {
        if (itemIndex != 0 && element!=null)
        {
            AdsManager.Instance.itemIndex = itemIndex;
            AdsManager.Instance.itemToBuy = element;
        }                
        popUp.GetComponent<PopUpManager>().Ptype = PopUpManager.popupType.shop;
        popUp.SetActive(stat);
        Debug.Log("shop pop up " + stat);
    }

    public void updateCheeseText()
    {
        CheeseAmount.text = GameData.GetCheese()+"/"+GameData.getCheeseProcess();
    }
}
