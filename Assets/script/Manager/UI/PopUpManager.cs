using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpManager : MonoBehaviour
{
    [Header("POPUP")]
    [SerializeField] Button YesBtn;
    [SerializeField] Button Yes2Btn;
    [SerializeField] Button NothanksBtn;

    public enum popupType
    {
        shop,
        level
    }
    public popupType Ptype;
    public void popupSetter()
    {
        Debug.Log("xem quang cao");
        switch (Ptype)
        {
            case popupType.level:
                if (AdsManager.Instance.hasInternet)
                {

                    if (AdsManager.Instance.getAdsStat(AdsManager.adsType.reward))
                    {
                        AdsManager.Instance.adsToCallBack = AdsManager.rewardAdsType.level;
                        AdsManager.Instance.ShowRewardedAd();
                    }
                }
                else
                {
                    AdsManager.Instance.blockPannel.SetActive(true);
                }
                break;
            case popupType.shop:
                if (AdsManager.Instance.hasInternet)
                {
                    if (AdsManager.Instance.getAdsStat(AdsManager.adsType.reward))
                    {
                        AdsManager.Instance.adsToCallBack = AdsManager.rewardAdsType.skin;
                        AdsManager.Instance.ShowRewardedAd();
                    }
                }
                else
                {
                    AdsManager.Instance.blockPannel.SetActive(true);
                }
                break;
        }
    }

    public void BtnInnit()
    {
        Debug.Log("Button innit");
        YesBtn.onClick.AddListener(() =>
        {
            //xem qc
            popupSetter();
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
        });

        Yes2Btn.onClick.AddListener(() =>
        {
            //xem qc
            popupSetter();
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
        });

        NothanksBtn.onClick.AddListener(() =>
        {
            this.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Click,0);
            //tat di
        });
    }

    private void Update()
    {
        if (!AdsManager.Instance.getAdsStat(AdsManager.adsType.reward))
        {
            if (Yes2Btn == null || YesBtn == null)
                return;
            YesBtn.interactable = false;
            Yes2Btn.interactable = false;
        }
        else        
        {
            YesBtn.interactable = true;
            Yes2Btn.interactable = true;
        }
    }

    private void OnEnable()
    {
        BtnInnit();
        StartCoroutine(appearNothanks());
        if (!AdsManager.Instance.getAdsStat(AdsManager.adsType.reward))
        {
            AdsManager.Instance.LoadRewardedAd();
        }
    }
    IEnumerator appearNothanks()
    {
        NothanksBtn.gameObject.SetActive(false);
        yield return new WaitForSeconds(2f);
        NothanksBtn.gameObject.SetActive(true);
    }
}

