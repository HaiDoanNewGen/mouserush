using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UiComplete : Singleton<UiComplete>
{    
    [SerializeField] GameObject MidField;    

    [Header("TOP_Field")]
    [SerializeField] TextMeshProUGUI cheeseText;

    [Header("MID_Field")]
    [SerializeField] GameObject CheeseGroup;
    [SerializeField] GameObject NotEnoughCheese;

    [Header("EndingPannel")]
    [SerializeField] TextMeshProUGUI WinTitle;
    [SerializeField] GameObject winPannel;
    [SerializeField] GameObject LoosePannel;

    Vector2 tempPos;

    int stuckTime = 0,levelStuck = 0;

    public enum EndingType
    {
        defaultEnd,
        trapEnd,
        catEnd,        
    }

    public EndingType endingType;

    private void Start()
    {        
        tempPos = NotEnoughCheese.transform.position;
    }

    private void OnEnable()
    {                
        NotEnoughCheese.SetActive(false);
        for (int i = 0; i < CheeseGroup.transform.childCount; i++)
        {
            CheeseGroup.transform.GetChild(i).gameObject.SetActive(false);
            CheeseGroup.transform.GetChild(i).transform.localScale = new Vector3(0, 0, 0);
        }
        MidField.transform.localScale = new Vector3(0, 0, 0);
        MidField.GetComponent<AnimationController>().ForceResetAnim();        
        cheeseText.text = GameData.GetCheese().ToString();

        if (GameData.GetCurrentLevel() + 1 > UIManager.Instance.LevelPrefabCount - 1)
        {
            ButtonManager.Instance.commingSoon(true);            
        }else
        {
            ButtonManager.Instance.commingSoon(false);
        }

    }

    public void SetEnding(bool isWinning)
    {
        if (isWinning)
        {
            //Win pannel
            WinTitle.text = "VICTORY";
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Success,0);
            winPannel.SetActive(true);
            LoosePannel.SetActive(false);
            CheeseGroup.SetActive(true);
            Debug.Log(CheeseCollector.Instance.CheeseObtained);
            if(GameData.GetLevelCheeseObtained(GameData.GetCurrentLevel())> CheeseGroup.transform.childCount)
            {
                Debug.LogError("cheese obtained more than cheese per level");
                return;
            }
            Debug.Log(GameData.GetLevelCheeseObtained(GameData.GetCurrentLevel()));
            if (PlayerPrefs.GetInt("TrapTut" + GameData.GetCurrentLevel()) != 1)
                PlayerPrefs.SetInt("TrapTut" + GameData.GetCurrentLevel(), 1);
            /*HintManager.Instance.TurnOffHint();*/
        }
        else
        {
            //lose pannel
            WinTitle.text = "YOU LOSE";
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Fail,0);
            LoosePannel.SetActive(true);
            winPannel.SetActive(false);
            CheeseGroup.SetActive(true);
            if (GameData.GetCurrentLevel() == levelStuck)
            {
                stuckTime++;
            }
            else
            {
                levelStuck = GameData.GetCurrentLevel();
                stuckTime = 0;
                if (AdsManager.Instance.hasInternet)
                {
                    if (FirebaseInit.Instance == null)
                    {
                        Debug.LogError("firebaseinnit null");
                    }
                    else
                    {
                        FirebaseInit.Instance.playerLost(levelStuck);
                    }
                }
            }
            if (stuckTime > 3)
            {
                if (AdsManager.Instance.hasInternet)
                {
                    if (FirebaseInit.Instance == null)
                    {
                        Debug.LogError("firebaseinnit null");
                    }
                    else
                    {
                        FirebaseInit.Instance.playerStuckAtLevel(levelStuck);
                    }

                }
            }
        }        
        StartCoroutine(delayCheeseAnim());
        if(ButtonManager.Instance.GAMEPLAY_Back.interactable == false)
        {
            ButtonManager.Instance.GAMEPLAY_Back.interactable = true;
        }
    }

    IEnumerator delayCheeseAnim()
    {
        yield return new WaitForSeconds(1);
        for (int i = 0; i < /*GameData.GetLevelCheeseObtained(GameData.GetCurrentLevel())*/CheeseCollector.Instance.getCheeseObtained(); i++)
        {
            CheeseGroup.transform.GetChild(i).gameObject.SetActive(true);
            CheeseGroup.transform.GetChild(i).GetComponent<AnimationController>().ForceResetAnim();            
            switch (i)
            {
                case 0:
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.Star1, 0);
                    break;
                case 1:
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.Star2, 0);
                    break;
                case 2:
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.Star3, 0);
                    break;
            }
            yield return new WaitForSeconds(0.25f);
        }
    }

    public void NotEnoughCheeseToPass()
    {
        NotEnoughCheese.SetActive(true);
        NotEnoughCheese.transform.position = tempPos;
        NotEnoughCheese.GetComponent<AnimationController>().ForceResetAnim();
        StartCoroutine(delayNotEnoughText());
    }
    IEnumerator delayNotEnoughText()
    {
        NotEnoughCheese.GetComponent<TextMeshProUGUI>().DOFade(1f, 1f);
        yield return new WaitForSeconds(1f);
        NotEnoughCheese.GetComponent<TextMeshProUGUI>().DOFade(0f, 1f);

    }
    private void OnDisable()
    {
        NotEnoughCheese.SetActive(false);
    }
}
