using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetector : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //stop the player
            /*collision.GetComponent<playerController>().playerState = playerController.PlayerState.stopping;*/
            //start chasing the player
            this.GetComponent<Collider2D>().enabled = false;
            GetComponentInParent<enemyController>().enabled = true;
            GetComponentInParent<enemyController>().catchPlayer(collision.transform,true);
        }
    }
}
