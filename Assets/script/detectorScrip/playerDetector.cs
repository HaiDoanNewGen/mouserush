using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerDetector : MonoBehaviour
{
    bool takeTheTrap = false;
    Collider2D collisionObj;    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //run straight to the trap
            collision.GetComponent<playerController>().setPlayerState(playerController.PlayerState.stopping);
            collisionObj = collision;
            takeTheTrap = true;
            GetComponent<CircleCollider2D>().enabled = false;
        }
    }

    private void Update()
    {
        if (takeTheTrap)
        {
            if((Vector2)transform.parent.position!= (Vector2)collisionObj.transform.position)
            {
              collisionObj.transform.position = Vector2.MoveTowards((Vector2)collisionObj.transform.position, transform.parent.position, 0.1f);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
    }
}
