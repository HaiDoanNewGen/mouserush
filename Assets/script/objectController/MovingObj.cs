using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObj : MonoBehaviour
{
    [Serializable]
    public enum MoveDirection
    {
        Horizontal,
        Vertical
    }
    [Serializable]
    public enum DirectionPriority
    {
        left,
        right
    }

    Vector2 vectorDirection;
    [SerializeField] float speed;
    [SerializeField] bool FlipAble;
    [SerializeField] MoveDirection moveDirect;
    [SerializeField] DirectionPriority directPrio;
    [SerializeField] bool StopAble = false;    
    [SerializeField] float stopDuration = 1f;
    GameObject tempWall;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {                
        if (collision.CompareTag("Wall"))
        {
            if (collision.gameObject == tempWall)
            {
                return;
            }
            tempWall = collision.gameObject;
            SwitchDirection(collision);
        }
    }    
    void SwitchDirection(Collider2D collision)
    {
        if (StopAble)
        {
            StartCoroutine(stopForSecs(stopDuration));
            return;
        }
        else
        {
            moveToDirection();
        }        
    }

    IEnumerator stopForSecs(float duration)
    {
        GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        GetComponentInParent<enemyController>().switchPose();
        yield return new WaitForSeconds(duration);
        GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        moveToDirection();
        GetComponentInParent<enemyController>().switchPose();
        yield return new WaitForSeconds(duration);        
    }

    void moveToDirection()
    {        
        switch (moveDirect)
        {
            case MoveDirection.Horizontal:
                //change direction
                if (vectorDirection == Vector2.left)
                {
                    vectorDirection = Vector2.right;
                    GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;                    
                    GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    GetComponentInParent<Rigidbody2D>().AddForce(vectorDirection * speed, ForceMode2D.Impulse);
                    checkFlip(180f);
                }
                else
                {
                    vectorDirection = Vector2.left;
                    GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;                    
                    GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    GetComponentInParent<Rigidbody2D>().AddForce(vectorDirection * speed, ForceMode2D.Impulse);
                    checkFlip(0f);
                }
                break;
            case MoveDirection.Vertical:
                //change direction
                if (vectorDirection == Vector2.up)
                {
                    vectorDirection = Vector2.down;
                    GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;                    
                    GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    GetComponentInParent<Rigidbody2D>().AddForce(vectorDirection * speed, ForceMode2D.Impulse);
                    checkFlip(180f);
                }
                else
                {
                    vectorDirection = Vector2.up;
                    GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                    GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    GetComponentInParent<Rigidbody2D>().AddForce(vectorDirection * speed, ForceMode2D.Impulse);
                    checkFlip(0f);
                }
                break;
        }
    }

    private void Start()
    {
        switch (moveDirect)
        {
            case MoveDirection.Horizontal:
                if(directPrio == DirectionPriority.left)
                {
                    vectorDirection = Vector2.left;
                    checkFlip(0f);
                }
                else
                {
                    vectorDirection = Vector2.right;
                    checkFlip(180f);
                }                
                break;
            case MoveDirection.Vertical:
                if(directPrio == DirectionPriority.right)
                {
                    vectorDirection = Vector2.up;
                    checkFlip(0f);
                }
                else
                {
                    vectorDirection = Vector2.down;
                    checkFlip(180f);
                }                
                break;
        }
        GetComponentInParent<Rigidbody2D>().AddForce(vectorDirection * speed, ForceMode2D.Impulse);
    }    

    void checkFlip(float FlipY)
    {
        if (FlipAble)
        {
            transform.parent.rotation = Quaternion.Euler(new Vector3(0f, FlipY, 0f));
        }
    }
}
