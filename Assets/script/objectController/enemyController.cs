using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class enemyController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] GameObject movingPose;
    [SerializeField] GameObject IdlePose;
    bool isChasing = false;
    Transform PlayerCatched;


    public void catchPlayer(Transform Player, bool goChasing)
    {
        isChasing = goChasing;
        PlayerCatched = Player;
        stopWandering();
    }    

    private void FixedUpdate()
    {
        if (isChasing)
        {
            chasingPlayer();
        }
    }

    public void switchPose()
    {
        if (movingPose.activeInHierarchy)
        {
            IdlePose.SetActive(true);
            movingPose.SetActive(false);
        }
        else
        {
            IdlePose.SetActive(false);
            movingPose.SetActive(true);
        }
    }

    public void chasingPlayer()
    {        
        transform.position = Vector2.MoveTowards((Vector2)transform.position, (Vector2)PlayerCatched.position, speed);
    }

    public void stopWandering()
    {
        GetComponentInChildren<MovingObj>().enabled = false;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;        
    }
}
