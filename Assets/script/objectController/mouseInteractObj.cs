using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseInteractObj : MonoBehaviour
{
    [Serializable]
    public enum interactionType
    {
        Trap,
        Cheese,
        House,
        Cat,
        BadApple
    }
    [SerializeField] interactionType InteractionType;
    private void OnTriggerEnter2D(Collider2D collision)
    {        
        if (collision.CompareTag("Player"))
        {
            switch (InteractionType)
            {
                case interactionType.Trap:
                    //end game (lose)
                    if (GameData.GetVibrate())
                    {
                        Handheld.Vibrate();
                    }
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.Trap,0);
                    Debug.Log("====Lose Game====");
                    collision.GetComponent<Collider2D>().enabled = false;                    
                    collision.GetComponent<playerController>().setPlayerState(playerController.PlayerState.stopping);
                    collision.GetComponent<playerController>().crashAnim.SetActive(true);
                    UiGameplay.Instance.EndGameToggle(false);
                    break;
                case interactionType.Cheese:
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.Eat,0);
                    collision.GetComponent<playerController>().gotDaCheese();
                    GetComponent<Collider2D>().enabled = false;
                    //call back to cheese collector to addup
                    CheeseCollector.Instance.AddCheese(1);
                    //play some fadeout anim
                    gameObject.SetActive(false);
                    break;
                case interactionType.House:
                    //end game win
                    if (GameData.GetVibrate())
                    {
                        Handheld.Vibrate();
                    }
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.MouseHouse,0);
                    GetComponent<houseConstructor>().startAnimation();
                    GetComponent<Collider2D>().enabled = false;
                    MouseHouse.Instance.decreaseGoal();
                    if (MouseHouse.Instance.getGoalStat())
                    {
                        collision.GetComponent<Collider2D>().enabled = false;
                        collision.gameObject.SetActive(false);
                    }
                    collision.GetComponent<playerController>().setPlayerState(playerController.PlayerState.idle);
                    break;
                case interactionType.Cat:
                    //end game win
                    if (GameData.GetVibrate())
                    {
                        Handheld.Vibrate();
                    }
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.Cat,0);
                    GetComponent<Collider2D>().enabled = false;
                    collision.GetComponent<playerController>().setPlayerState(playerController.PlayerState.stopping);
                    collision.GetComponent<playerController>().crashAnim.SetActive(true);
                    gameObject.SetActive(false);
                    UiGameplay.Instance.EndGameToggle(false);
                    break;
                case interactionType.BadApple:
                    if (GameData.GetVibrate())
                    {
                        Handheld.Vibrate();
                    }
                    SoundManager.Instance.PlaySound(SoundManager.SoundID.Eat,0);
                    Debug.Log("====slow down mah nibba====");
                    GetComponent<Collider2D>().enabled = false;
                    collision.GetComponent<playerController>().speed = 0.03f;
                    gameObject.SetActive(false);
                    break;
            }            
        }
    }
}
