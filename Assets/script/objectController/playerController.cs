using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{

    public List<Vector2> runPoint;
    public float speed = 1;
    int index=0;
    private PlayerState playerState /*= PlayerState.wandering*/ = PlayerState.idle;
    PlayerState preState;

    [Header("MouseAnim")]
    [SerializeField] GameObject Idle;
    [SerializeField] GameObject Running;
    [SerializeField] GameObject cheese;

    [Space]

    [SerializeField]public GameObject crashAnim;

    Vector2 flipPos;
    
    bool hasStop = false;
    bool hasSetAnim = false;    

    public void gotDaCheese()
    {
        if(!cheese.activeInHierarchy)
            cheese.SetActive(true);
    }    

    public void setPlayerState(PlayerState stat)
    {
        if(stat == PlayerState.running)
        {
            SoundManager.Instance.PlaySound(SoundManager.SoundID.Run,0);
        }
        if(stat == PlayerState.idle || stat == PlayerState.stopping)
        {
            SoundManager.Instance.StopSound(SoundManager.SoundID.Run);
        }
        playerState = stat;
        Debug.Log(playerState + " abc");
    }

    public enum PlayerState
    {
        running,
        stopping,
        idle
        /*wandering*/
    }

    private void OnEnable()
    {
        crashAnim.SetActive(false);
        setPlayerState(PlayerState.idle);
        setAnim();
        preState = playerState;
        flipPos = transform.position;        
    }

    private void Start()
    {
        GetComponent<SpriteSetter>().setSkin(GameData.getSelectedItem());
    }

    void setAnim()
    {
        switch (playerState)
        {
            case PlayerState.idle:
                Idle.SetActive(true);
                Running.SetActive(false);
                break;
            case PlayerState.running:
                Idle.SetActive(false);
                Running.SetActive(true);
                break;
            case PlayerState.stopping:
                Idle.SetActive(false);
                Running.SetActive(false);
                break;
        }        
    }


    private void FixedUpdate()
    {        
        if (transform.position.x - flipPos.x > 0.1f)
        {
            flipPos = transform.position;
            transform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0));
        }
        if (transform.position.x - flipPos.x < -0.1f)
        {
            flipPos = transform.position;
            transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0));
        }
        if (playerState == PlayerState.running)
        {
            runNiggaRun();
        }
        /*if (playerState != PlayerState.wandering && GetComponentInChildren<MovingObj>()!=null)
        {
            GetComponentInChildren<MovingObj>().enabled = false;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;
        }*/        
        if (playerState == PlayerState.stopping)
        {
            if (!hasStop)
            {
                stopRunning();
            }
            if (!hasSetAnim)
            {
                hasSetAnim = true;
                setAnim();
            }
        }
        if(playerState == PlayerState.idle)
        {
            if (!hasSetAnim)
            {
                hasSetAnim = true;
                setAnim();
            }
        }
        if (preState != playerState)
        {
            preState = playerState;
            hasSetAnim = false;
        }
    }

    void stopRunning()
    {        
        hasStop = true;
        transform.position = transform.position;
        if (!hasSetAnim)
        {
            hasSetAnim = true;
            setAnim();
        }
    }

    public void runNiggaRun()
    {
        if (!hasSetAnim)
        {
            hasSetAnim = true;
            setAnim();
        }
        /*if ((Vector2)transform.position != runPoint[index])
        {
            transform.position = Vector2.MoveTowards((Vector2)transform.position, runPoint[index], speed);

        }
        else if (index < runPoint.Count - 1)
        {
            index++;
        }*/
        //change the way to calculate running speed
        if ((Vector2)transform.position != runPoint[index])
        {
            transform.position = Vector2.MoveTowards((Vector2)transform.position, runPoint[index], speed);
            if (index < runPoint.Count - 1 && Vector2.Distance((Vector2)transform.position, runPoint[index]) < 0.025f)
                index++;            
        }        
    }        
}
