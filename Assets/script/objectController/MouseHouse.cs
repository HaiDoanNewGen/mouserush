using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseHouse : Singleton<MouseHouse>
{
    int goalNumber;
    
    public void UpdateHouseByLevel(int levelIndex)
    {
        goalNumber = UiLevel.Instance.getLevelDatabase().getDataByLevel(levelIndex).goalNumber;
    }

    public void decreaseGoal()
    {
        if(!getGoalStat())
        {
            goalNumber--;
        }
        else
        {
            //win
            UiGameplay.Instance.EndGameToggle(true);
        }
    }

    public bool getGoalStat()
    {
        return goalNumber - 1 > 0 ? false : true;
    }
}
