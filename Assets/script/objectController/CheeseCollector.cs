using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheeseCollector : Singleton<CheeseCollector>
{
    private int cheeseObtained;

    public int CheeseObtained { get => cheeseObtained; set => cheeseObtained = value; }


    /*int CheeseByLevel;

    public void updateCheeseBylevel(int levelIndex)
    {
        CheeseByLevel = UiLevel.Instance.getLevelDatabase().getDataByLevel(levelIndex).cheeseNumber;
    }*/


    public void AddCheese(int numCheese)
    {
        //call when get a cheese
        Debug.Log("========add cheese + " + numCheese);
        cheeseObtained+=numCheese;
    }
    
    public void updateCheeseObtained()
    {
        if (cheeseObtained < GameData.GetLevelCheeseObtained(GameData.GetCurrentLevel()))
        {
            return;
        }
        GameData.AddupCheese(cheeseObtained- GameData.GetLevelCheeseObtained(GameData.GetCurrentLevel()));
        GameData.UpdateCheeseByLevel(GameData.GetCurrentLevel(), cheeseObtained);
        //update cheese obtained by level

    }

    public int getCheeseObtained()
    {
        return cheeseObtained;
    }
}
