using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class houseConstructor : MonoBehaviour
{
    
    [SerializeField] GameObject houseSkin;
    bool isUpdated = false;
    private void Update()
    {
        if (!isUpdated)
        {
            setAnimationSkin();
            isUpdated = true;
        }
    }

    public void setAnimationSkin()
    {
        houseSkin.SetActive(true);
        houseSkin.GetComponent<SpriteSetter>().setEndSkin(GameData.getSelectedItem());
        houseSkin.SetActive(false);
    }

    public void startAnimation()
    {
        houseSkin.SetActive(true);
        houseSkin.GetComponent<SpriteSetter>().setEndSkin(GameData.getSelectedItem());
    }
}
