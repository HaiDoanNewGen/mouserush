using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelDataBase", menuName = "GameData/Level Database")]
public class LevelData : ScriptableObject
{
    [Serializable]
    public struct LevelDataList
    {
        public int goalNumber;
        public int cheeseNumber;
        public int cheeseToUnlock;
    }
    public List<LevelDataList> levelDataLists;

    public LevelDataList getDataByLevel(int index)
    {
        return levelDataLists[index];
    }    
}
