
using UnityEngine;
using System.Runtime.Serialization;
using System.Collections.Generic;

//Player Data Holder

/*public class LevelState
{
    public int LevelIndex = 0;
    public bool State = false; // playable

    public LevelState(int index, bool stat)
    {
        this.LevelIndex = index;
        this.State = stat;
    }
}*/


public enum ItemState
{
    owned,
    selected,
    locked,
    unlocked
}

[System.Serializable] public class PlayerData
{
    public int Cheese = 0;
    public int LevelProcess = 0; // = number off all cheese obtain from begining    
    public float VolumeValue = 1;
    public int selectedItem = 0;
    public List<int> LevelComplete = new List<int>();    
    public List<int> LevelSkip = new List<int>();
    /*public List<int> CheeseObtainedByLvl = new List<int>();*/
    public List<int> OwnedDataShop = new List<int>() {0};
    /*public List<LevelState> LevelStates = new List<LevelState>();*/
    public bool PhoneVibrate = true;
    public List<int> ListItemAdProcess = new List<int>();
}



public static class GameData
{
    static PlayerData playerData = new PlayerData();

    static int CurrentLevel = 0;
    static GameData()
    {        
        LoadPlayerData();                             
    }

    public static void SetCurrentLevel(int index)
    {
        CurrentLevel = index;
    }    

    public static int GetCurrentLevel()
    {
        return CurrentLevel;
    }

    //Player Data Method
    
    public static void AddupCheese(int cheeseNum)
    {
        playerData.Cheese += cheeseNum;
        SavePlayerData();
    }

    public static int GetCheese()
    {
        return playerData.Cheese;
    }

    public static void setCheeseProcess(int level)
    {
        if (level <= playerData.LevelProcess)
            return;
        playerData.LevelProcess = level;
        SavePlayerData();
    }
    public static int getCheeseProcess() // cheese process
    {
        return playerData.LevelProcess;
    }

    public static float GetVolumeValue()
    {
        return playerData.VolumeValue;
    }

    public static void SetVolumeValue(float value)
    {
        playerData.VolumeValue = value;
        SavePlayerData();
    }

    public static void SetVibrate(bool stat)
    {
        playerData.PhoneVibrate = stat;
        SavePlayerData();
    }

    public static bool GetVibrate()
    {
        return playerData.PhoneVibrate;
    }

    static void LoadPlayerData()
    {
        playerData = BinarySerializer.Load<PlayerData>("player-data.txt");            
    }

    static void SavePlayerData()
    {
        BinarySerializer.Save(playerData,"player-data.txt");

    }          


    #region LevelComplete
    public static void AddLevelComplete(int level)
    {
        playerData.LevelComplete.Add(level);
        SavePlayerData();
    }

    public static List<int> GetAllLevelComplete()
    {
        List<int> levelComplete = new List<int>();
        levelComplete = playerData.LevelComplete;
        return levelComplete;
    }
    #endregion    

    #region LevelSkip
    public static void AddLevelSkip(int level)
    {
        playerData.LevelSkip.Add(level);
        SavePlayerData();
    }

    public static List<int> GetAllLevelSkip()
    {
        List<int> levelSkip = new List<int>();
        levelSkip = playerData.LevelSkip;
        return levelSkip;
    }

    public static void RemoveLevelSkip(int level)
    {
        playerData.LevelSkip.Remove(level);
        SavePlayerData();
    }
    #endregion

    #region CheeseObtainedByLevel
    public static void UpdateCheeseByLevel(int level,int cheeseObtained)
    {
        PlayerPrefs.SetInt("Cheese" + level.ToString(), cheeseObtained);        
    }

    public static int GetLevelCheeseObtained(int index)
    {                                
        return PlayerPrefs.GetInt("Cheese" + index.ToString(), 0);
    }
    #endregion

    #region LevelState
    public static void UpdateLevelState(int levelIndex, bool state) // state = playable
    {
        PlayerPrefs.SetInt(levelIndex.ToString(),state == true? 0:1);
        Debug.Log("level zzz "+levelIndex+" "+PlayerPrefs.GetInt(levelIndex.ToString()));
    }

    public static bool GetLevelState(int levelIndex)
    {        
        return PlayerPrefs.GetInt(levelIndex.ToString(),1)==0;  // playable = true or false
    }
    #endregion

    #region ShopDatabase
    public static void AddItemOwned(int index)
    {
        playerData.OwnedDataShop.Add(index);
        updateItemState(index, ItemState.owned);
        SavePlayerData();
    }

    public static List<int> getOwnedItem()
    {
        return playerData.OwnedDataShop;
    }

    public static void updateItemState(int index, ItemState Stat)
    {
        Debug.Log("update stat item = " + index+" "+Stat.ToString());
        PlayerPrefs.SetString("ShopItem_" + index, Stat.ToString());
    }

    public static ItemState getItemStat(int index)
    {

        string state = PlayerPrefs.GetString("ShopItem_" + index, ItemState.locked.ToString());
        if(index == 0 && PlayerPrefs.GetString("ShopItem_" + index, ItemState.locked.ToString()) == "locked")
        {
            setSelectedItem(index);
            updateItemState(index, ItemState.selected);
            state = "selected";
        }
        ItemState convertState = new ItemState();
        switch (state)
        {
            case "owned":
                convertState = ItemState.owned;
                break;
            case "selected":
                convertState = ItemState.selected;
                break;
            case "locked":
                convertState = ItemState.locked;
                break;
            case "unlocked":
                convertState = ItemState.unlocked;
                break;
        }
        return convertState;
    }

    public static void setSelectedItem(int index)
    {
        updateItemState(getSelectedItem(), ItemState.owned);
        updateItemState(index, ItemState.selected);
        playerData.selectedItem = index;
        SavePlayerData();
    }

    public static int getSelectedItem()
    {
        return playerData.selectedItem;
    }

    public static void updateListAdItem(int itemIndex,int adProcess)
    {
        playerData.ListItemAdProcess[itemIndex] += adProcess;
    }

    public static void CreateAdList(int amount)
    {
        if (playerData.ListItemAdProcess.Count == 0)
        {
            for(int i = 0; i < amount; i++)
            {
                playerData.ListItemAdProcess.Add(0);
            }
            SavePlayerData();
        }
    }

    public static int getAdItemProcess(int index)
    {
        return playerData.ListItemAdProcess[index];
    }
    #endregion
}
