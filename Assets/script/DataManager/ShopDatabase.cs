using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ShopDatabase", menuName = "GameData/Shop Database")]

public class ShopDatabase : ScriptableObject
{
    [Serializable]
    public struct ShopData
    {                
        public int pricesAd;
        public int ProcessToUnlock;
    }

    public List<ShopData> ShopdataList;

    public ShopData getShopdataByIndex(int index)
    {
        return ShopdataList[index];
    }
    public int getShopdataSize()
    {
        return ShopdataList.Count;
    }    

    public int getProcessToUnlock(int index)
    {
        return ShopdataList[index].ProcessToUnlock;
    }
}
